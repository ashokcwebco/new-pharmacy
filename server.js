var express         = require('express');
var  bodyParser     = require('body-parser');
var dbConn     = require('./config/connection');


var app = express();

// Parser body to request
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
  next();
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
//app.use(cookieParser());
// app.use(express.static(path.join(__dirname, "js")));
require('./routes')(app, dbConn); // call api 

dbConn.connect().then(function () {
  var  port          = 8888; // use port
  app.listen(port, ()=>{
      console.log('we are live on: '+port);
  });
});



