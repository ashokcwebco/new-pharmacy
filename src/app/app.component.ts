import {ChangeDetectorRef,ViewContainerRef, Component } from '@angular/core';
import { PharmacistComponent } from './public/users/pharmacist/pharmacist.component';
import {  BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {Http, Response, Headers, RequestOptions} from '@angular/http';
import { CommonService } from './service/common.service';
import { Constant } from '../app/constant';
import {Router,ActivatedRoute} from '@angular/router';
import { Location } from '@angular/common';
import { AuthService } from './service/auth.service';
import Swal from 'sweetalert2';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'app';
  AuthUserName;
  user_detail;
  current_route;
  constructor(
    public router: Router,
    public route: ActivatedRoute,
    private location : Location,
    public auth : AuthService,
    private http: Http,
    public comService : CommonService,
    public toastr: ToastsManager,
    vcr: ViewContainerRef
    ) {this.toastr.setRootViewContainerRef(vcr);}
  ngOnInit() {
   this.header_changes();
  }
  
  header_changes() {
    this.current_route = this.location.path(); 

    if(this.auth.authCheck()) {
      this.user_detail = this.auth.authCheck();
      this.AuthUserName = this.user_detail.userName;
      this.auth.showHeader = true;
    } else {
      this.auth.showHeader = false;
    }
  }


  logOut() {
    Swal({
      title: 'Are you sure?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes',
      cancelButtonText: 'Cancel'
    }).then((result) => {
      if (result.value) {
        this.auth.showHeader = false;
        localStorage.removeItem(Constant.LOGIN_STORAGE);
        localStorage.removeItem(Constant.LOGIN_NAME);
        this.router.navigate(['/login']); 
      } else if (result.dismiss === Swal.DismissReason.cancel) {
      }
    })

  }


  
  processOut(event){
    if(event.target.action == "return"){
     var barcode =  event.target.value.toUpperCase();
      this.http.get(Constant.API_URL+'packageReturn/'+barcode+'/1').map(res => res.json()).subscribe(data => {
          this.comService.isReturn[barcode]= true;
          this.toastr.success('Package successfully return to bin', 'Success!');
      }, err => {
          console.log(err.message);
      });
    }

    // if(event.target.value){
    //     const header = new Headers();
    //     header.append("Content-Type", "application/json");
    //     const options = new RequestOptions({headers: header});
    //     this.http.get(Constant.API_URL+'order/getOrderByBarCode/'+event.target.value, options).map(res => res.json()).subscribe(data => {
    //     }, err => {
    //   });
    // }else{
    //   this.comService.scantext = null;
    //   this.comService.searchBarCoed = null;
    // }

  }

  public listStatus:Boolean;
  diplayList(id){
    this.listStatus = false;
    this.router.navigate(['profile',id]);
  }

  public patientFilterLoading:Boolean;
  patientFilter(event){
    if(event.target.action != "return" && event.target.value.length > 3){
      this.patientFilterLoading = true;
      this.http.get(Constant.API_URL+'patients/filterTop/'+event.target.value).map(res => res.json()).subscribe(data => {
        this.listStatus = true;
      this.comService.patientList = data;
      this.patientFilterLoading = false;
      }, err => {
          console.log(err.message);
          this.patientFilterLoading = false;
      });
    }

  }
 
}
