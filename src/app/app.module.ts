import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {  BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { NgModule, LOCALE_ID } from '@angular/core';
import {  HttpModule} from "@angular/http";
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { AppRoutingModule } from './app-routing.module';
import { CommonService } from './service/common.service';
import { AuthService } from './service/auth.service';
import { PrintService } from './service/print.service';
import { ConversationService } from './service/conversation.service';


import { AppComponent } from './app.component';
import { LoginComponent } from './public/login/login.component';

// New 
import { DashboardComponent } from './public/operations/new/new.component';

// Ready section (IN BIN)
import { ReadyComponent } from './public/operations/ready/ready.component';
  import { InBinComponent } from './public/operations/ready/in-bin/in-bin.component';

// Delivery (DELIVERY RUN)
import { DeliveryRunComponent } from './public/operations/delivery/delivery-run/delivery-run.component';
  import { NewRunComponent } from './public/operations/delivery/new-run/new-run.component';
  import { InTransitComponent } from './public/operations/delivery/in-transit/in-transit.component';
  import { FinalizeComponent } from './public/operations/delivery/finalize/finalize.component';


import { PharmacistComponent } from './public/users/pharmacist/pharmacist.component';
import { AgenciesComponent } from './public/agencies/agencies.component';
import { TicketPageComponent } from './public/operations/ticket-page/ticket-page.component';
import { ReminderComponent } from './public/reminders/allReminder/reminder.component';

import { NgDragDropModule } from 'ng-drag-drop';


import { AddReminderComponent } from './public/reminders/add-reminder/add-reminder.component';
import { EscapeHtmlPipe } from './keep-html.pipe';
import {ToastModule} from 'ng2-toastr/ng2-toastr';


import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { DueTodayComponent } from './public/operations/delivery/due-today/due-today.component';
import { ReorderComponent } from './public/operations/new/reorder/reorder.component';
import { GeneralComponent } from './public/operations/new/general/general.component';
import { MedchangeComponent } from './public/operations/new/medchange/medchange.component';
import { InProductionComponent } from './public/operations/production/in-production/in-production.component';
import { NewComponent } from './public/operations/production/new/new.component';
import { ProgressComponent } from './public/operations/production/progress/progress.component';
import { ProfileComponent } from './public/Profile/profile/profile.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashboardComponent,
    PharmacistComponent,
    AgenciesComponent,
    TicketPageComponent,
    ReminderComponent,
    ReadyComponent,
    DeliveryRunComponent,
    InBinComponent,
    AddReminderComponent,
    EscapeHtmlPipe,
    NewRunComponent,
    InTransitComponent,
    FinalizeComponent,
    DueTodayComponent,
    ReorderComponent,
    GeneralComponent,
    MedchangeComponent,
    InProductionComponent,
    NewComponent,
    ProgressComponent,
    ProfileComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpModule,
    FormsModule,
    BrowserAnimationsModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    NgDragDropModule.forRoot(),
    NgMultiSelectDropDownModule.forRoot(),
    ToastModule.forRoot()
  ],

  providers: [CommonService,AuthService,PrintService,ConversationService,[{provide: LOCALE_ID, useValue: 'en-US' }]],

  bootstrap: [AppComponent]
})
export class AppModule { }
