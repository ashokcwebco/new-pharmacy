import { NgModule } from '@angular/core';
import { Routes, RouterModule,ActivatedRoute} from '@angular/router';

import { LoginComponent } from './public/login/login.component';
import { DashboardComponent } from './public/operations/new/new.component';
import { PharmacistComponent } from './public/users/pharmacist/pharmacist.component';
import { AgenciesComponent } from './public/agencies/agencies.component';
import { TicketPageComponent } from './public/operations/ticket-page/ticket-page.component';
import { ReminderComponent } from './public/reminders/allReminder/reminder.component';
import { AddReminderComponent } from './public/reminders/add-reminder/add-reminder.component';
import { ReadyComponent } from './public/operations/ready/ready.component';
import { DeliveryRunComponent } from './public/operations/delivery/delivery-run/delivery-run.component';
import { InProductionComponent } from './public/operations/production/in-production/in-production.component';
import { ProfileComponent } from './public/Profile/profile/profile.component';

import { Constant } from '../app/constant';

const routes: Routes = [
  { path: '', redirectTo: '/operations/new', pathMatch: 'full' },
  { path:'', component:TicketPageComponent,
    children:[
      {path:'operations/new', component:DashboardComponent},
      {path:'operations/in-production', component:InProductionComponent},
      {path:'operations/ready', component:ReadyComponent},
      {path:'operations/delivery-run', component:DeliveryRunComponent},
      {path:'reminders', component:ReminderComponent},
      {path:'add-reminder', component:AddReminderComponent},
    ]
  },
  {path:'users', component:PharmacistComponent},
  {path:'profile/:PatID', component:ProfileComponent},
  
  {path:'login', component:LoginComponent},
  {path:'agencies', component:AgenciesComponent},
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
