import { Component, OnInit } from '@angular/core';
import {Http, Response, Headers, RequestOptions} from '@angular/http';
import 'rxjs/add/operator/map';
import { Constant } from '../../constant';
import { Router } from '@angular/router';
import { AuthService } from '../../service/auth.service';
import { CommonService } from '../../service/common.service';
import { AppComponent } from '../../app.component';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  login_error : any;
  constructor(
    private http: Http,
    private router: Router,
    public auth : AuthService,
    public comService : CommonService,
    public AppComponent : AppComponent 
  ) { 
    
  }

  ngOnInit() {
    this.AppComponent.header_changes();
    if(this.auth.authCheck()) {
      this.router.navigate(['/operations/new']);
      return;
    }
  }

  data = {
    email : '',
    password : ''
  };

  loginForm() {
    if(!this.data.email || !this.data.email) {
      return;
    }
    this.comService.showLoading();
    const header = new Headers();
    header.append("Content-Type", "application/json");
    const options = new RequestOptions({headers: header});
    var body = {email : this.data.email, password : this.data.password};
    this.http.post(Constant.API_URL+'login-user',body,options).map(res => res.json()).subscribe(data => {
      this.comService.showLoading(false);
            if(data != null) {
              localStorage.setItem(Constant.LOGIN_STORAGE, data.user_id);
              localStorage.setItem(Constant.LOGIN_NAME, data.userName);
              this.auth.showHeader = true;
              this.router.navigate(['/operations/new']);
            } else {
              this.login_error = 'Incorrect email or password combination.';
            }
        }, err => {
          this.comService.showLoading(false);
    });

    

  }


  
}