import { Component, ViewContainerRef, OnInit } from '@angular/core';
import {Constant} from '../../../constant';
import {Http, Response, Headers, RequestOptions} from '@angular/http';
import { ReactiveFormsModule,FormArray, FormsModule, FormGroup, FormControl, Validators,FormBuilder} from '@angular/forms';
import 'rxjs/add/operator/map';
import {Router, ActivatedRoute} from '@angular/router';
import { DatePipe } from '@angular/common';
import { CommonService } from '../../../service/common.service';
import { AuthService } from '../../../service/auth.service';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';


@Component({
  selector: 'app-ticket-page',
  templateUrl: './ticket-page.component.html',
  styleUrls: ['./ticket-page.component.scss']
})
export class TicketPageComponent implements OnInit {
  cities;
  prnsObj = [];
  patients=[];ticketType=[];patientList = [];batchType;type;
  ticketForm: FormGroup;
  title: FormControl;
  TicketType : FormControl;
  PatID: FormControl;
  Details: FormControl;
  // prns: FormControl;
  DeliveryDate: FormControl;
  change: FormControl;
  action: FormControl;
  startDate: FormControl;
  endDate : FormControl;
  deliveryDate : FormControl;
  Strips : FormControl;
  
  defaultDeliverOn;
  public min = new Date(2018, 1, 12, 10, 30);
  showlist = true;
  ischecked = true;
  selectedPatient;
  // dropdownSettings = {};

  constructor(
    private fb: FormBuilder,
    private http: Http,
    public auth : AuthService ,
    private router: Router,
    public comService : CommonService,
    public toastr: ToastsManager,
     vcr: ViewContainerRef  
  ) { 
    this.toastr.setRootViewContainerRef(vcr);
  }
  items: any = [];
 
  ngOnInit() {

  /*==================================================================
  DESCRIPTION: Authentication Checking 
  ===================================================================*/
    if(!this.auth.authCheck()) {
      this.router.navigate(['/login']);
      return;
    }

    this.getCounter();

    this.formNewTicketRole();
    this.fromNewTicketData();
    this.getTicketType();

    
  }

  /*==================================================================
  DESCRIPTION: LEFT PANEL TAB COUNTERS 
  ===================================================================*/
  getCounter() {
    const header = new Headers();
    header.append("Content-Type", "application/json");
    const options = new RequestOptions({headers: header});
    this.http.get(Constant.API_URL+'counter', options).map(res => res.json()).subscribe(data => {
      this.comService.navCounter.Tickets = data.TicketCount;
      this.comService.navCounter.reminder = data.ReminderCount;
      this.comService.navCounter.delivery = data.DeliveryCount;
      this.comService.navCounter.homeBin =  data.BinsCount;
      this.comService.navCounter.production = data.batchTicketCount + data.batchPkgCount;
  }, err => {
  });
  }

  /*==================================================================
  WRITTEN BY:
  DESCRIPTION: define validation for creating new ticket 
  ===================================================================*/
  formNewTicketRole() {
    this.title = new FormControl('');
    this.TicketType = new FormControl('', Validators.required);
    this.PatID = new FormControl('',Validators.required);
    this.Details = new FormControl('');
    this.DeliveryDate = new FormControl('');
    this.change = new FormControl('');
    this.action = new FormControl('');
    this.startDate = new FormControl('');
    this.endDate = new FormControl('');
    this.Strips = new FormControl('');
    
  }
  /*==================================================================
  WRITTEN BY:
  DESCRIPTION:: set valuein form group 
  ===================================================================*/
  fromNewTicketData() {
    this.ticketForm = this.fb.group({
      TicketType: this.TicketType,
      title: this.title,
      PatID: this.PatID,
      Details:this.Details,
      DeliveryDate:this.DeliveryDate,
      change : this.change,
      action : this.action,
      items: this.fb.array([this.createItem()])
    });
  
  }

 /*==================================================================
  DESCRIPTION: PUSH RXID TI A ARRAY
  ===================================================================*/

  colleteRxIDs(e) {
    if(e.currentTarget.checked){
      this.prnsObj.push(parseInt(e.target.value));
    }else{
      var indx = this.comService.indexWhere(this.prnsObj, item=>item === parseInt(e.target.value));
      this.prnsObj.splice(indx,1)
    }  
  }

  /*==================================================================
  DESCRIPTION: CREATE FORM GROUP FIELDS (DINAMICALY)IN MEDCHANGES TICKET 
  ===================================================================*/

  createItem(): FormGroup {
    return this.fb.group({
        startDate : this.startDate,
        endDate : this.endDate,
        deliveryDate : this.deliveryDate,
        Strips : this.Strips
    });
  }

  /*==================================================================
  DESCRIPTION: ADD FIELDS (DINAMICALY )IN MEDCHANGES TICKET 
  ===================================================================*/

  addItem(): void {
    this.items = this.ticketForm.get('items') as FormArray;
    this.items.push(this.createItem());
  }



  removeDescription(Indx) {
    const control = <FormArray>this.ticketForm.controls["items"];
    control.removeAt(Indx);
  }

/*==================================================================
  DESCRIPTION: WHEN SELECT PATIENT ITS PASS DATA ACC. TO  PATIENT ID
  ===================================================================*/
  selectPat(obj, batchType){
    this.ticketForm.controls['PatID'].setValue(obj.FirstName+' '+obj.LastName);
    this.selectedPatient = obj;
    this.prnsObj= [];
    this.showlist = false;
    if(batchType != 1) 
      this.ticketForm.controls['DeliveryDate'].setValue(this.comService.getDateFromDay(obj.DefaultDeliveryDay,obj.hour, obj.minute));
    if(batchType == 2) 
      this.getPatientsRX(obj.ID);
    
  }

 /*==================================================================
  DESCRIPTION: IF GENERAL TYPE TICKET ITS SET DELEVERY DATE NOT REQUIRED
  ===================================================================*/
  onChange(item:any){
    this.batchType = item;
    if(item !=1 && item){
      this.ticketForm.controls["DeliveryDate"].setValidators([Validators.required]);
    }
  }
  /*==================================================================
  WRITTEN BY:
  DESCRIPTION: Create new ticket with validation 
  ===================================================================*/
  public creatingTicket:Boolean;
  onSubmit(){
    if(this.ticketForm.valid){
      if(this.ticketForm.value.TicketType == 2 && this.prnsObj.length < 1){
        this.toastr.info("Please select atleast one PRN.");
        return;
      }
      this.ticketForm.value.prns = (this.ticketForm.value.TicketType != 2) ?[]:this.prnsObj;
      if(this.ticketForm.value.TicketType == 3) {
        this.ticketForm.value.Details = this.ticketForm.value.change+';'+this.ticketForm.value.action+';'+JSON.stringify(this.ticketForm.value.items);
      }
    
      this.creatingTicket = true;
      this.ticketForm.value.PatID = (this.selectedPatient)?this.selectedPatient.ID:'';
      this.ticketForm.value.UserIDCreated = this.auth.authCheck()['user_id']; 
      if(this.batchType !=1)
      this.ticketForm.value.DeliveryDate = this.comService.dateFormatWithTime(this.ticketForm.value.DeliveryDate);
      else
      this.ticketForm.value.DeliveryDate = '';
          const header = new Headers();
          header.append("Content-Type", "application/json");
          const options = new RequestOptions({headers: header});
          this.http.post(Constant.API_URL+'ticket/new-ticket',this.ticketForm.value, options).map(res => res.json()).subscribe(data => {
            this.prnsObj = [];
            this.comService.navCounter.Tickets = this.comService.navCounter.Tickets + parseInt(data.length);
            if(data[0].TicketType ==1)
            this.comService.general.unshift(data[0]);
            if(data[0].TicketType ==2)
            this.comService.reorder.unshift(data[0]);
            if(data[0].TicketType ==3)
            this.comService.medChangeTicket.unshift(data[0]);
            this.comService.closeModel();
            this.ticketForm.reset();
            this.ihavpatientRXs = false;
            this.creatingTicket = false;
        }, err => {
          this.creatingTicket = false;
        });
    }else{
      this.toastr.error('Please fill all mandatory fields.', 'Oops!');
    }
  }

  /*==================================================================
  WRITTEN BY:
  DESCRIPTION: get ticket types like "general, reoder" 
  ===================================================================*/
  public loadingTicketType:Boolean;
  getTicketType(){
    this.loadingTicketType = true;
    const header = new Headers();
    header.append("Content-Type", "application/json");
    const options = new RequestOptions({headers: header});
    this.http.get(Constant.API_URL+'ticket/type',options).map(res => res.json()).subscribe(data => {
      this.loadingTicketType = false;
      this.ticketType = data;
        }, err => {
          this.loadingTicketType = false;
           console.log(err.message);
    });
  }

  /*==================================================================
  WRITTEN BY:
  DESCRIPTION: Filter patient to assign a new ticket
  ===================================================================*/
  findingPatients = false;
  filterPat(e){
    if(e.target.value.length >= 4) {
        this.findingPatients = true;
        this.patientRXs = [];
      const header = new Headers();
      header.append("Content-Type", "application/json");
      const options = new RequestOptions({headers: header});
      this.http.get(Constant.API_URL+'patients/filter/'+e.target.value,options).map(res => res.json()).subscribe(data => { 
            this.showlist = true; 
            this.findingPatients = false;
            this.patientList = data;
          }, err => {
            this.findingPatients = false;
      });
    } else {
      this.patientList = [];
    }
  }

  /*==================================================================
  DESCRIPTION: Get Patients RX
  ===================================================================*/

  patientRXs = [];
  ihavpatientRXs = false;
  findingpatientRXs = false;
  getPatientsRX(PatID){
    this.ihavpatientRXs = false;
    this.findingpatientRXs = true;
    const header = new Headers();
    header.append("Content-Type", "application/json");
    const options = new RequestOptions({headers: header});
    this.http.get(Constant.API_URL+'getPatRx/'+PatID,options).map(res => res.json()).subscribe(data => { 
            this.patientRXs = data;
            this.ihavpatientRXs = true;
            this.findingpatientRXs = false;
        }, err => {
          this.findingpatientRXs = false;
    });
  }

  /*==================================================================
  DESCRIPTION: RESET TICKET FORM
  ===================================================================*/
  ticketFromReset(){
    this.ticketForm.reset();
  }

}