import { Component, ViewContainerRef, OnInit } from '@angular/core';
import {Constant} from './../../../../constant';
import {Http, Response, Headers, RequestOptions} from '@angular/http';
import { ReactiveFormsModule, FormsModule, FormGroup, FormControl, Validators,FormBuilder} from '@angular/forms';
import 'rxjs/add/operator/map';
import { AuthService } from '../../../../service/auth.service';
import {Router,ActivatedRoute} from '@angular/router';
import { DatePipe } from '@angular/common';
import { CommonService } from '../../../../service/common.service';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
@Component({
  selector: 'app-new',
  templateUrl: './new.component.html',
  styleUrls: ['./new.component.scss']
})
export class NewComponent implements OnInit {

  status = {nonAuto:false, loader:[],loadAttachedOrder:[],mainLoader:false};
  BatchDetails = [];
  attachTicketMainIcon = [];
  rxCards =[];
  batchNum = [];
  RxList = [];
  temp=[];
  constructor( private route: ActivatedRoute,
    private http: Http,
    public auth : AuthService ,
    private router: Router,
    public comService : CommonService ,
    public toastr: ToastsManager,
    vcr: ViewContainerRef  ) {this.toastr.setRootViewContainerRef(vcr); }

  ngOnInit() {
    this.getBatches();
    this.comService.batches =[];
  }

  /*==================================================================
  WRITTEN BY:
  DECRIPTION: get new ticket created 
  ===================================================================*/
  getBatches(){
    this.status.mainLoader = true;
    const header = new Headers();
    header.append("Content-Type", "application/json");
    const options = new RequestOptions({headers: header});
    this.http.get(Constant.API_URL+'order/getBatches',options).map(res => res.json()).subscribe(data => {
          this.comService.batches = data;
          this.status.mainLoader = false;
      }, err => {
          console.log(err.message);
          this.status.mainLoader = false;
    });
  }


  /*==================================================================
  WRITTEN BY: SEAMLESS CARE
  DECRIPTION: GET DETAILS FOR EACH BATCH PATIENT AND ITS RX 
  ===================================================================*/
  getBatchDetails(e){
    if(!this.status.loadAttachedOrder[e.target.rel]){
      this.status.loader[e.target.rel] = true; // loader
      const header = new Headers();
      header.append("Content-Type", "application/json");
      const options = new RequestOptions({headers: header});
      this.http.get(Constant.API_URL+'order/getBatchDetails/'+e.target.rel,options).map(res => res.json()).subscribe(data => {
              this.BatchDetails[e.target.rel] = data;
              this.status.loadAttachedOrder[e.target.rel] = true;
              this.status.loader[e.target.rel] = false;
              e.path[1].nextElementSibling.style.display = "block"; 
              this.attachTicketMainIcon[e.target.rel] = true; 
          }, err => {
            console.log(err.message);
            this.status.loader[e.target.rel] = false;
      });
  }else{
    e.path[1].nextElementSibling.style.display =  (e.path[1].nextElementSibling.style.display == "none")?"block":"none";
      this.attachTicketMainIcon[e.target.rel] =  (e.path[1].nextElementSibling.style.display == "none")?false:true;
      this.status.loader[e.target.rel] = false;
  }
}

 /*==================================================================
  WRITTEN BY:
  DECRIPTION: COLLECT BATCH NUMBERS
  ===================================================================*/

orderBatch(obj){
  this.batchNum.push(obj['BatchNum']);
  var Ind = this.comService.indexWhere(this.comService.batches[0], item=>item.BatchNum ===obj['BatchNum']);
  this.comService.batches[0].splice(Ind,1);
  // this.comService.batches[0][Ind].isSelected = true;
}

   /*==================================================================
  WRITTEN BY:
  DECRIPTION: PLASE ORDER BATCH RX CARDS
  ===================================================================*/
  public oredrLoader=[];
  orderBatchCard(obj){
      var batchNum =  obj['BatchNum'];
      this.oredrLoader[batchNum] = true;
      var body = {"batchNum":batchNum,"UserIDCreated":this.auth.authCheck()['user_id']};
      this.http.post(Constant.API_URL+'order/createOrdersFromBatch',body).map(res => res.json()).subscribe(data => {
        if(data.code){
          this.toastr.error("Please assign home to patient", 'Sorry!');
          this.oredrLoader[batchNum] = false;
          return; 
        }
        this.oredrLoader[batchNum] = false;
        var Ind = this.comService.indexWhere(this.comService.batches[0], item=>item.BatchNum === batchNum);
        this.comService.batches[0].splice(Ind,1);
        for(var i=0; i < data.length; i++){
          this.comService.batch.unshift(data[i]);
        }
        this.toastr.success('Order successfully created.', 'Success!');
      }, err => {
        this.toastr.error(err.message, 'Oops!');
        this.oredrLoader[batchNum] = false;
      });
  }
}
