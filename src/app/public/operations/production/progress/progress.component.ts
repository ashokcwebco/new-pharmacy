import { Component, OnInit } from '@angular/core';
import {Constant} from '../../../../constant';
import {Http, Response, Headers, RequestOptions} from '@angular/http';
import { ReactiveFormsModule, FormsModule, FormGroup, FormControl, Validators,FormBuilder} from '@angular/forms';
import 'rxjs/add/operator/map';
import {Router} from '@angular/router';
import { DatePipe } from '@angular/common';
import { PrintService } from '../../../../service/print.service';
import { CommonService } from '../../../../service/common.service';
import { AuthService } from '../../../../service/auth.service';
import { AppComponent } from '../../../../app.component';
import { ConversationService } from '../../../../service/conversation.service';
@Component({
  selector: 'app-progress',
  templateUrl: './progress.component.html',
  styleUrls: ['./progress.component.scss']
})
export class ProgressComponent implements OnInit {

  status = {loader:[],mainLoader:false, loadAttachedOrder:[],nAprv:[],packLoader:[],progressIsLoaded:[]}
  btnDisable=[];
  btnDisableApp = [];
  public RxCards=[];
  attachTicketMainIcon = [];
  deliveryChangeFrom: FormGroup;
  deliveryDate: FormControl;
  constructor(
    private http: Http,
    private router: Router,
    private prnt:PrintService,
    public comService : CommonService ,
    public auth : AuthService,
    public AppComponent : AppComponent ,
    public ConService : ConversationService  
  
  ){ }

  ngOnInit() {
    this.getBatchTicket();
    this.comService.batch= [];
    this.deliveryDate = new FormControl('', Validators.required);
    this.deliveryChangeFrom = new FormGroup({deliveryDate: this.deliveryDate});
  }

  /*==================================================================
  WRITTEN BY:SEAMLESS CARE
  DECRIPTION: GET GENERAL TICKETS
  ===================================================================*/
  getBatchTicket(){
    this.status.mainLoader = true;
    const header = new Headers();
    header.append("Content-Type", "application/json");
    const options = new RequestOptions({headers: header});
    this.http.get(Constant.API_URL+'ticket/batch',options).map(res => res.json()).subscribe(data => {
            this.comService.batch = data;
            this.status.mainLoader = false;
        }, err => {
           this.status.mainLoader = false;
    });
  }

  /*==================================================================
  WRITTEN BY:SEAMLESS CARE
  DECRIPTION: GET MEDICATIONS
  INPUT: PATID,ORDERID
  ===================================================================*/
  getMedication(ticket){
    if(!this.status.progressIsLoaded[ticket.PharmacyTicketID]){
      this.status.progressIsLoaded[ticket.PharmacyTicketID] = true;
      this.status.loader[ticket.PharmacyTicketID] = true;
      const header = new Headers();
      header.append("Content-Type", "application/json");
      const options = new RequestOptions({headers: header});
      this.http.get(Constant.API_URL+'getMedication/'+ticket.orderID,options).map(res => res.json()).subscribe(data => {
              this.RxCards[ticket.PharmacyTicketID] = data;
              this.status.loader[ticket.PharmacyTicketID] = false;
          }, err => {
            this.status.loader[ticket.PharmacyTicketID] = false;
      });
    }
  }

  manageTicketMove(ticketId){

      var ticketIndex =  this.comService.indexWhere(this.comService.batch, item=>item.PharmacyTicketID === ticketId);
      this.comService.batch.splice(ticketIndex,1);
      this.comService.navCounter.Tickets = this.comService.navCounter.Tickets - parseInt('1');
  
  }

  /*==================================================================
  WRITTEN BY:SEAMLESS CARE
  DECRIPTION: TICKET COMPLETE 
  ===================================================================*/

  completeTicket(ticketId){
    this.btnDisable[ticketId] = true;
    var  body ={"TicketID":ticketId, "UserID": this.auth.authCheck()['user_id']}
    const header = new Headers();
    header.append("Content-Type", "application/json");
    const options = new RequestOptions({headers: header});
    this.http.put(Constant.API_URL+'ticket/complete',body,options).map(res => res.json()).subscribe(data => {
      this.manageTicketMove(ticketId)
        }, err => {
          this.btnDisable[ticketId] = false;
    });
 }

 public currentTicketUpdating = 0;
 changeDeliveryDate(ticketID,date){
   this.currentTicketUpdating = ticketID;
   this.deliveryChangeFrom.controls['deliveryDate'].setValue(new Date(this.comService.dateFormater(date)));
 }

 public updatingDeliveryDate:Boolean;
 updateDeliveryDate(){
    if(this.deliveryChangeFrom.valid){
      this.updatingDeliveryDate = true;
      this.deliveryChangeFrom.value.PharmacyTicketID = this.currentTicketUpdating;
      this.deliveryChangeFrom.value.deliveryDate = this.comService.dateFormatWithTime(this.deliveryChangeFrom.value.deliveryDate);
      this.http.put(Constant.API_URL+'updateTicketDeliveryDate',this.deliveryChangeFrom.value).map(res => res.json()).subscribe(data => {
            var ticketIndex =  this.comService.indexWhere(this.comService.batch, item=>item.PharmacyTicketID === this.currentTicketUpdating);
            this.comService.closeModel('CloseButton1');
            this.comService.batch[ticketIndex].DeliveryDate = this.deliveryChangeFrom.value.deliveryDate;
            this.currentTicketUpdating = 0;
            this.updatingDeliveryDate = false;
          }, err => {
            this.updatingDeliveryDate = false;
      });
    }
  }

}
