import { Component, OnInit } from '@angular/core';
import {Constant} from '../../../constant';
import {Router} from '@angular/router';
import {Http, Response, Headers, RequestOptions} from '@angular/http';
import { CommonService } from '../../../service/common.service';
import { AuthService } from '../../../service/auth.service';
import { print } from 'html-to-printer';

@Component({
  selector: 'app-ready',
  templateUrl: './ready.component.html',
  styleUrls: ['./ready.component.scss']
})
export class ReadyComponent implements OnInit {
  private binHistory = {"Homes":[],"OrderCards":[]};
  public loadFilterData =  false;
  constructor( private router: Router,
    public comService : CommonService ,
    private http: Http,
    public auth : AuthService) { }

  ngOnInit() {
    this.comService.homeBinLoader = false;
    this.comService.activeNav('ready');
  }

  /*==================================================================
  WRITTEN BY:SEAMLESS CARE
  DECRIPTION: GET FILTER BINS
  ===================================================================*/

  filterHome(event){ 
    if(event.target.value.length >= 2){
      this.loadFilterData = true;
      if(this.binHistory.Homes.length <= 0){
        this.binHistory.Homes = this.comService.inBin;
        this.binHistory.OrderCards = this.comService.inBinOrderCard;
      }
      this.http.get(Constant.API_URL+'getFilterInBin/'+event.target.value).map(res => res.json()).subscribe(data => {
        this.comService.inBin = data[0].Homes;
        this.comService.navCounter.homeBin = data[0].Homes.length;
        this.comService.inBinOrderCard=data[0].OrderCards;
        this.loadFilterData = false;
      }, err => {
        this.loadFilterData = false;
      });
    }else{
      if(!event.target.value){
        this.loadFilterData = false;
        this.comService.navCounter.homeBin = this.binHistory.Homes.length;
        this.comService.inBin = this.binHistory.Homes;
        this.comService.inBinOrderCard =this.binHistory.OrderCards;
      }
    }
  }

}
