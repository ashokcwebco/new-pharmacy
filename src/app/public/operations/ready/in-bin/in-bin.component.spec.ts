import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InBinComponent } from './in-bin.component';

describe('InBinComponent', () => {
  let component: InBinComponent;
  let fixture: ComponentFixture<InBinComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InBinComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InBinComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
