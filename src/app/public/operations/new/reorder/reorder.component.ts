import { Component, OnInit } from '@angular/core';
import {Constant} from '../../../../constant';
import {Http, Response, Headers, RequestOptions} from '@angular/http';
import { ReactiveFormsModule, FormsModule, FormGroup, FormControl, Validators,FormBuilder} from '@angular/forms';
import 'rxjs/add/operator/map';
import {Router} from '@angular/router';
import { DatePipe } from '@angular/common';
import { PrintService } from '../../../../service/print.service';
import { CommonService } from '../../../../service/common.service';
import { AuthService } from '../../../../service/auth.service';
import { AppComponent } from '../../../../app.component';
import { ConversationService } from '../../../../service/conversation.service';


@Component({
  selector: 'app-reorder',
  templateUrl: './reorder.component.html',
  styleUrls: ['./reorder.component.scss']
})
export class ReorderComponent implements OnInit {
  status = {loader:[],mainLoader:false,OrderIsLoaded:[],nAprv:[],packLoader:[]}
  btnDisable = [];
  attachedOrders = [];
  RxCards = [];
  deliveryChangeFrom: FormGroup;
  deliveryDate: FormControl;
  constructor(   private http: Http,
    private router: Router,
    public comService : CommonService ,
    private prnt:PrintService,
    public auth : AuthService,
    public AppComponent : AppComponent,
    public ConService : ConversationService  
   
  ) { }

  ngOnInit() {
    this.comService.reorder = [];
    this.getReOrderTicket();
    this.deliveryDate = new FormControl('', Validators.required);
    this.deliveryChangeFrom = new FormGroup({deliveryDate: this.deliveryDate});
   
  }
  
  /*==================================================================
  WRITTEN BY:SEAMLESS CARE
  DECRIPTION: GET GENERAL TICKETS
  ===================================================================*/
  getReOrderTicket(){
    this.status.mainLoader = true;
    const header = new Headers();
    header.append("Content-Type", "application/json");
    const options = new RequestOptions({headers: header});
    this.http.get(Constant.API_URL+'ticket/reorders',options).map(res => res.json()).subscribe(data => {
            this.comService.reorder = data;
            this.status.mainLoader = false;
        }, err => {
           console.log(err.message);
           this.status.mainLoader = false;
    });
  }

  /*==================================================================
  WRITTEN BY:
  DECRIPTION: Get Attached order with spacific ticket
  ===================================================================*/

  getRxCards(ticketID){
    if(!this.status.OrderIsLoaded[ticketID]){
      this.status.loader[ticketID] = true; // loader
      const header = new Headers();
      header.append("Content-Type", "application/json");
      const options = new RequestOptions({headers: header});
      this.http.get(Constant.API_URL+'reorder/RxCards/'+ticketID,options).map(res => res.json()).subscribe(data => {
          this.RxCards[ticketID]= data;
          this.status.OrderIsLoaded[ticketID] = true;
          this.status.loader[ticketID] = false;
       //   e.path[1].nextElementSibling.style.display = "block"; 
          }, err => {
            this.status.loader[ticketID] = false;
      });
    }else{
    //  e.path[1].nextElementSibling.style.display =  (e.path[1].nextElementSibling.style.display == "none")?"block":"none";
      this.status.loader[ticketID] = false;
    }
  }

  manageTicketMove(ticketId){
    
      var ticketIndex =  this.comService.indexWhere(this.comService.reorder, item=>item.PharmacyTicketID === ticketId);
      this.comService.reorder.splice(ticketIndex,1);
      this.comService.navCounter.Tickets = this.comService.navCounter.Tickets - parseInt('1');
  }

  /*==================================================================
  WRITTEN BY:SEAMLESS CARE
  DECRIPTION: TICKET COMPLETE 
  ===================================================================*/

  completeTicket(ticketId){
    this.btnDisable[ticketId] = true;
    var  body ={"TicketID":ticketId, "UserID": this.auth.authCheck()['user_id']}
    const header = new Headers();
    header.append("Content-Type", "application/json");
    const options = new RequestOptions({headers: header});
    this.http.put(Constant.API_URL+'ticket/complete',body,options).map(res => res.json()).subscribe(data => {
      this.manageTicketMove(ticketId)
        }, err => {
          this.btnDisable[ticketId] = false;
    });
 }

 public currentTicketUpdating = 0;
 changeDeliveryDate(ticketID,date){
   this.currentTicketUpdating = ticketID;
   this.deliveryChangeFrom.controls['deliveryDate'].setValue(new Date(this.comService.dateFormater(date)));
 }
 public updatingDeliveryDate:Boolean;
 updateDeliveryDate(){
    if(this.deliveryChangeFrom.valid){
      this.updatingDeliveryDate=true;
      this.deliveryChangeFrom.value.PharmacyTicketID = this.currentTicketUpdating;
      this.deliveryChangeFrom.value.deliveryDate = this.comService.dateFormatWithTime(this.deliveryChangeFrom.value.deliveryDate);
      this.http.put(Constant.API_URL+'updateTicketDeliveryDate',this.deliveryChangeFrom.value).map(res => res.json()).subscribe(data => {
          var ticketIndex =  this.comService.indexWhere(this.comService.reorder, item=>item.PharmacyTicketID === this.currentTicketUpdating);
          this.comService.closeModel('CloseButton1');
          this.comService.reorder[ticketIndex].DeliveryDate = this.deliveryChangeFrom.value.deliveryDate;
         this.currentTicketUpdating = 0;
          this.updatingDeliveryDate=false;
        
          }, err => {
            this.updatingDeliveryDate=false;
      });
    }
  }

}
