import { Component, OnInit } from '@angular/core';
import {Constant} from '../../../../constant';
import {Http, Response, Headers, RequestOptions} from '@angular/http';
import { ReactiveFormsModule, FormsModule, FormGroup, FormControl, Validators,FormBuilder} from '@angular/forms';
import 'rxjs/add/operator/map';
import {Router} from '@angular/router';
import { DatePipe } from '@angular/common';
import { PrintService } from '../../../../service/print.service';
import { CommonService } from '../../../../service/common.service';
import { AuthService } from '../../../../service/auth.service';
import { AppComponent } from '../../../../app.component'
import { ConversationService } from '../../../../service/conversation.service';


@Component({
  selector: 'app-medchange',
  templateUrl: './medchange.component.html',
  styleUrls: ['./medchange.component.scss']
})
export class MedchangeComponent implements OnInit {
  status = {loader:[],loadAttachedOrder:[],mainLoader:false,nAprv:[],packLoader:[]}
  btnDisable=[];
  btnDisablePre= [];
  attachTicketMainIcon = [];
  orderDetailLabel = ['CHANGES :','ACTION :','START DATE : ','END DATE : ','DELIVERY DATE :', 'STRIP :'];
  deliveryChangeFrom: FormGroup;
  deliveryDate: FormControl;
  constructor(
    private http: Http,
    private router: Router,
    public comService : CommonService ,
    private prnt:PrintService,
    public auth : AuthService,
    public AppComponent : AppComponent,
    public ConService : ConversationService  
       
  
  ){}

  ngOnInit() {
    this.comService.medChangeTicket = [];
    this.getMedChangeTicket();
    this.deliveryDate = new FormControl('', Validators.required);
    this.deliveryChangeFrom = new FormGroup({deliveryDate: this.deliveryDate});
  }


  /*==================================================================
  WRITTEN BY:SEAMLESS CARE
  DECRIPTION: GET MEDCHNAGES TICKETS
  ===================================================================*/
  getMedChangeTicket(){
    this.status.mainLoader =true;
    const header = new Headers();
    header.append("Content-Type", "application/json");
    const options = new RequestOptions({headers: header});
    this.http.get(Constant.API_URL+'ticket/medchanges',options).map(res => res.json()).subscribe(data => {
            this.comService.medChangeTicket = data;
            this.status.mainLoader=false;
        }, err => {
           this.status.mainLoader=false;
    });
  }

  getOrders(e){
    e.path[1].nextElementSibling.style.display =  (e.path[1].nextElementSibling.style.display == "none" || e.path[1].nextElementSibling.style.display == "")?"block":"none";
    this.attachTicketMainIcon[e.target.rel] =  (this.attachTicketMainIcon[e.target.rel])?false:true;
    // this.attachTicketMainIcon[e.target.rel] = true;
  }

  manageTicketMove(ticketId){
    var ticketIndex =  this.comService.indexWhere(this.comService.medChangeTicket, item=>item.PharmacyTicketID === ticketId);
    this.comService.medChangeTicket.splice(ticketIndex,1);
    this.comService.navCounter.Tickets = this.comService.navCounter.Tickets - parseInt('1');

}

   /*==================================================================
  WRITTEN BY:SEAMLESS CARE
  DECRIPTION: ORDER APPROVAL IN CASE MED CHANGES
  ===================================================================*/

  orderPreApproval(ticketId){
    
    this.btnDisablePre[ticketId] = true;
      var  body ={"ticketId":ticketId, "UserID": this.auth.authCheck()['user_id']}
      const header = new Headers();
      header.append("Content-Type", "application/json");
      const options = new RequestOptions({headers: header});
      this.http.put(Constant.API_URL+'ticket/preApproval',body,options).map(res => res.json()).subscribe(data => {
            var ordIndex =  this.comService.indexWhere(this.comService.medChangeTicket, item=>item.PharmacyTicketID === ticketId);
           this.comService.medChangeTicket[ordIndex].UserIDPreApproval = this.auth.authCheck()['user_id'];
           this.btnDisablePre[ticketId] = false;
          }, err => {
            this.btnDisablePre[ticketId] = false;
      });
   }

  /*==================================================================
  WRITTEN BY:SEAMLESS CARE
  DECRIPTION: TICKET COMPLETE 
  ===================================================================*/

  completeTicket(ticketId){
    this.btnDisable[ticketId] = true;
    var  body ={"TicketID":ticketId, "UserID": this.auth.authCheck()['user_id']}
    const header = new Headers();
    header.append("Content-Type", "application/json");
    const options = new RequestOptions({headers: header});
    this.http.put(Constant.API_URL+'ticket/complete',body,options).map(res => res.json()).subscribe(data => {
       this.manageTicketMove(ticketId)
       this.btnDisable[ticketId] = false;
        }, err => {
          this.btnDisable[ticketId] = false;
    });
 }

  /*==================================================================
  WRITTEN BY:SEAMLESS CARE
  DECRIPTION: SET DELIVERY DATE
  ===================================================================*/
 public currentTicketUpdating = 0;
 changeDeliveryDate(ticketID,date){
   this.currentTicketUpdating = ticketID;
   this.deliveryChangeFrom.controls['deliveryDate'].setValue(new Date(this.comService.dateFormater(date)));
 }

  /*==================================================================
  WRITTEN BY:SEAMLESS CARE
  DECRIPTION: UPDATING DELIVERY DATE
  ===================================================================*/
public updatingDeliveryDate:Boolean;
 updateDeliveryDate(){
    if(this.deliveryChangeFrom.valid){
      this.updatingDeliveryDate= true;
      this.deliveryChangeFrom.value.PharmacyTicketID = this.currentTicketUpdating;
      this.deliveryChangeFrom.value.deliveryDate = this.comService.dateFormatWithTime(this.deliveryChangeFrom.value.deliveryDate);
      this.http.put(Constant.API_URL+'updateTicketDeliveryDate',this.deliveryChangeFrom.value).map(res => res.json()).subscribe(data => {
            var ticketIndex =  this.comService.indexWhere(this.comService.medChangeTicket, item=>item.PharmacyTicketID === this.currentTicketUpdating);
            this.comService.medChangeTicket[ticketIndex].DeliveryDate = this.deliveryChangeFrom.value.deliveryDate;
            this.currentTicketUpdating = 0;
            this.updatingDeliveryDate= false;
            this.comService.closeModel('CloseButton12');
          }, err => {
            this.updatingDeliveryDate= false;
      });
    }
  }

  jsonParse(obj){
    return JSON.parse(obj);
   }
}
