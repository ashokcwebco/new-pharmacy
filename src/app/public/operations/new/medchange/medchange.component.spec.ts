import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MedchangeComponent } from './medchange.component';

describe('MedchangeComponent', () => {
  let component: MedchangeComponent;
  let fixture: ComponentFixture<MedchangeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MedchangeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MedchangeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
