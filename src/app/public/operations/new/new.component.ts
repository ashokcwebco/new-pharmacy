import { Component, OnInit } from '@angular/core';
import {Constant} from '../../../constant';
import {Http, Response, Headers, RequestOptions} from '@angular/http';
import { ReactiveFormsModule, FormsModule, FormGroup, FormControl, Validators,FormBuilder} from '@angular/forms';
import 'rxjs/add/operator/map';
import {Router} from '@angular/router';
import { DatePipe } from '@angular/common';
import { CommonService } from '../../../service/common.service';
import { AuthService } from '../../../service/auth.service';
import { AppComponent } from '../../../app.component';

@Component({
  selector: 'app-dashboard',
  templateUrl: './new.component.html',
  styleUrls: ['./new.component.scss']
})
export class DashboardComponent implements OnInit {
  status = {loader:[],loadAttachedOrder:[],nAprv:[]}
  attachTicketMainIcon = [];
  attachTicketInnerIcon =[]; 
  attachedMedications = [];
  packages;
  attachedOrders= [];
  constructor(
    private http: Http,
    private router: Router,
    public comService : CommonService ,
    public auth : AuthService,
    public AppComponent : AppComponent   
  
  ) {this.comService.returnClass = []; }

  
  ngOnInit() {
    // this.comService.navCounter.new=0;
    this.comService.activeNav('new');
    this.AppComponent.header_changes();
        //Auth Check -----------------------------
        if(!this.auth.authCheck()) {
          this.router.navigate(['/login']);
          return;
        }
        console.log(this.auth.authCheck());
       //--------------------------------
    this.getNewCard();
  
  }

  /*==================================================================
  WRITTEN BY:SEAMLESS CARE
  DECRIPTION: GET REORDER TICKETS
  ===================================================================*/


  

  /*==================================================================
  WRITTEN BY:
  DECRIPTION: get new ticket created 
  ===================================================================*/
  getNewCard(){
   // this.comService.showLoading();
    const header = new Headers();
    header.append("Content-Type", "application/json");
    const options = new RequestOptions({headers: header});
    this.http.get(Constant.API_URL+'ticket/ticket-card',options).map(res => res.json()).subscribe(data => {
            this.comService.newCards = data;
     //       this.comService.showLoading(false);
        }, err => {
           console.log(err.message);
       //    this.comService.showLoading(false);
    });
  }

  /*==================================================================
  WRITTEN BY:
  DECRIPTION: Get Attached order with spacific ticket
  ===================================================================*/

  getOrders(e){
    if(!this.status.loadAttachedOrder[e.target.rel]){
      this.status.loader[e.target.rel] = true; // loader
      const header = new Headers();
      header.append("Content-Type", "application/json");
      const options = new RequestOptions({headers: header});
      this.http.get(Constant.API_URL+'ticket/orders/'+e.target.rel,options).map(res => res.json()).subscribe(data => {
          this.attachedOrders[e.target.rel]= data;
          this.status.loadAttachedOrder[e.target.rel] = true;
          this.status.loader[e.target.rel] = false;
          e.path[1].nextElementSibling.style.display = "block"; 
          this.attachTicketMainIcon[e.target.rel] = true; 
     
          }, err => {
            this.status.loader[e.target.rel] = false;
      });
    }else{
      e.path[1].nextElementSibling.style.display =  (e.path[1].nextElementSibling.style.display == "none")?"block":"none";
      this.attachTicketMainIcon[e.target.rel] =  (e.path[1].nextElementSibling.style.display == "none")?false:true;

      this.status.loader[e.target.rel] = false;
    }
    
  }



  ticketAction(obj) {
    if(obj.TicketType !=4)
       this.router.navigate(['../attach', {ticketId: obj.PharmacyTicketID,type:obj.TicketType}],{ preserveQueryParams: true });
    else
      this.router.navigate(['../order-batch', {ticketId: obj.PharmacyTicketID,type:obj.TicketType}],{ preserveQueryParams: true });
    
  }
  
}
