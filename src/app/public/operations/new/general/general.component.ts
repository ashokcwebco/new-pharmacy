import { Component, OnInit, Pipe } from '@angular/core';
import {Constant} from '../../../../constant';
import {Http, Response, Headers, RequestOptions} from '@angular/http';
import { ReactiveFormsModule, FormsModule, FormGroup, FormControl, Validators,FormBuilder} from '@angular/forms';
import 'rxjs/add/operator/map';
import {Router} from '@angular/router';
import { DatePipe } from '@angular/common';
import { PrintService } from '../../../../service/print.service';
import { CommonService } from '../../../../service/common.service';
import { AuthService } from '../../../../service/auth.service';
import { AppComponent } from '../../../../app.component';
import { ConversationService } from '../../../../service/conversation.service';

@Component({
  selector: 'app-general',
  templateUrl: './general.component.html',
  styleUrls: ['./general.component.scss']
})
export class GeneralComponent implements OnInit {
  status = {loader:[],mainLoader:false,loadAttachedOrder:[],nAprv:[],packLoader:[]}
  btnDisable=[];
  btnDisableApp = [];
  attachTicketMainIcon = [];
  deliveryChangeFrom: FormGroup;
  deliveryDate: FormControl;
  
  constructor(
    private http: Http,
    private router: Router,
    private prnt:PrintService,
    public comService : CommonService ,
    public auth : AuthService,
    public AppComponent : AppComponent ,
    public ConService : ConversationService  
  
  ){ }

  ngOnInit() {
    this.getGeneralTicket();
    this.comService.general= [];
    this.deliveryDate = new FormControl('', Validators.required);
    this.deliveryChangeFrom = new FormGroup({deliveryDate: this.deliveryDate});
  }

    /*==================================================================
  WRITTEN BY:SEAMLESS CARE
  DECRIPTION: GET GENERAL TICKETS
  ===================================================================*/
  getGeneralTicket(){
    this.status.mainLoader = true;
    const header = new Headers();
    header.append("Content-Type", "application/json");
    const options = new RequestOptions({headers: header});
    this.http.get(Constant.API_URL+'ticket/general',options).map(res => res.json()).subscribe(data => {
            this.comService.general = data;
            this.status.mainLoader = false;
        }, err => {
           this.status.mainLoader = false;
    });
  }

  manageTicketMove(ticketId){

      var ticketIndex =  this.comService.indexWhere(this.comService.general, item=>item.PharmacyTicketID === ticketId);
      this.comService.general.splice(ticketIndex,1);
      this.comService.navCounter.Tickets = this.comService.navCounter.Tickets - parseInt('1');
  
  }

  /*==================================================================
  WRITTEN BY:SEAMLESS CARE
  DECRIPTION: TICKET COMPLETE 
  ===================================================================*/

  completeTicket(ticketId){
    this.btnDisable[ticketId] = true;
    var  body ={"TicketID":ticketId, "UserID": this.auth.authCheck()['user_id']}
    const header = new Headers();
    header.append("Content-Type", "application/json");
    const options = new RequestOptions({headers: header});
    this.http.put(Constant.API_URL+'ticket/complete',body,options).map(res => res.json()).subscribe(data => {
      this.manageTicketMove(ticketId)
        }, err => {
          this.btnDisable[ticketId] = false;
    });
 }

 public currentTicketUpdating = 0;
 changeDeliveryDate(ticketID,date){
   this.currentTicketUpdating = ticketID;
   this.deliveryChangeFrom.controls['deliveryDate'].setValue(new Date(date));
 }
public updatingDeliveryDate:Boolean;
 updateDeliveryDate(){
    if(this.deliveryChangeFrom.valid){
      this.updatingDeliveryDate = true;
      this.deliveryChangeFrom.value.PharmacyTicketID = this.currentTicketUpdating;
      this.deliveryChangeFrom.value.deliveryDate = this.comService.dateFormatWithTime(this.deliveryChangeFrom.value.deliveryDate);
      this.http.put(Constant.API_URL+'updateTicketDeliveryDate',this.deliveryChangeFrom.value).map(res => res.json()).subscribe(data => {
            var ticketIndex =  this.comService.indexWhere(this.comService.general, item=>item.PharmacyTicketID === this.currentTicketUpdating);
            this.comService.general[ticketIndex].DeliveryDate = this.deliveryChangeFrom.value.deliveryDate;
            this.updatingDeliveryDate = false;
            this.currentTicketUpdating = 0;
            this.comService.closeModel('CloseButton1');
          }, err => {
             this.updatingDeliveryDate = false;
            
      });
    }
  }

}
