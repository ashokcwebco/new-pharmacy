import { Component,ViewContainerRef, OnInit } from '@angular/core';
import {Constant} from '../../../../constant';
import {Http, Response, Headers, RequestOptions} from '@angular/http';
import { ReactiveFormsModule, FormsModule, FormGroup, FormControl, Validators,FormBuilder} from '@angular/forms';
import { AuthService } from '../../../../service/auth.service';
import { DatePipe } from '@angular/common';
import { CommonService } from '../../../../service/common.service';
import { PrintService } from '../../../../service/print.service';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { ConversationService } from '../../../../service/conversation.service';
import { debug } from 'util';
@Component({
  selector: 'app-new-run',
  templateUrl: './new-run.component.html',
  styleUrls: ['./new-run.component.scss']
})
export class NewRunComponent implements OnInit {
  showlist = true;usersList;selectedPatient = {user_id:''};
  status = {loader:[],packageLoader:[],mainLoader:false, pkgLoader:[],ispkgLoaded:[],packageOrderLoad:[], packageOrderLoaded:[]}
  deliveryPkgOrders=[];
  private packages =[];
  public deliveryPkgCollapse= [];
  newRunForm: FormGroup;
  DriverUserID: FormControl;
  DateScheduled: FormControl;
  Title: FormControl;
  constructor( private http: Http,
    public auth : AuthService ,
    public comService : CommonService,
    public ConService : ConversationService ,
    private prnt:PrintService,
    public toastr: ToastsManager,
    vcr: ViewContainerRef ) {this.toastr.setRootViewContainerRef(vcr); }

  ngOnInit() {
    this.getDrivers();
    this.formNewRunRole();
    this.fromNewRunData();
    this.comService.newRuns = [];
    this.getDeliveryRuns();
  }

  /*==================================================================
  WRITTEN BY:
  DECRIPTION: define validation for creating new Run 
  ===================================================================*/
  formNewRunRole() {
    this.DateScheduled = new FormControl('', Validators.required);
    this.DriverUserID = new FormControl('', Validators.required);
    this.Title = new FormControl('', Validators.required);
  
  }
  /*==================================================================
  WRITTEN BY:
  DECRIPTION:: set valuein form group 
  ===================================================================*/
  fromNewRunData() {
    this.newRunForm = new FormGroup({
      DriverUserID: this.DriverUserID,
      DateScheduled: this.DateScheduled,
      Title: this.Title
    });
  }

  /*==================================================================
  WRITTEN BY:
  DECRIPTION: Filter patient to assign a new ticket
  ===================================================================*/
  filterDriver(e){
    const header = new Headers();
    header.append("Content-Type", "application/json");
    const options = new RequestOptions({headers: header});
    this.http.get(Constant.API_URL+'getUserFilter/'+e.target.value,options).map(res => res.json()).subscribe(data => { 
        this.showlist = true; 
        this.usersList = data;
    }, err => {
        console.log(err.message);
    });
  }

  selectDriver(obj){
    this.newRunForm.controls['DriverUserID'].setValue(obj.userName);
    this.selectedPatient = obj;
    this.showlist = false;
  }

   updateCurrentRunObj(obj){
   var ind =  this.comService.indexWhere(this.comService.newRuns, item=>item.DeliveryRunID === this.updateRunID);
   this.comService.newRuns[ind].DateScheduled = obj.DateScheduled;
   this.comService.newRuns[ind].DriverUserID = this.selectedPatient.user_id;
   this.comService.newRuns[ind].Title = obj.Title;
   this.comService.newRuns[ind].userName = obj.DriverUserID;
   }

public saveNewRun:Boolean;
  onSubmit(){
    if(this.newRunForm.valid){
      // this.newRunForm.value.DriverUserID = this.selectedPatient.user_id;
      this.newRunForm.value.UserScheduled = this.auth.authCheck()['user_id'];
      this.newRunForm.value.DateScheduled = this.comService.dateFormatWithTime(this.newRunForm.value.DateScheduled);
      this.saveNewRun = true;
      if(this.updateRunID){
        this.newRunForm.value.DeliveryPkgId = this.updateRunID;
        this.http.put(Constant.API_URL+'updateRun',this.newRunForm.value).map(res => res.json()).subscribe(data => {
          this.saveNewRun = false;
          this.updateCurrentRunObj(this.newRunForm.value);
            this.comService.closeModel('CloseButton1')
            this.toastr.success("Run update successfully.", 'Success!');
        }, err => {
          this.saveNewRun = false;
        });
      }else{
          this.http.post(Constant.API_URL+'createNewRun',this.newRunForm.value).map(res => res.json()).subscribe(data => {
            this.comService.newRuns.unshift(data[0]);
            this.comService.runList.push(data[0]);
            this.saveNewRun = false;
            this.comService.closeModel('CloseButton1')
            this.toastr.success("New run saved successfully.", 'Success!');
          }, err => {
            this.saveNewRun = false;
          });
          this.newRunForm.reset();
      }
    }else{
      this.toastr.error("Please fill all mandatory fields.", 'Oops!');
    }
  }

/*==================================================================
WRITTEN BY: SEAMLESS CARE
DECRIPTION:GET DELIVERY RUN 
===================================================================*/
  getDeliveryRuns(){
    this.status.mainLoader = true;
    this.http.get(Constant.API_URL+'getDeliveryRun').map(res => res.json()).subscribe(data => { 
      this.comService.newRuns = data;
      this.status.mainLoader = false;
    }, err => {
        console.log(err.message);
        this.status.mainLoader = false;
    });
  }

/*==================================================================
WRITTEN BY: SEAMLESS CARE
DECRIPTION:GET DELIVERY PACKAGES IN RUN 
===================================================================*/
  getPackageInRun(RunID){
    if(!this.status.ispkgLoaded[RunID]){
      this.status.pkgLoader[RunID] = true;
      this.status.ispkgLoaded[RunID] = true;
      this.http.get(Constant.API_URL+'getPackageInRun/'+RunID).map(res => res.json()).subscribe(data => { 
        this.status.pkgLoader[RunID] = false
        this.packages[RunID] = data;
      }, err => {
        this.status.pkgLoader[RunID] = false
          console.log(err.message);
      });
    }
  }

/*==================================================================
WRITTEN BY: SEAMLESS CARE
DECRIPTION:GET ALL DRIVER LIST
===================================================================*/
public driverData = [];
public drivers=[];
getDrivers(){
  this.http.get(Constant.API_URL+'getDrivers').map(res => res.json()).subscribe(data => { 
    this.drivers = data;
  }, err => {
  });
}

/*==================================================================
WRITTEN BY: SEAMLESS CARE
DECRIPTION:UPDETE DRIVER
===================================================================*/
public updatingDriver = [];
updateDriver(e,RunID){
  this.updatingDriver[RunID] = true;
  var body = {"DriverUserID":parseInt(e.target.value),"RunID":RunID};
  this.http.put(Constant.API_URL+'updateRunDriver',body).map(res => res.json()).subscribe(data => {
    this.updatingDriver[RunID] = false;
  },err=>{
    this.updatingDriver[RunID] = false;
  })

}


setSelectedDriver(driverId,RunID){
  this.driverData[RunID] = driverId;
}
/*==================================================================
WRITTEN BY: SEAMLESS CARE
DECRIPTION:GET DELIVERY PACKAGES IN RUN 
===================================================================*/
  getDeliveryPackgeDOrders(pkgID){
    if(!this.status.packageOrderLoaded[pkgID]){
        this.status.packageOrderLoad[pkgID] = true;
        this.status.packageOrderLoaded[pkgID] = true;
        this.http.get(Constant.API_URL+'deliveryPkgsOrders/'+pkgID).map(res => res.json()).subscribe(data => {
          this.deliveryPkgOrders[pkgID] = data;
          this.status.packageOrderLoad[pkgID] = false;
        }, err => {
          this.status.packageOrderLoad[pkgID] = false;
          console.log(err);
      });
    }
  }

  /*==================================================================
  WRITTEN BY:SEAMLESS CARE
  DECRIPTION: GET MEDICATION OF SPACIFIC ORDER
  ===================================================================*/
  public medication =[];
  public loadReq =[];
  getMedication(OrderID){
    if(!this.loadReq[OrderID]){
      this.loadReq[OrderID] = true;
        this.http.get(Constant.API_URL+'getMedication/'+OrderID).map(res => res.json()).subscribe(data => {
          this.medication[OrderID] = data;
         }, err => {
           console.log(err);
         });
    }
  }
  
  checkRunPackage(Run){
    this.http.get(Constant.API_URL+'getPackageInRun/'+Run.DeliveryRunID).map(res => res.json()).subscribe(data => { 
      if(data.length > 0)
        this.startRun(Run);
        else
        this.toastr.error('No packages in run Please load run.', 'Sorry!');
    }, err => {
            console.log(err.message);
    });
  }



  startRun(Run){
    var StartedBy = this.auth.authCheck()['user_id'];
    this.http.get(Constant.API_URL+'getStartRun/'+Run.DeliveryRunID+'/'+StartedBy).map(res => res.json()).subscribe(data => { 
      if(data.length > 0){
        this.comService.deliveryInTransit.push(data[0]);
        var indx =  this.comService.indexWhere(this.comService.newRuns ,item=>item.DeliveryRunID === Run.DeliveryRunID);
        this.comService.newRuns.splice(indx,1);;
      }
      this.toastr.success("Run is starting.", 'Success!');
    }, err => {
        console.log(err.message);
    });

  }

/*==================================================================
WRITTEN BY: SEAMLESS CARE
DECRIPTION:GET DELIVERY PACKAGES IN RUN 
===================================================================*/
setValues(data){
  // this.selectedPatient.user_id = data.DriverUserID;
  this.newRunForm.controls['DateScheduled'].setValue(new Date(data.DateScheduled));
  this.newRunForm.controls['Title'].setValue(data.Title);
  this.newRunForm.controls['DriverUserID'].setValue(data.DriverUserID);
  this.updateRunID = data.DeliveryRunID;
}

/*==================================================================
WRITTEN BY: SEAMLESS CARE
DECRIPTION:GET RUN DATA FOR EDIT RUN
===================================================================*/
public loadingSingleRun:Boolean;
  getRun(RunID){
    this.loadingSingleRun = true;
    this.newRunForm.reset();
    this.http.get(Constant.API_URL+'getRun/'+RunID).map(res => res.json()).subscribe(data => {
        this.setValues(data);
        this.loadingSingleRun = false;
      }, err => {
        console.log(err);
        this.loadingSingleRun= false;
    });
  }
/*==================================================================
WRITTEN BY: SEAMLESS CARE
DECRIPTION:GET RESET RUN FROM
===================================================================*/
  resetForm(){
    this.newRunForm.reset();
  }

  jsonParse(obj){
    return JSON.parse(obj);
   }
public updateRunID = '';

}
