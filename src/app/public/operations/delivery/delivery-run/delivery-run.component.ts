import { Component, OnInit } from '@angular/core';
import {Constant} from '../../../../constant';
import {Router} from '@angular/router';
import { CommonService } from '../../../../service/common.service';
import { AuthService } from '../../../../service/auth.service';

@Component({
  selector: 'app-delivery-run',
  templateUrl: './delivery-run.component.html',
  styleUrls: ['./delivery-run.component.scss']
})
export class DeliveryRunComponent implements OnInit {

  constructor(
    private router: Router,
    public comService : CommonService ,
    public auth : AuthService
  ) { }

  ngOnInit() {
    this.comService.activeNav('deliveryRun');
  }

}
