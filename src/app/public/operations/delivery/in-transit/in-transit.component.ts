import { Component,ViewContainerRef, OnInit } from '@angular/core';
import {Constant} from '../../../../constant';
import {Http, Response, Headers, RequestOptions} from '@angular/http';
import { ReactiveFormsModule, FormsModule, FormGroup, FormControl, Validators,FormBuilder} from '@angular/forms';
import { AuthService } from '../../../../service/auth.service';
import { DatePipe } from '@angular/common';
import { CommonService } from '../../../../service/common.service';
import { PrintService } from '../../../../service/print.service';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { ConversationService } from '../../../../service/conversation.service';
import Swal from 'sweetalert2';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';

@Component({
  selector: 'app-in-transit',
  templateUrl: './in-transit.component.html',
  styleUrls: ['./in-transit.component.scss']
})
export class InTransitComponent implements OnInit {
  showlist = true;usersList;selectedPatient;
  status = {loader:[],packageLoader:[],mainLoader:false, pkgLoader:[],ispkgLoaded:[],packageOrderLoad:[], packageOrderLoaded:[]}
  deliveryPkgOrders=[];
  // private isReturn = [];
  private packages =[];
  public deliveryPkgCollapse= [];
  constructor( private http: Http,
    public auth : AuthService ,
    private prnt:PrintService,
    public comService : CommonService,
    public ConService : ConversationService ,
    public toastr: ToastsManager,
    vcr: ViewContainerRef) { this.toastr.setRootViewContainerRef(vcr);}

  ngOnInit() {
    document.body.addEventListener('keyboardSequence', function (this,e) {
      document.getElementById("inputBarcode")['value'] ='';
      document.getElementById("inputBarcode")['value'] = e["detail"].sequence;
      document.getElementById("inputBarcode")['action'] = "return";
      document.getElementById("inputBarcode").click();
    });
    this.comService.deliveryInTransit = [];
    this.getDeliveryInTransit();
  }


/*==================================================================
WRITTEN BY: SEAMLESS CARE
DECRIPTION:GET DELIVERY RUN 
===================================================================*/
  getDeliveryInTransit(){
    this.status.mainLoader = true;
    this.http.get(Constant.API_URL+'getDeliveryInTransit').map(res => res.json()).subscribe(data => { 
      this.comService.deliveryInTransit = data;
      this.status.mainLoader = false;
    }, err => {
      this.status.mainLoader = false;
        console.log(err.message);
    });
  }

/*==================================================================
WRITTEN BY: SEAMLESS CARE
DECRIPTION:GET DELIVERY PACKAGES IN RUN 
===================================================================*/
  getPackageInRun(RunID){
    if(!this.status.ispkgLoaded[RunID]){
      this.status.pkgLoader[RunID] = true;
      this.status.ispkgLoaded[RunID] = true;
      this.http.get(Constant.API_URL+'getPackageInRun/'+RunID).map(res => res.json()).subscribe(data => { 
        this.status.pkgLoader[RunID] = false
        this.packages[RunID] = data;
      }, err => {
        this.status.pkgLoader[RunID] = false
          console.log(err.message);
      });
    }
  }

  /*==================================================================
  WRITTEN BY:SEAMLESS CARE
  DECRIPTION: GET MEDICATION OF SPACIFIC ORDER
  ===================================================================*/
  public medication =[];
  public loadReq =[];
  getMedication(OrderID){
    if(!this.loadReq[OrderID]){
      this.loadReq[OrderID] = true;
        this.http.get(Constant.API_URL+'getMedication/'+OrderID).map(res => res.json()).subscribe(data => {
          this.medication[OrderID] = data;
         }, err => {
           console.log(err);
         });
    }
  }


  getDeliveryPackgeDOrders(pkgID){
    if(!this.status.packageOrderLoaded[pkgID]){
        this.status.packageOrderLoad[pkgID] = true;
        this.status.packageOrderLoaded[pkgID] = true;
        this.http.get(Constant.API_URL+'deliveryPkgsOrders/'+pkgID).map(res => res.json()).subscribe(data => {
          this.deliveryPkgOrders[pkgID] = data;
          this.status.packageOrderLoad[pkgID] = false;
        }, err => {
          this.status.packageOrderLoad[pkgID] = false;
          console.log(err);
      });
    }
  }

  returnPkg(pkg){
    this.toastr.info("Please scan '"+pkg.Title+"' Package.");
    
  }


  jsonParse(obj){
    return JSON.parse(obj);
   }

  deliveryFinalize(Run){
    var StartedBy = this.auth.authCheck()['user_id'];
    this.http.get(Constant.API_URL+'deliveryFinalize/'+Run.DeliveryRunID+'/'+StartedBy).map(res => res.json()).subscribe(data => {
      if(data.length > 0){
          this.comService.deliveryFinlize.unshift(data[0]); 
          var indx =  this.comService.indexWhere(this.comService.deliveryInTransit ,item=>item.DeliveryRunID ===Run.DeliveryRunID);
          this.comService.deliveryInTransit.splice(indx,1);
      }
      this.toastr.success("Run finalize successfully.", 'Success!');
    }, err => {
        console.log(err.message);
    });
  }

  // deliveryReturn(Run){
  //   this.http.get(Constant.API_URL+'deliveryReturn/'+Run.DeliveryRunID).map(res => res.json()).subscribe(data => {
  //     if(data.length > 0){
  //       this.comService.deliveryFinlize.unshift(data[0]);
  //       var indx =  this.comService.indexWhere(this.comService.deliveryInTransit ,item=>item.DeliveryRunID ===Run.DeliveryRunID);
  //       this.comService.deliveryInTransit.splice(indx,1);;
  //     } 
  //     alert('returned');
  //   }, err => {
  //       console.log(err.message);
  //   });
  // }

}
