import { Component,ViewContainerRef, OnInit } from '@angular/core';
import {Http, Response, Headers, RequestOptions} from '@angular/http';
import { ReactiveFormsModule, FormsModule, FormGroup, FormControl, Validators,FormBuilder} from '@angular/forms';
import { CommonService } from '../../../../service/common.service';
import { AuthService } from '../../../../service/auth.service';
import {Constant} from '../../../../constant';
import { PrintService } from '../../../../service/print.service';
import { ConversationService } from '../../../../service/conversation.service';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
@Component({
  selector: 'app-due-today',
  templateUrl: './due-today.component.html',
  styleUrls: ['./due-today.component.scss']
})
export class DueTodayComponent implements OnInit {
  status = {loader:[],loadAttachedOrder:[],nAprv:[], mainLoader:false, packageLoad:[],packageErr:[],pkgLoaded:[],packageOrderLoad:[], packageOrderLoaded:[]}
  private loadReq=[];
  deliveryPkgCollapse = [];
  public medication = [];
  public orderCounter= [];
  public newRuns = [];
  public deliveryPkgOrders = [];
  private currentHome;
  private packages={"DeliveryInstructions":''};
  public addressList = [];
  deliveryRunForm: FormGroup;
  DeliveryRunId: FormControl;
  Title: FormControl;
  Address1: FormControl;
  Address2: FormControl;
  City: FormControl;
  Prov: FormControl;
  Postal: FormControl;
  DeliveryInstructions: FormControl;

  deliveryChangeFrom: FormGroup;
  deliveryDate: FormControl;

  constructor(private http: Http,
  public auth : AuthService ,
  public comService : CommonService,
  public toastr: ToastsManager,
  public ConService : ConversationService,
  private prnt:PrintService,
  vcr: ViewContainerRef
) { this.toastr.setRootViewContainerRef(vcr);}

  ngOnInit() {
    this.comService.deliveryPackages=[];
    this.getInBin();
    this.newDeliveryRunRole();
    this.newDeliveryRunData();
    this.comService.dueTodayBin = [];
    this.comService.dueTodayOrderCard = [];
    this.deliveryDate = new FormControl('', Validators.required);
    this.deliveryChangeFrom = new FormGroup({deliveryDate: this.deliveryDate});
  }

  /*==================================================================
  WRITTEN BY:
  DECRIPTION: define validation for creating new Delivery Run 
  ===================================================================*/
  newDeliveryRunRole() {
    this.DeliveryRunId = new FormControl('', Validators.required);
    this.Title = new FormControl('', Validators.required);
    this.Address1 = new FormControl('');
    this.Address2 = new FormControl('');
    this.City = new FormControl('');
    this.Prov = new FormControl('');
    this.Postal = new FormControl('');
    this.DeliveryInstructions = new FormControl('');
    
  }
  /*==================================================================
  WRITTEN BY:
  DECRIPTION:: set value in form group 
  ===================================================================*/
  newDeliveryRunData() {
    this.deliveryRunForm = new FormGroup({
      DeliveryRunId: this.DeliveryRunId,
      Title: this.Title,
      Address1: this.Address1,
      Address2: this.Address2,
      City: this.City,
      Prov: this.Prov,
      Postal: this.Postal,
      DeliveryInstructions: this.DeliveryInstructions
    });
  }

  /*==================================================================
  WRITTEN BY:SEAMLESS CARE
  DECRIPTION:: GET ALL HOMES AND ITS ORDERS 
  ===================================================================*/
  getInBin(){
    this.status.mainLoader = true;
    const header = new Headers();
    header.append("Content-Type", "application/json");
    const options = new RequestOptions({headers: header});
    this.http.get(Constant.API_URL+'getDueToday', options).map(res => res.json()).subscribe(data => {
        this.comService.dueTodayBin = data[0].Homes;
        this.comService.dueTodayOrderCard=data[0].OrderCards;
        this.status.mainLoader = false;
    }, err => {
      this.status.mainLoader = false;
    });

    this.http.get(Constant.API_URL+'getNewRun').map(res => res.json()).subscribe(data => {
      this.comService.runList = data;
      }, err => {
        console.log(err);
    });
    this.http.get(Constant.API_URL+'getPackages').map(res => res.json()).subscribe(data => {
      this.comService.deliveryPackages = data;
    }, err => {
      console.log(err);
    });
  }

  /*==================================================================
  WRITTEN BY:SEAMLESS CARE
  DECRIPTION: GET MEDICATION OF SPACIFIC ORDER
  ===================================================================*/
  getMedication(OrderID){
    if(!this.loadReq[OrderID]){
      this.loadReq[OrderID] = true;
        this.http.get(Constant.API_URL+'getMedication/'+OrderID).map(res => res.json()).subscribe(data => {
          this.medication[OrderID] = data;
         }, err => {
           console.log(err);
         });
    }
  }

  /*==================================================================
  WRITTEN BY:SEAMLESS CARE
  DECRIPTION:CURRENT HOME OBJECT WE ARE USING SOME ACTION
  ===================================================================*/
  getHome(homeobj){
    this.currentHome = homeobj;
    this.deliveryRunForm.controls["Title"].setValue(homeobj.Title);
    this.deliveryRunForm.controls["Address1"].setValue(homeobj.Address1);
    this.deliveryRunForm.controls["Address2"].setValue(homeobj.Address2);
    this.deliveryRunForm.controls["City"].setValue(homeobj.City);
    this.deliveryRunForm.controls["Postal"].setValue(homeobj.Postal);
    this.deliveryRunForm.controls["Prov"].setValue(homeobj.Prov);
    this.deliveryRunForm.controls["DeliveryInstructions"].setValue(homeobj.DeliveryInstructions);
    
    this.http.get(Constant.API_URL+'HomeAddresses/'+homeobj.HomeID).map(res => res.json()).subscribe(data => {
      this.addressList = data;
    }, err => {
      console.log(err);
    });
    
  }

  /*==================================================================
  WRITTEN BY:SEAMLESS CARE
  DECRIPTION: COPY ADDRESS FOR HOME 
  ===================================================================*/
  shipTo(e:any){
    var indx =  this.comService.indexWhere(this.addressList, item=>item.HomeAddressesID === parseInt(e.target.value));
        this.deliveryRunForm.controls["Title"].setValue(this.addressList[indx].Title);
        this.deliveryRunForm.controls["Address1"].setValue(this.addressList[indx].Address1);
        this.deliveryRunForm.controls["Address2"].setValue(this.addressList[indx].Address2);
        this.deliveryRunForm.controls["City"].setValue(this.addressList[indx].City);
        this.deliveryRunForm.controls["Postal"].setValue(this.addressList[indx].Postal);
        this.deliveryRunForm.controls["Prov"].setValue(this.addressList[indx].Prov);
        this.deliveryRunForm.controls["DeliveryInstructions"].setValue(this.addressList[indx].DeliveryInstructions);
        // this.deliveryRunForm.controls["Prov"].setValue(this.addressList[indx].Prov);
    
  }
  /*==================================================================
  WRITTEN BY:SEAMLESS CARE
  DECRIPTION: CREATE NEW DELIVERY PACKAGE CARD
  ===================================================================*/
  public createDelivery:Boolean;
  onSubmit(){
    if(this.deliveryRunForm.valid){
      this.createDelivery= true;
      this.deliveryRunForm.value.DateToDeliver = this.comService.dateFormat(this.deliveryRunForm.value.DateToDeliver);
      this.deliveryRunForm.value.HomeID = this.currentHome.HomeID;
          this.http.post(Constant.API_URL+'newDeliveryCard',this.deliveryRunForm.value).map(res => res.json()).subscribe(data => {
            this.comService.showLoading(false);
            this.comService.deliveryPackages.unshift(data[0]);
            this.toastr.success('Delivery package successfully created.', 'Success!');
            this.comService.closeModel('CloseButton2');
        }, err => {
          this.createDelivery = false;
        });
          this.deliveryRunForm.reset();
          this.createDelivery = false;
    }else{
      this.toastr.error('Please fill all mandatory fields.', 'Oops!');
    }
  }

  /*==================================================================
  WRITTEN BY:SEAMLESS CARE
  DECRIPTION:UPADTE INSTRUCTIONS CREATED PACKAGES CARD 
  ===================================================================*/
  public updatingDeliveryPkg = [];
  updatePackages(pkg){
    this.updatingDeliveryPkg[pkg.DeliveryPkgId] = true;
    var  body = {"DeliveryInstructions":pkg.DeliveryInstructions,"DeliveryPkgId":pkg.DeliveryPkgId};
    this.http.put(Constant.API_URL+'updateDeliveryPkgs',body).map(res => res.json()).subscribe(data => {
        this.updatingDeliveryPkg[pkg.DeliveryPkgId] = false;
      }, err => {
        this.updatingDeliveryPkg[pkg.DeliveryPkgId] = false;
    });

  }

  /*==================================================================
  WRITTEN BY:SEAMLESS CARE
  DECRIPTION: ORDER DROP IN CREATED PACKAGES  
  ===================================================================*/

  onItemDrop(e: any,pkg) {
    var body={"DeliveryPkgID":pkg.DeliveryPkgId,"OrderID":e.dragData.OrderID};
    const header = new Headers();
    header.append("Content-Type", "application/json");
    const options = new RequestOptions({headers: header});
    this.http.put(Constant.API_URL+'orderMoveInPackage',body, options).map(res => res.json()).subscribe(data => {
      if(data == 1){
        this.getSingleDeliveryOrder(pkg.DeliveryPkgId);
        var orderCradIndx =  this.comService.indexWhere(this.comService.dueTodayOrderCard, item=>item.OrderID === e.dragData.OrderID);
        this.comService.dueTodayOrderCard.splice(orderCradIndx,1);
      }
      }, err => {
    });
  }
  /*==================================================================
  WRITTEN BY:SEAMLESS CARE
  DECRIPTION: GET ORDERS THAT MOVED IN BIN (SINGLE ORDER) push
  ===================================================================*/
  getSingleDeliveryOrder(pkgID){
      this.status.packageOrderLoad[pkgID] = true;
      this.status.packageOrderLoaded[pkgID] = true;
      this.http.get(Constant.API_URL+'deliveryPkgsOrder/'+pkgID).map(res => res.json()).subscribe(data => {
        this.deliveryPkgOrders[pkgID].unshift(data);
        this.status.packageOrderLoad[pkgID] = false;
      }, err => {
        this.status.packageOrderLoad[pkgID] = false;
        console.log(err);
    });
  }

  /*==================================================================
  WRITTEN BY:SEAMLESS CARE
  DECRIPTION: GET ALL PACKAGES 
  ===================================================================*/
  getPackages(e,HomeID){
    if(!this.status.pkgLoaded[HomeID]){
      this.deliveryPkgCollapse[HomeID] = (this.deliveryPkgCollapse[HomeID])?false:true;
      this.status.packageLoad[HomeID] = true;
      this.http.get(Constant.API_URL+'getPackages/'+HomeID).map(res => res.json()).subscribe(data => {
        this.status.packageErr[HomeID] = (data.length < 1)?"No package found":false;
        this.comService.deliveryPackages[HomeID] = data;
        this.status.packageLoad[HomeID] = false;
        this.status.pkgLoaded[HomeID] = true;
        e.path[1].nextElementSibling.style.display = "block"; 
      }, err => {
        this.status.packageLoad[HomeID] = false;
        console.log(err);
      });
    }else{this.deliveryPkgCollapse[HomeID] = (this.deliveryPkgCollapse[HomeID])?false:true;
      e.path[1].nextElementSibling.style.display =  (e.path[1].nextElementSibling.style.display == "none")?"block":"none";
    }
  }

  /*==================================================================
  WRITTEN BY:SEAMLESS CARE
  DECRIPTION: GET ORDERS THAT REDAY TO DLIVER IN THE PACKAGE
  ===================================================================*/
  getDeliveryPackgeDOrders(pkgID){
    if(!this.status.packageOrderLoaded[pkgID]){
        this.status.packageOrderLoad[pkgID] = true;
        this.status.packageOrderLoaded[pkgID] = true;
        this.http.get(Constant.API_URL+'deliveryPkgsOrders/'+pkgID).map(res => res.json()).subscribe(data => {
          this.deliveryPkgOrders[pkgID] = data;
          this.status.packageOrderLoad[pkgID] = false;
        }, err => {
          this.status.packageOrderLoad[pkgID] = false;
          console.log(err);
      });
    }
  }



  /*==================================================================
  WRITTEN BY:SEAMLESS CARE
  DECRIPTION: SEND PACKAGE TO RUN 
  ===================================================================*/
  sendToDeliver(pkg){
   
  
    var  body = {"DeliveryPkgId":pkg.DeliveryPkgId,"UserDelivered":this.auth.authCheck()['user_id']};
    this.http.put(Constant.API_URL+'sendToDeliver',body).map(res => res.json()).subscribe(data => {
      if(data.length > 0){
        this.toastr.success('Package successfully delivery.', 'Success!');
        this.comService.newRuns.push(data[0]);
        var indx =  this.comService.indexWhere(this.comService.deliveryPackages[pkg.HomeID] ,item=>item.DeliveryPkgId ===pkg.DeliveryPkgId);
        this.comService.deliveryPackages[pkg.HomeID][indx].isRun = true;
      }
      }, err => {
        console.log(err);
    });
  }


  /*==================================================================
  WRITTEN BY:SEAMLESS CARE
  DECRIPTION: SET DELIVERY DATE
  ===================================================================*/
  public currentTicketUpdating = 0;
  changeDeliveryDate(ticketID,date){
    this.currentTicketUpdating = ticketID;
    this.deliveryChangeFrom.controls['deliveryDate'].setValue(new Date(this.comService.dateFormater(date)));
  }
 
   /*==================================================================
   WRITTEN BY:SEAMLESS CARE
   DECRIPTION: UPDATING DELIVERY DATE
   ===================================================================*/
 public updatingDeliveryDate:Boolean;
  updateDeliveryDate(){
     if(this.deliveryChangeFrom.valid){
       this.updatingDeliveryDate= true;
       this.deliveryChangeFrom.value.PharmacyTicketID = this.currentTicketUpdating;
       this.deliveryChangeFrom.value.deliveryDate = this.comService.dateFormatWithTime(this.deliveryChangeFrom.value.deliveryDate);
       this.http.put(Constant.API_URL+'updateTicketDeliveryDate',this.deliveryChangeFrom.value).map(res => res.json()).subscribe(data => {
             var ticketIndex =  this.comService.indexWhere(this.comService.inBinOrderCard, item=>item.PharmacyTicketID === this.currentTicketUpdating);
             this.comService.inBinOrderCard[ticketIndex].DeliveryDate = this.deliveryChangeFrom.value.deliveryDate;
             this.currentTicketUpdating = 0;
             this.updatingDeliveryDate= false;
             this.comService.closeModel('CloseButton1341');
           }, err => {
             this.updatingDeliveryDate= false;
       });
     }
   }

  jsonParse(obj){
    return JSON.parse(obj);
   }
}
