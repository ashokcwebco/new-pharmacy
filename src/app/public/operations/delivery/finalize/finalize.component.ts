import { Component,ViewContainerRef, OnInit } from '@angular/core';
import {Constant} from '../../../../constant';
import {Http, Response, Headers, RequestOptions} from '@angular/http';
import { ReactiveFormsModule, FormsModule, FormGroup, FormControl, Validators,FormBuilder} from '@angular/forms';
import { AuthService } from '../../../../service/auth.service';
import { PrintService } from '../../../../service/print.service';
import { DatePipe } from '@angular/common';
import { CommonService } from '../../../../service/common.service';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { ConversationService } from '../../../../service/conversation.service';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';

@Component({
  selector: 'app-finalize',
  templateUrl: './finalize.component.html',
  styleUrls: ['./finalize.component.scss']
})
export class FinalizeComponent implements OnInit {
  showlist = true;usersList;selectedPatient;
  status = {loader:[],packageLoader:[],mainLoader:false, pkgLoader:[],ispkgLoaded:[],packageOrderLoad:[], packageOrderLoaded:[]}
  deliveryPkgOrders=[];
  private packages =[];
  public deliveryPkgCollapse= [];
  constructor( private http: Http,
    public auth : AuthService ,
    private prnt:PrintService,
    public ConService : ConversationService ,
    public comService : CommonService,
    public toastr: ToastsManager,
    vcr: ViewContainerRef) { this.toastr.setRootViewContainerRef(vcr);}

  ngOnInit() {
    this.getDeliveryFinalize();
  }



/*==================================================================
WRITTEN BY: SEAMLESS CARE
DECRIPTION:GET DELIVERY FINALIZING
===================================================================*/
getDeliveryFinalize(){
  this.status.mainLoader = true;
    this.http.get(Constant.API_URL+'getDeliveryFinalize').map(res => res.json()).subscribe(data => { 
      this.comService.deliveryFinlize = data;
      this.status.mainLoader = false;
    }, err => {
      this.status.mainLoader = false;
        console.log(err.message);
    });
  }

/*==================================================================
WRITTEN BY: SEAMLESS CARE
DECRIPTION:GET DELIVERY PACKAGES IN RUN 
===================================================================*/
  getPackageInRun(RunID){
      this.status.pkgLoader[RunID] = true;
      this.http.get(Constant.API_URL+'getPackageInRun/'+RunID).map(res => res.json()).subscribe(data => { 
        this.status.pkgLoader[RunID] = false
        this.packages[RunID] = data;
      }, err => {
        this.status.pkgLoader[RunID] = false
          console.log(err.message);
      });
   
  }
  /*==================================================================
  WRITTEN BY:SEAMLESS CARE
  DECRIPTION: GET MEDICATION OF SPACIFIC ORDER
  ===================================================================*/
  public medication = [];
  public loadingReq=[];
  getMedication(OrderID){
    if(!this.loadingReq[OrderID]){
      this.loadingReq[OrderID] = true;
        this.http.get(Constant.API_URL+'getMedication/'+OrderID).map(res => res.json()).subscribe(data => {
          this.medication[OrderID] = data;
         }, err => {
           console.log(err);
         });
    }
  }
  
  getDeliveryPackgeDOrders(pkgID){
    if(!this.status.packageOrderLoaded[pkgID]){
        this.status.packageOrderLoad[pkgID] = true;
        this.status.packageOrderLoaded[pkgID] = true;
        this.http.get(Constant.API_URL+'deliveryPkgsOrders/'+pkgID).map(res => res.json()).subscribe(data => {
          this.deliveryPkgOrders[pkgID] = data;
          this.status.packageOrderLoad[pkgID] = false;
        }, err => {
          this.status.packageOrderLoad[pkgID] = false;
          console.log(err);
      });
    }
  }

   /*==================================================================
  WRITTEN BY:SEAMLESS CARE
  DECRIPTION: SELECT MARK AS COMPLETE
  ===================================================================*/
  public pkgIDs=[];
  public isSelectPkg=[];
  pkgCompleteMark(pkgID){
    this.isSelectPkg[pkgID] = true;
    if(this.comService.indexWhere(this.pkgIDs, item=>item === pkgID) == -1)
    this.pkgIDs.push(parseInt(pkgID));
  }

  removeCompletedPkg(RunID){
    for(var i =0; i< this.pkgIDs.length; i++){
     var ind =  this.comService.indexWhere(this.packages[RunID], item=>item.DeliveryPkgId === this.pkgIDs[i])
     this.packages[RunID].splice(ind,1);
    }
  }


  jsonParse(obj){
    return JSON.parse(obj);
   }

  
public DeliveryComplete = [];
  deliveryComplete(Run){
    if(this.pkgIDs.length >0){
      this.DeliveryComplete[Run.DeliveryRunID]= true;
      var StartedBy = this.auth.authCheck()['user_id'];
      var body = {"userID":this.auth.authCheck()['user_id'],"RunID":Run.DeliveryRunID,"pkgIDs":this.pkgIDs};
      this.http.put(Constant.API_URL+'deliveryFinalize',body).map(res => res.json()).subscribe(data => { 
        this.removeCompletedPkg(Run.DeliveryRunID);
        this.toastr.success("Run complete successfully.", 'Success!');
        this.DeliveryComplete[Run.DeliveryRunID]= false;
      }, err => {
        this.DeliveryComplete[Run.DeliveryRunID]= false;
          console.log(err.message);
      });
    }else{
      this.toastr.info("Please select alteast one package.");
    }
  }

}
