import { Component, OnInit } from '@angular/core';
import {Http, Response, Headers, RequestOptions} from '@angular/http';
import { ReactiveFormsModule, FormsModule, FormGroup, FormControl, Validators,FormBuilder} from '@angular/forms';
import 'rxjs/add/operator/map';
import { Constant } from '../../../constant';
import {Router} from '@angular/router';
import { AuthService } from '../../../service/auth.service';
import { CommonService } from '../../../service/common.service';
import { AppComponent } from '../../../app.component';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-pharmacist',
  templateUrl: './pharmacist.component.html',
  styleUrls: ['./pharmacist.component.scss']
})
export class PharmacistComponent implements OnInit {

  submitDisabled = {
      'userSubmit': false
  };
  loader = {
      'getUsers': false
  };
  users = [];
  userTypes;

  start = 0;
  load_more = false;
  public status = {mainLoader:false};
  selected_userType = 1;
  myForm: FormGroup;
  userName: FormControl;
  email: FormControl;
  password: FormControl;
  userType: FormControl;
  phone: FormControl;

  dropdownList = [];
  selectedItems = [];
  dropdownSettings = {};

  constructor(
      private http: Http,
      private router: Router,
      private auth: AuthService,
      public comService: CommonService,
      public AppComponent: AppComponent
  ) {}

  ngOnInit() {

      this.AppComponent.header_changes();

      //Checking Authentication -----------------------------
      if (!this.auth.authCheck()) {
          this.router.navigate(['/login']);
          return;
      }
      //--------------------------------

      this.getUserType();

      this.formRole();
      this.fromData();

      this.dropdownSettings = {
          singleSelection: false,
          idField: 'RoleId',
          textField: 'UserRole',
          allowSearchFilter: false,
          enableCheckAll: false
      };
  }

  /*==================================================================
  DECRIPTION: GET USERS / RoleID = 0 WILL GET ALL USERS
  INPUT : RoldID
  ===================================================================*/
  getUsers(userType) {
      var requested_url;
      if (userType == 0) {
          requested_url = Constant.API_URL + 'allUsers/' + this.start;
      } else {
          requested_url = Constant.API_URL + 'users/' + userType + '/' + this.start;
      }

      this.loader['getUsers'] = true;
      const header = new Headers();
      header.append("Content-Type", "application/json");
      const options = new RequestOptions({
          headers: header
      });
      this.http.get(requested_url, options).map(res => res.json()).subscribe(data => {
          this.loader['getUsers'] = false;

          var data_users = data.users;
          for (var x = 0; x < data_users.length; x++) {
              this.users.push(data_users[x]);
          }
          this.start = this.start + data_users.length;
          if (this.start == data.total_user) {
              this.load_more = false;
          } else {
              this.load_more = true;
          }
      }, err => {
          this.loader['getUsers'] = false;
      });
  }

  /*==================================================================
  DECRIPTION: GET ROLES 
  ===================================================================*/
  getUserType() {
      this.status.mainLoader = true;
      const header = new Headers();
      header.append("Content-Type", "application/json");
      const options = new RequestOptions({headers: header});
      this.http.get(Constant.API_URL + 'user-type', options).map(res => res.json()).subscribe(data => {
        this.status.mainLoader = false;
          if (data) {
              this.userTypes = data;
              this.getUsers(this.selected_userType);
          }
      }, err => {
        this.status.mainLoader = false;
      });
  }

  /*==================================================================
  DECRIPTION : USER ACCORDING LEFT PANEL ACTION CLICK
  ARGUMENT : CHECKBOX EVENT / 0
  ===================================================================*/
  selectUserType(event) {
      var selected_type;
      if (typeof event == 'object') {
          selected_type = event.target.defaultValue;
      } else {
          selected_type = event;
      }

      this.users = [];
      this.start = 0;
      this.selected_userType = selected_type;
      this.getUsers(this.selected_userType);
  }

  /*==================================================================
  DECRIPTION : GET USER ROLES 
  ARGUMENT : USER DATA
  ===================================================================*/
  getUserRoles(user) {
      const header = new Headers();
      header.append("Content-Type", "application/json");
      const options = new RequestOptions({
          headers: header
      });
      this.http.get(Constant.API_URL + 'UserRoles/' + user.user_id, options).map(res => res.json()).subscribe(data => {
          this.edit.userType = data;
          this.iHaveUserTypes = true;
      }, err => {});
  }

 /*==================================================================
  DECRIPTION : ACTION PERFORM WHEN CLICK ON EDIT BUTTON 
  ===================================================================*/
  iHaveUserTypes = false;
  modal_title = {
    user: ''
  };
  edit = {
    userName: "",
    email: "",
    phone: "",
    userType: [],
    user_id: "",
    password: ""
}
  editUser(user) {
      this.iHaveUserTypes = false;
      this.modal_title['user'] = 'Edit User';

      this.edit = {
          userName: user.userName,
          email: user.email,
          phone: user.phone,
          userType: [],
          user_id: user.user_id,
          password: user.password
      }
      this.getUserRoles(this.edit);
  }
  
 /*==================================================================
  DECRIPTION : ACTION PERFORM WHEN CLICK ON ADD BUTTON 
  ===================================================================*/
  addUserClick() {
      this.iHaveUserTypes = true;
      this.modal_title['user'] = 'Add User';

      this.edit = {
          userName: "",
          email: "",
          phone: "",
          userType: [],
          user_id: "",
          password: ""
      }
  }

   /*==================================================================
  DECRIPTION : FORM VALIDATION 
  ===================================================================*/
  formRole() {
      this.userName = new FormControl('', [
          Validators.required,
          Validators.pattern("[A-Za-z ‘-]{2,}")
      ]);

      this.userType = new FormControl('', [Validators.required]);

      this.email = new FormControl('', [
          Validators.required,
          Validators.pattern("[^ @]*@[^ @]*")
      ]);
      this.password = new FormControl('', [
          Validators.required,
          Validators.minLength(8)
      ]);
      this.phone = new FormControl('', []);

  }

  fromData() {
      this.myForm = new FormGroup({
          userName: this.userName,
          userType: this.userType,
          email: this.email,
          password: this.password,
          phone: this.phone,
      });
  }

  /*==================================================================
  DECRIPTION: SAVING USER / UPDATING USER
  ===================================================================*/
  saveUser() {
    
      if (this.myForm.valid) {
          // Checking userRoles
          var selected_userType = this.myForm.value.userType;

      if (!selected_userType[0]) {
        Swal('Please select atleast one Role.');
          return;
      }

      var final_userType = [];

      for (var x = 0; x < selected_userType.length; x++) {
          final_userType.push(selected_userType[x].RoleId);
      }

      this.myForm.value.userType = final_userType;
          //-------------------

          this.submitDisabled['userSubmit'] = true;

          var action_request;
          const header = new Headers();
          header.append("Content-Type", "application/json");
          const options = new RequestOptions({
              headers: header
          });
          if (this.edit.user_id) {
              this.http.put(Constant.API_URL + 'user/' + this.edit.user_id, this.myForm.value, options).map(res => res.json()).subscribe(data => {
                  this.submitDisabled['userSubmit'] = false;
                  if (data != null) {
                      this.users = [];
                      this.start = 0;
                      this.getUsers(this.selected_userType);
                      this.comService.closeModel();

                  } else {
                      Swal('Email already exist.');
                  }
              }, err => {
                  this.submitDisabled['userSubmit'] = false;

              });
          } else {
              this.http.post(Constant.API_URL + 'user/add', this.myForm.value, options).map(res => res.json()).subscribe(data => {

                  this.submitDisabled['userSubmit'] = false;

                  if (data != null) {
                      this.users = [];
                      this.start = 0;
                      this.getUsers(this.selected_userType);
                      this.comService.closeModel();
                  } else {
                      Swal('Email already exist.');
                  }
              }, err => {
                  this.submitDisabled['userSubmit'] = false;
              });
          }
      } else {
        Swal('Please complete all required fields.');
      }
  }

  /*==================================================================
  DECRIPTION : PURGE USER 
  ARGUMENT : user_id
  ===================================================================*/
  deleteUser(user_id) {

      var confirmation = confirm('Are You sure');

      if (confirmation == false) {
          return;
      }
      this.comService.showLoading();
      const header = new Headers();
      header.append("Content-Type", "application/json");

      const options = new RequestOptions({
          headers: header
      });
      this.http.delete(Constant.API_URL + 'user/' + user_id, options).map(res => res.json()).subscribe(data => {
          this.comService.showLoading(false);
          this.users = [];
          this.start = 0;
          this.getUsers(this.selected_userType);
      }, err => {
          this.comService.showLoading(false);
      });
  }

}