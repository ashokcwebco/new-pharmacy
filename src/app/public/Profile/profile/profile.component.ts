import { Component, ViewContainerRef, OnInit } from '@angular/core';
import {Constant} from '../../../constant';
import {Http, Response, Headers, RequestOptions} from '@angular/http';
import { ReactiveFormsModule,FormArray, FormsModule, FormGroup, FormControl, Validators,FormBuilder} from '@angular/forms';
import 'rxjs/add/operator/map';
import {Router, ActivatedRoute} from '@angular/router';
import { DatePipe } from '@angular/common';
import { CommonService } from '../../../service/common.service';
import { AuthService } from '../../../service/auth.service';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { ConversationService } from '../../../service/conversation.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  public ProfData;
  public patMedications;
  public ticketService;
  public allergies;
  public conditions;
    constructor(
    private fb: FormBuilder,
    private http: Http,
    public auth : AuthService ,
    private route:ActivatedRoute,
    private router: Router,
    public comService : CommonService,
    public toastr: ToastsManager,
    public ConService : ConversationService ,
     vcr: ViewContainerRef  
  ) { 
    this.toastr.setRootViewContainerRef(vcr);
  }

  ngOnInit() {
    this.getPatProf();
    this.getPatMedications();
    this.getAllergiesAndCondition();
  }


getPatProf(){
  this.http.get(Constant.API_URL+'getPatPro/'+this.route.params["value"].PatID).map(res => res.json()).subscribe(data => {
    this.ProfData = data;
      }, err => {
         console.log(err.message);
  });
}

public loadAlgCnd:Boolean;
getAllergiesAndCondition(){
  // +this.route.params["value"].PatID
  this.loadAlgCnd = true;
  this.http.get(Constant.API_URL+'getPatAlgCnd/'+this.route.params["value"].PatID).map(res => res.json()).subscribe(data => {
        this.allergies = data.Alg;
        this.conditions = data.Cnd;
        this.loadAlgCnd = false;
      }, err => {
        this.loadAlgCnd = false;
  });
}

public loadingPatMedication:Boolean;
getPatMedications(){
  this.loadingPatMedication = true;
  this.http.get(Constant.API_URL+'getPatMedication/'+this.route.params["value"].PatID).map(res => res.json()).subscribe(data => {
        this.patMedications = data;
        this.loadingPatMedication = false;
      }, err => {
        console.log(err.message);
        this.loadingPatMedication = false;
  });
}

public LoadingTicketService:Boolean;
getPatTicketService(){
  this.LoadingTicketService = true;
  this.http.get(Constant.API_URL+'getPatTicketService/'+this.route.params["value"].PatID).map(res => res.json()).subscribe(data => {
        this.ticketService = data;
        this.LoadingTicketService = false;
      }, err => {
        console.log(err.message);
        this.LoadingTicketService = false;
  });
}

public TicketDetails ;
viewTicketDitail(obj){
 this.ConService.getConversation(obj.PharmacyTicketID) 
  this.TicketDetails = obj;
}

jsonParse(obj){
  return JSON.parse(obj);
 }

}
