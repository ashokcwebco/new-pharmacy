import { Component,ViewContainerRef, OnInit } from '@angular/core';
import {Constant} from '../../../constant';
import { ReactiveFormsModule, FormsModule, FormGroup, FormControl, Validators,FormBuilder} from '@angular/forms';
import {Http, Response, Headers, RequestOptions} from '@angular/http';
import {Router, ActivatedRoute} from '@angular/router';
import { CommonService } from '../../../service/common.service';
import { AuthService } from '../../../service/auth.service';
import { createElementCssSelector } from '@angular/compiler';
import Swal from 'sweetalert2';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';


@Component({
  selector: 'app-add-reminder',
  templateUrl: './add-reminder.component.html',
  styleUrls: ['./add-reminder.component.scss']
})
export class AddReminderComponent implements OnInit {
  loading = {
      'reminder': false
  };

  TicketTypeRadio = 2;
  dataObj = {
      filter: [],
      batchItems: [],
      reorderItems: [],
      ReminderId: '',
      type: '',
      detailItem: ''
  };

  // DECLARATION  FROM CONTROLS 
  batchFrom: FormGroup;
  status: FormControl;
  repeat: FormControl;
  dayIntervel: FormControl;
  strips: FormControl;
  TicketType: FormControl;
  Title: FormControl;
  DueDate: FormControl
  prns: FormControl;
  PatID: FormControl;

  dropdownSettings = {};

  constructor(
      public comService: CommonService,
      private http: Http,
      public auth: AuthService,
      private router: Router,
      public toastr: ToastsManager,
       vcr: ViewContainerRef) {this.toastr.setRootViewContainerRef(vcr);}

  ngOnInit() {
      this.batchFormRole();
      this.batchFormData();

      this.dropdownSettings = {
          singleSelection: false,
          idField: 'ID',
          textField: 'DrgInfo',
          allowSearchFilter: true,
          enableCheckAll: false
      };

  }

  /*==================================================================
  WRITTEN BY:
  DECRIPTION: define validation for creating new ticket 
  ===================================================================*/
  batchFormRole() {
      this.status = new FormControl(0);
      this.repeat = new FormControl(0);
      this.dayIntervel = new FormControl(7);
      this.strips = new FormControl('');
      this.TicketType = new FormControl('');
      this.Title = new FormControl('', [Validators.required]);
      this.DueDate = new FormControl('', [Validators.required]);
      this.prns = new FormControl('');
      this.PatID = new FormControl('');
  }
  /*==================================================================
  WRITTEN BY:
  DECRIPTION:: set valuein form group 
  ===================================================================*/
  batchFormData() {
      this.batchFrom = new FormGroup({
          status: this.status,
          repeat: this.repeat,
          dayIntervel: this.dayIntervel,
          strips: this.strips,
          TicketType: this.TicketType,
          Title: this.Title,
          DueDate: this.DueDate,
          prns: this.prns,
          PatID: this.PatID
      });
  }

  addBatch() {
      if (this.batchFrom.value.strips && this.batchFrom.value.strips.trim()) {
          this.dataObj.batchItems.push(this.batchFrom.value.strips);
          this.batchFrom.controls['strips'].setValue('');
      } else {
        this.toastr.error('Please enter strips in Custom text field.', 'Oops!');
      }
  }


  /*==================================================================
  WRITTEN BY:SEAMLESS CARE  
  DECRIPTION: REMOVE SELECTED PATIENT RX
  ===================================================================*/
  removeAddedBatch(batchItem) {
      var indx = this.comService.indexWhere(this.dataObj.batchItems, item => item === batchItem);
      this.dataObj.batchItems.splice(indx, 1);
  }

  removeAddedReOrder(Item) {
      var indx = this.comService.indexWhere(this.dataObj.reorderItems, item => item === Item);
      this.dataObj.reorderItems.splice(indx, 1);
  }

  /*==================================================================
    WRITTEN BY:SEAMLESS CARE
    DECRIPTION:SAVE REMINDER 
  ===================================================================*/
  ReminderRxData = {};
  onSubmitBatch(type) {
    if (!this.batchFrom.valid) {
        this.toastr.error('Please fill all mandatory fields.', 'Oops!');
        return;
    }
      this.batchFrom.value.detailItem = this.dataObj.batchItems.join(";");

    //   if (this.batchFrom.value.detailItem || this.batchFrom.value.strips) {
          if (type == 2 && this.batchFrom.value.prns.length == 0) {
            this.toastr.error('Please select atleast one Re-Order.', 'Oops!');
              return;
          }
          this.loading['reminder'] = true;

          this.dataObj.type = type;
          const header = new Headers();
          this.batchFrom.value.UserIDLastCreated = this.auth.authCheck()['user_id'];
          this.batchFrom.value.TicketType = type;
          this.batchFrom.value.status = (this.batchFrom.value.status) ? 1 : 0;
          this.batchFrom.value.repeat = (this.batchFrom.value.repeat) ? 1 : 0;
          this.batchFrom.value.dayIntervel = (this.batchFrom.value.dayIntervel) ? this.batchFrom.value.dayIntervel : 0;
          if(this.batchFrom.value.detailItem) {
          this.batchFrom.value.Details = this.batchFrom.value.detailItem;
          } else {
          this.batchFrom.value.Details = this.batchFrom.value.strips;
              
          }
          if (type == 2) {
              this.batchFrom.value.PatID = this.SelectedPatID;
          } else {
              this.batchFrom.value.PatID = 0;
          }
          header.append("Content-Type", "application/json");
          const options = new RequestOptions({
              headers: header
          });
          this.http.post(Constant.API_URL + 'reminders/save', this.batchFrom.value, options).map(res => res.json()).subscribe(data => {
              this.loading['reminder'] = false;
              this.dataObj.ReminderId = data[0].id;
              this.dataObj.detailItem = this.batchFrom.value.detailItem;
              this.toastr.success('Reminder has been saved successfully.', 'Success!');


              this.dataObj.batchItems = [];
              this.dataObj.reorderItems = [];
              this.batchFrom.reset();

              this.router.navigate(['/reminders']);

          }, err => {
              this.loading['reminder'] = false;
          });
    //   } else
    //   this.toastr.error('Please add strips for a patients', 'Oops!');
  }

  /*==================================================================
DECRIPTION : GET PATIENTS RX
ARGUMENT : PatID
===================================================================*/
  patientRXs = [];
  ihavpatientRXs;
  findingPrn = false;
  SelectedPatID;
  getPatientsRX(PatID) {
      this.ihavpatientRXs = false;
      this.findingPrn = true;
      const header = new Headers();
      header.append("Content-Type", "application/json");
      const options = new RequestOptions({
          headers: header
      });
      this.http.get(Constant.API_URL + 'getPatientsRx/' + PatID, options).map(res => res.json()).subscribe(data => {
          this.patientRXs = data;
          this.ihavpatientRXs = true;
          this.SelectedPatID = PatID;
          this.findingPrn = false;

      }, err => {
          this.findingPrn = false;

      });
  }


  /*==================================================================
DECRIPTION : FILTER PATIENTS
ARGUMENT : INPUT VALUE
===================================================================*/
  patientList = [];
  findingPatients = false;
  filterPat(e) {
      if (e.target.value) {
          this.findingPatients = true;
          const header = new Headers();
          header.append("Content-Type", "application/json");
          const options = new RequestOptions({
              headers: header
          });
          this.http.get(Constant.API_URL + 'patients/filter/' + e.target.value, options).map(res => res.json()).subscribe(data => {
              this.showlist = true;
              this.findingPatients = false;
              this.patientList = data;
          }, err => {
              this.findingPatients = false;
          });
      } else {
          this.patientList = [];
      }
  }

    /*==================================================================
DECRIPTION : FILTER PATIENTS CLICK ACTION
ARGUMENT : PATIENT INFO
===================================================================*/
  showlist = false;
  selectPat(obj) {
      this.batchFrom.controls['PatID'].setValue(obj.FirstName + ' ' + obj.LastName);
      this.showlist = false;
      this.getPatientsRX(obj.ID);
  }

}