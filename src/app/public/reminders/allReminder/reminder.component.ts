import { Component,ViewContainerRef, OnInit } from '@angular/core';
import {Constant} from '../../../constant';
import {Http, Response, Headers, RequestOptions} from '@angular/http';
import {Router, ActivatedRoute} from '@angular/router';
import { CommonService } from '../../../service/common.service';
import { AuthService } from '../../../service/auth.service';
import { createElementCssSelector } from '@angular/compiler';
import Swal from 'sweetalert2';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';

@Component({
  selector: 'app-reminder',
  templateUrl: './reminder.component.html',
  styleUrls: ['./reminder.component.scss']
})
export class ReminderComponent implements OnInit {

  dataObj = {
      reminders: [],
      batchItems: [],
      reorderItems: []
  };
  strip;
  status = {
      spiner: false,
      filter: false,
      mainLoader:false
  };
  loading = {
      'reminder': []
  };

  constructor(
      public comService: CommonService,
      private http: Http,
      public auth: AuthService,
      private router: Router,
      public toastr: ToastsManager, 
      vcr: ViewContainerRef) {
        this.toastr.setRootViewContainerRef(vcr);
      }

  ngOnInit() {
      this.comService.shownGroup = null;
      this.getReminder();
      this.comService.activeNav('reminder');
  }

  addBatch(obj) {
      if (obj.strip && obj.strip.trim()) {
          if (obj.TicketType == 4)
              this.dataObj.batchItems[obj.ReminderID].push(obj.strip);
          else
              this.dataObj.reorderItems[obj.ReminderID].push(obj.strip);
          obj.strip = '';
      } else 
        this.toastr.error('Please enter strips in text field.', 'Oops!');
      
  }
  /*==================================================================
  DECRIPTION: GET REMINDERS
  ===================================================================*/
  remindersHistory = [];
  getReminder() {
      this.status.mainLoader = true;
      const header = new Headers();
      header.append("Content-Type", "application/json");
      const options = new RequestOptions({
          headers: header
      });
      this.http.get(Constant.API_URL + 'reminders', options).map(res => res.json()).subscribe(data => {
          for (var i = 0; i < data.length; i++) {
              if (data[i].TicketType == 4)
                  this.dataObj.batchItems[data[i].ReminderID] = data[i].Details.split(";");
              if (data[i].TicketType == 2)
                  this.dataObj.reorderItems[data[i].ReminderID] = data[i].Details.split(";");
          }
          this.dataObj.reminders = data;
          this.remindersHistory = data;
          this.status.mainLoader = false;
      }, err => {
        this.status.mainLoader = false;
      });

  }

  /*==================================================================
  WRITTEN BY:SEAMLESS CARE
  DECRIPTION: REMOVE SELECTED PATIENT RX
  ===================================================================*/
  remove(obj, j) {
      if (obj.TicketType == 4)
          this.dataObj.batchItems[obj.ReminderID].splice(j, 1);
      if (obj.TicketType == 2)
          this.dataObj.reorderItems[obj.ReminderID].splice(j, 1);
  }

  /*==================================================================
  WRITTEN BY:SEAMLESS CARE
  DECRIPTION:SAVE ADDED RX IN "REMINDERPATRX" TABLE
  ===================================================================*/

  update(object) {
      object.Details = (object.TicketType == 4) ? this.dataObj.batchItems[object.ReminderID].join(";") : this.dataObj.reorderItems[object.ReminderID].join(";");
      if (object.Details) {

          this.loading['reminder'][object.ReminderID] = true;

          const header = new Headers();
          object.Active = (object.Active) ? 1 : 0;
          object.RepeatOn = (object.RepeatOn) ? 1 : 0;
          header.append("Content-Type", "application/json");
          const options = new RequestOptions({
              headers: header
          });
          this.http.put(Constant.API_URL + 'reminders/update', object, options).map(res => res.json()).subscribe(data => {
              this.loading['reminder'][object.ReminderID] = false;
              this.toastr.success('Reminder #' + object.ReminderID + 'Update Successfully.', 'Success!');
          }, err => {
              this.loading['reminder'][object.ReminderID] = false;

          });
      } else
        this.toastr.error('Please add strips for a patients', 'Oops!');
      
  }

      /*==================================================================
DECRIPTION: CREATE TICKET
ARGUMENT : INPUT EVENT
===================================================================*/
isCreatingTicket = {'' : []};
  createTicket(object) {


    Swal({
        title: 'Are you sure?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes',
        cancelButtonText: 'Cancel'
      }).then((result) => {
        if (result.value) {
            object.Details = (object.TicketType == 4) ? this.dataObj.batchItems[object.ReminderID].join(";") : this.dataObj.reorderItems[object.ReminderID].join(";");
            if (object.Details) {
                this.isCreatingTicket[object.ReminderID] = true;
                const header = new Headers();
                var body = {
                    "title": object.Title,
                    "Details": object.Details,
                    "DeliveryDate" : object.DueDate,
                    "UserIDCreated": this.auth.authCheck()['user_id'],
                    "TicketType": object.TicketType,
                    "PatID": object.PatID,
                    "Prns" : object.Prns 
                };
                header.append("Content-Type", "application/json");
                const options = new RequestOptions({
                    headers: header
                });
                this.http.post(Constant.API_URL + 'reminders/ticket/new-ticket', body, options).map(res => res.json()).subscribe(data => {
                  this.isCreatingTicket[object.ReminderID] = false;
                  this.toastr.success('Reminder ticket has been created.', 'Success!')
                }, err => {
                  this.isCreatingTicket[object.ReminderID] = false;
                  this.toastr.warning('Some technical issue is occurred. Please try again.', 'Alert!');
                });
            } else
                this.toastr.error('Save reminder first. then create a ticket.', 'Oops!');                         
        } else if (result.dismiss === Swal.DismissReason.cancel) {
         
        }
      })

  }


    /*==================================================================
DECRIPTION: Search Reminders
ARGUMENT : INPUT EVENT
===================================================================*/
  searchReminder(e: any) {
      var keyword = e.target.value;

      if (keyword == false) {
          this.dataObj.reminders = this.remindersHistory;
          return;
      }
      this.status['filter'] = true;
      const header = new Headers();
      header.append("Content-Type", "application/json");
      const options = new RequestOptions({
          headers: header
      });
      this.http.get(Constant.API_URL + 'remindersFilter/' + keyword, options).map(res => res.json()).subscribe(data => {
          this.status['filter'] = false;

          if (data.length == 0) {
              this.dataObj.reminders = [];
          }
          this.comService.navCounter.reminder = data.length;
          for (var i = 0; i < data.length; i++) {
              if (data[i].TicketType == 4)
                  this.dataObj.batchItems[data[i].ReminderID] = data[i].Details.split(";");
              if (data[i].TicketType == 2)
                  this.dataObj.reorderItems[data[i].ReminderID] = data[i].Details.split(";");
          }
          this.dataObj.reminders = data;
      }, err => {
          this.status['filter'] = false;

      });
  }

  /*==================================================================
DECRIPTION: GET REMINDER RX
===================================================================*/
  reminderRXs = {
      '': []
  };
  ihaveRXs = {
      '': false
  };
  findingPrn = {
      '': false
  };
  getReminderRX(Reminder) {
      this.findingPrn[Reminder.ReminderID] = true;
      const header = new Headers();
      header.append("Content-Type", "application/json");
      const options = new RequestOptions({
          headers: header
      });
      this.http.get(Constant.API_URL + 'ReminderRx/' + Reminder.ReminderID, options).map(res => res.json()).subscribe(data => {
          this.reminderRXs[Reminder.ReminderID] = data;
          this.ihaveRXs[Reminder.ReminderID] = true;
          this.findingPrn[Reminder.ReminderID] = false;

      }, err => {
          this.findingPrn[Reminder.ReminderID] = false;

      });
  }
}