import {Component,ViewContainerRef,OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {Http,Response,Headers,RequestOptions} from '@angular/http';
import 'rxjs/add/operator/map';
import {AuthService} from '../../service/auth.service';
import {ReactiveFormsModule,FormsModule,FormGroup,FormControl,Validators,FormBuilder,FormControlDirective} from '@angular/forms';
import {CommonService} from '../../service/common.service';
import {Constant} from '../../constant';
import {AppComponent} from '../../app.component';
import Swal from 'sweetalert2';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';

@Component({
    selector: 'app-agencies',
    templateUrl: './agencies.component.html',
    styleUrls: ['./agencies.component.scss']
})
export class AgenciesComponent implements OnInit {

    loader = {
        'getHomePatients': [],
        'searchPatients': [],
        'getSuperUsers': [],
        'getHome': [],
        'getHomeUsers': [],
        'deleteHomePatients': []
    };

    submitDisabled = {
        'homeSubmit': false,
        'agencySubmit': false,
        'userSubmit': false
    };

    homePatsUsersFetched = {
        'homePatsFetched': []
    };
    regularCaregiverUsers = [];
    searchLoading = {
        'patients': false,
        'agency': false,
        'deleteUser': false
    };
    public status = {mainLoader:false};
    agencies = [];
    start = 0;
    load_more = false;
    droppedUsers = [];
    AgencyHistory = [];
    dropdownSettings = {};
    myForm: FormGroup;
    AgencyName: FormControl;
    AgencyPhone: FormControl;
    Address1: FormControl;
    Address2: FormControl;
    City: FormControl;
    Prov: FormControl;
    Postal: FormControl;
    userForm: FormGroup;
    userName: FormControl;
    email: FormControl;
    password: FormControl;
    phone: FormControl;
    is_super:FormControl;

    homeForm: FormGroup;
    HomeName: FormControl;
    HomePhone: FormControl;
    BinName: FormControl;
    DefaultDeliveryDay: FormControl;
    DefaultDeliveryTime: FormControl;
    
    Area:FormControl;
    SubArea:FormControl;
    
    homeAddressForm: FormGroup;
    Title: FormControl;
    HomeAddress1: FormControl;
    HomeAddress2: FormControl;
    DeliveryInstructions:FormControl;
    HomeCity: FormControl;
    HomeProv: FormControl;
    HomePostal: FormControl;
    Active: FormControl;

    CareGiverForm :FormGroup;
    careGiver: FormControl;
    ragularCaregiver = [];
    items = [];

    assignedCareGiver = [];

    droppedItems = [];
    HomeUsers = [];
    editAssignedCaregiver = {
        careGiver:[]
    }
    constructor(
        public auth: AuthService,
        public router: Router,
        private http: Http,
        public comService: CommonService,
        public AppComponent: AppComponent,
        public toastr: ToastsManager,
        vcr: ViewContainerRef 

    ) {
        this.toastr.setRootViewContainerRef(vcr);
    }

    ngOnInit() {
        this.fullAddress = [];
        this.AppComponent.header_changes();
        //Checking Authentication -----------------------------
        if (!this.auth.authCheck()) {
            this.router.navigate(['/login']);
            return;
        }
        //--------------------------------

        this.comService.shownGroup = null;
        this.getArea();
        this.getAgency();

        this.formRole();
        this.fromData();
        // this.getRegularCaregiver();
        this.homeFormRole();
        this.homeFromData();

        this.homeAddressFormRole();
        this.homeAddressFromData();

        this.userFormRole();
        this.userFromData();

        this.dropdownSettings = {
            singleSelection: false,
            idField: 'user_id',
            textField: 'userName',
            allowSearchFilter: false,
            enableCheckAll: false
        };


        this.careGiver = new FormControl('', [Validators.required]);
        this.CareGiverForm = new FormGroup({ careGiver: this.careGiver });

    }

    /*==================================================================
    DESCRIPTION: ACTION PERFORM, WHEN PATIENT DRAG AND DROP TO HOME
    ===================================================================*/
    onItemDrop(e: any, HomeID) {

        var data = e.dragData[0];
        var whoDrag = e.dragData[1];
        data.HomeID = HomeID;
        if (whoDrag == 'patientDrag') {
            // Removing Patient form Search Patients
            for (var i = 0; i < this.patients.length; i++) {
                if (this.patients[i].ID == data.ID) {
                    this.patients.splice(i, 1);
                    break;
                }
            }
            //-------------------
            this.droppedItems.unshift(data);

            // Saving Drag Patients
            const header = new Headers();
            header.append("Content-Type", "application/json");
            const options = new RequestOptions({
                headers: header
            });
            var body = {
                HomeID: HomeID,
                PatID: this.droppedItems[0].ID
            };
            this.http.post(Constant.API_URL + 'HomePat/', body, options).map(res => res.json()).subscribe(data => {

                for (var x = 0; x < this.droppedItems.length; x++) {
                    this.droppedItems[x]['id'] = data;
                    this.HomePats.unshift(this.droppedItems[x]);
                }
                this.droppedItems = [];
            }, err => {
            });
            //---------------------
        }
    }

    /*==================================================================
    DECRIPTION: GET ALL REULAR CAREGIVER
    ===================================================================*/
    getRegularCaregiver(AgencyID){
        this.http.get(Constant.API_URL + 'getRegularUser/'+AgencyID).map(res => res.json()).subscribe(data => {
            this.ragularCaregiver = data;
        }, err => {

        });
    }
    /*==================================================================
    DECRIPTION: GET ASSIGNED  REULAR CAREGIVER TO PATIENT
    INPIT: PATID
    ===================================================================*/
    public caregiverIsLoaded:Boolean;
    public AssignToPatName = '';
    GetCareGiverAssigned(obj){
        this.AssignToPatName = obj.FirstName+' '+obj.LastName;
        this.caregiverIsLoaded = true;
        this.http.get(Constant.API_URL + 'UserPatRel/'+obj.PatID).map(res => res.json()).subscribe(data => {
            this.editAssignedCaregiver.careGiver = data;
            this.caregiverIsLoaded = false;
        }, err => {
            console.log(err);
        });
        this.AssignTopatID = obj.PatID;
    }

    updateCareGiverList(PatID){
        this.http.get(Constant.API_URL + 'UserPatRel/'+PatID).map(res => res.json()).subscribe(data => {
            this.regularCaregiverUsers[PatID] = data;
        }, err => {
            
        });
    }
  /*==================================================================
    DECRIPTION: ASSIGN REULAR CAREGIVER TO PATIENT (SAVE)
    ===================================================================*/
    public AssignTopatID = 0;
    assignCareGiverSave(){
       if(this.CareGiverForm.valid) {
           this.submitDisabled['userAssigned'] = true;
        var body = {PatID: this.AssignTopatID, UserIDs: this.CareGiverForm.value.careGiver};
        this.http.post(Constant.API_URL + 'UserPatRel', body).map(res => res.json()).subscribe(data => {
            this.toastr.success('Caregiver successfully assigned. ', 'Success!');
                this.updateCareGiverList(this.AssignTopatID);
                this.comService.closeModel('closeUser234');
            this.submitDisabled['userAssigned'] = false;
        }, err => {
            console.log(err.message);
            this.comService.closeModel('closeUser234');
            this.submitDisabled['userAssigned'] = false;
        });
       }  
    }

    manageRegularUser(PatID,userData){
       if(this.regularCaregiverUsers[PatID])
         this.regularCaregiverUsers[PatID].push(userData);
       else
        this.regularCaregiverUsers[PatID] = [userData];
    }

    // onUserDrop(e: any, UserData) {
    //     var body = {PatID: e.dragData[0].PatID, UserID: UserData.user_id};
    //     this.http.post(Constant.API_URL + 'UserPatRel', body).map(res => res.json()).subscribe(data => {
    //         this.manageRegularUser(e.dragData[0].PatID,UserData);
    //         this.toastr.success(UserData.userName+' to successfully assigned. ', 'Success!');
    //             }, err => {
    //     });
    // }

    /*==================================================================
    DECRIPTION: HOME FORM VALIDATION
    ===================================================================*/
    homeFormRole() {
        this.HomeName = new FormControl('', [Validators.required]);
        this.HomePhone = new FormControl('');
        this.BinName = new FormControl('');
        this.DefaultDeliveryDay = new FormControl('');
        this.DefaultDeliveryTime = new FormControl('');
        this.Area = new FormControl('');
        this.SubArea = new FormControl('');
        // this.DeliveryInstructions = new FormControl('', [Validators.required]);

    }

    homeFromData() {
        this.homeForm = new FormGroup({
            HomeName: this.HomeName,
            HomePhone: this.HomePhone,
            BinName: this.BinName,
            DefaultDeliveryDay: this.DefaultDeliveryDay,
            // DeliveryInstructions: this.DeliveryInstructions,
            DefaultDeliveryTime: this.DefaultDeliveryTime,
            Area:this.Area,
            SubArea:this.SubArea
        });
    }

    public regularCaregiverLoaded = [];
    getUserPatRel(PatID){
        if(!this.regularCaregiverLoaded[PatID]){
            this.regularCaregiverLoaded[PatID] = true;
            this.http.get(Constant.API_URL + 'UserPatRel/'+PatID).map(res => res.json()).subscribe(data => {
                this.regularCaregiverUsers[PatID] = data;
            }, err => {
                
            });
        }
    }

        /*==================================================================
    DECRIPTION: HOME ADDRESS FORM VALIDATION
    ===================================================================*/
    homeAddressFormRole() {
        this.Title = new FormControl('', [Validators.required]);
        this.HomeAddress1 = new FormControl('');
        this.HomeAddress2 = new FormControl('');
        this.DeliveryInstructions = new FormControl('');
        this.HomeCity = new FormControl('');
        this.HomeProv = new FormControl('');
        this.HomePostal = new FormControl('');
        this.Active = new FormControl('');
        
    }

    homeAddressFromData() {
        this.homeAddressForm = new FormGroup({
            Title: this.Title,
            HomeAddress1: this.HomeAddress1,
            HomeAddress2: this.HomeAddress2,
            DeliveryInstructions: this.DeliveryInstructions,
            HomeCity: this.HomeCity,
            HomeProv: this.HomeProv,
            HomePostal: this.HomePostal,
            Active: this.Active,
            
        });
    }

    /*==================================================================
    DECRIPTION: USER FORM VALIDATION
    ===================================================================*/
    userFormRole() {
        this.userName = new FormControl('', [
            Validators.required,
            Validators.pattern("[A-Za-z ‘-]{2,}")
        ]);
        this.email = new FormControl('', [
            Validators.required,
            Validators.pattern("[^ @]*@[^ @]*")
        ]);
        this.password = new FormControl('', [
            Validators.required,
            Validators.minLength(8)
        ]);
        this.phone = new FormControl('', []);
        this.is_super = new FormControl('', [Validators.required]);
        


    }

    userFromData() {
        this.userForm = new FormGroup({
            userName: this.userName,
            email: this.email,
            password: this.password,
            phone: this.phone,
            is_super : this.is_super
        });
    }


    /*==================================================================
    DECRIPTION: GET AGENCIES
    ===================================================================*/

    getAgency() {
        this.status.mainLoader = true;
        const header = new Headers();
        header.append("Content-Type", "application/json");
        const options = new RequestOptions({headers: header});
        this.http.get(Constant.API_URL + 'agencies', options).map(res => res.json()).subscribe(data => {
            this.status.mainLoader = false;
            this.agencies = data.agencies;
            this.AgencyHistory = this.agencies;
        }, err => {
            this.status.mainLoader = false;
        });
    }

/*==================================================================
  DECRIPTION : GET AGENCIES USING SEARCH KEYWORD LIKE AGENCY, HOME, BIN, USER, BARCODE
  ARGUMENT : SEARCH EVENT
  ===================================================================*/

    searchAgencies(e) {
        var keyword = e.target.value
        if (keyword == false) {
            this.comService.shownGroup = null;
            this.agencies = this.AgencyHistory;
            return;
        }
        if(keyword.length >= 3){
            this.load_more = false;
            this.searchLoading['agency'] = true;
            const header = new Headers();
            header.append("Content-Type", "application/json");
            const options = new RequestOptions({headers: header});
            this.http.get(Constant.API_URL + 'agencies/filter/' + keyword , options).map(res => res.json()).subscribe(data => {
                this.searchLoading['agency'] = false;
                this.comService.shownGroup = null;
                this.agencies = data.agencies;
            }, err => {
                this.searchLoading['agency'] = false;
            });
        }
    }

    /*==================================================================
      DECRIPTION: SAVE AGENCY / UPDATE AGENCY
      ===================================================================*/
    saveAgency() {
        if (this.myForm.valid) {
            this.submitDisabled['agencySubmit'] = true;
            const header = new Headers();
            header.append("Content-Type", "application/json");
            const options = new RequestOptions({headers: header});
            if (this.edit.AgencyID) {
                this.http.put(Constant.API_URL + 'agency/' + this.edit.AgencyID, this.myForm.value, options).map(res => res.json()).subscribe(data => {
                    this.submitDisabled['agencySubmit'] = false;
                    this.toastr.success('Agency updated successfully.', 'Success!');
                    this.comService.closeModel();
                    this.agencies = [];
                    this.start = 0;
                    this.getAgency();
                }, err => {
                    this.submitDisabled['agencySubmit'] = false;
                });
            } else {
                this.http.post(Constant.API_URL + 'agency/', this.myForm.value, options).map(res => res.json()).subscribe(data => {
                    this.submitDisabled['agencySubmit'] = false;
                    this.toastr.success('Agency created successfully.', 'Success!');
                    this.comService.closeModel();
                    this.agencies = [];
                    this.start = 0;
                    this.getAgency();
                }, err => {
                    this.submitDisabled['agencySubmit'] = false;
                });
            }
        } else 
            this.toastr.error('Please file all mandatory fields.', 'Oops!');
        
    }

    /*==================================================================
      DECRIPTION: FORM VALIDATION
      ===================================================================*/
    formRole() {
        this.AgencyName = new FormControl('', [Validators.required]);
        this.AgencyPhone = new FormControl('', []);
        this.Address1 = new FormControl('',[]);
        this.Address2 = new FormControl('', []);
        this.City = new FormControl('',[]);
        this.Prov = new FormControl('',[]);
        this.Postal = new FormControl('', []);

    }

    fromData() {
        this.myForm = new FormGroup({
            AgencyName: this.AgencyName,
            AgencyPhone: this.AgencyPhone,
            Address1: this.Address1,
            Address2: this.Address2,
            City: this.City,
            Prov: this.Prov,
            Postal: this.Postal
        });
    }

     /*==================================================================
      DECRIPTION: ACTION PERFORM WHEN CLICK ON EDIT AGENCY BUTTON
      ===================================================================*/
      edit = {
        AgencyID: 0,
        AgencyName: "",
        AgencyPhone: "",
        Address1: "",
        Address2: "",
        City: "",
        Prov: "",
        Postal: ""
    }
    modal_title = {
        home: '',
        agency: '',
        user: ''
    };
    editAgency(agency) {
        this.modal_title['agency'] = 'Edit Agency';

        this.edit.AgencyID = agency.AgencyID;
        this.edit.AgencyName = agency.AgencyName;
        this.edit.AgencyPhone = agency.AgencyPhone;
        this.edit.Address1 = agency.Address1;
        this.edit.Address2 = agency.Address2;
        this.edit.City = agency.City;
        this.edit.Prov = agency.Prov;
        this.edit.Postal = agency.Postal;
    }

    /*==================================================================
      DECRIPTION: ACTION PERFORM WHEN CLICK ON ADD AGENCY BUTTON
      ===================================================================*/
    clickAgency() {
        this.modal_title['agency'] = 'Add Agency';

        this.edit = {
            AgencyID: 0,
            AgencyName: "",
            AgencyPhone: "",
            Address1: "",
            Address2: "",
            City: "",
            Prov: "",
            Postal: ""
        }
    }
    /*==================================================================
    DECRIPTION: GET AREAs
    ===================================================================*/
    public HomeArea=[];
    getArea(){
        this.http.get(Constant.API_URL+'home/area').map(res => res.json()).subscribe(data => {
            this.HomeArea = data;
        }, err => {
            console.log(err);
        });
    }

    /*==================================================================
    DECRIPTION: GET SUB-AREA
    INPUT: AREAID
    ===================================================================*/
    public HomeSubArea = [];
    public isAreaSelected:Boolean;
    public subAreaIsLoaded:Boolean;
    getSubArea(e){
        this.subAreaIsLoaded = true;
        this.isAreaSelected = true
        this.HomeSubArea = [];
        this.http.get(Constant.API_URL+'home/area/subArea/'+e.target.value).map(res => res.json()).subscribe(data => {
            this.HomeSubArea = data;
            this.subAreaIsLoaded = false;
        }, err => {
            this.subAreaIsLoaded = false;
            console.log(err);
        });
    }

    private isLoadedSubArea = [];
    getSubAreaByArea(AreaID,HomeID){
        if(!this.isLoadedSubArea[HomeID]){
            this.subAreaIsLoaded = true;
            this.isLoadedSubArea[HomeID] = true;
            this.isAreaSelected = true
            this.HomeSubArea = [];
            this.http.get(Constant.API_URL+'home/area/subArea/'+AreaID).map(res => res.json()).subscribe(data => {
                this.HomeSubArea = data;
                this.subAreaIsLoaded = false;
            }, err => {
                this.subAreaIsLoaded = false;
                console.log(err);
            });
        }
    }

    /*==================================================================
      DECRIPTION: ACTION PERFORM WHEN CLICK ON EDIT HOME BUTTON
      ===================================================================*/
    home = {
        HomeID: '',
        AgencyID: '',
        HomeName: '',
        HomePhone: '',
        BinName: '',
        Area:'',
        SubArea:'',
        BinBarcode: '',
        BinBarcodeImage: '',
        DefaultDeliveryDay: 0,
        DefaultDeliveryTime: '',
        
    };

    edit_home(home) {
        this.modal_title['home'] = 'Edit Home';

        this.home.HomeID = home.HomeID;
        this.home.HomeName = home.HomeName;
        this.home.HomePhone = home.HomePhone;
        this.home.BinName = home.BinName;
        this.home.SubArea = home.SubAreaID;
        this.home.Area = home.AreaID;
        this.home.BinBarcode = home.BinBarcode;
        this.home.BinBarcodeImage = home.BinBarcodeImage;
        this.home.DefaultDeliveryDay = home.DefaultDeliveryDay;
        this.home.DefaultDeliveryTime = home.DefaultDeliveryTime;
        this.home.AgencyID = home.AgencyID;
    }

    /*==================================================================
      DECRIPTION: ACTION PERFORM WHEN CLICK ON ADD HOME BUTTON
      ===================================================================*/
    home_click(agency) {
        this.modal_title['home'] = 'Add Home';
        this.home = {
            HomeID: '',
            AgencyID: '',
            HomeName: '',
            HomePhone: '',
            Area:'',
            SubArea:'',
            BinName: '',
            BinBarcode: '',
            BinBarcodeImage: '',
            DefaultDeliveryDay: 0,
            DefaultDeliveryTime: ''
        };
        this.home.AgencyID = agency.AgencyID;
    }

    /*==================================================================
    DECRIPTION: SAVE HOME / UPDATE HOME
    ===================================================================*/
    homeSubmit() {
        if (this.homeForm.valid) {
            this.homeForm.value.AgencyID = this.home.AgencyID;
            if (this.home.HomeID) {
                this.submitDisabled['homeSubmit'] = true;
                const header = new Headers();
                header.append("Content-Type", "application/json");
                const options = new RequestOptions({headers: header});
                this.http.put(Constant.API_URL + 'home/' + this.home.HomeID, this.homeForm.value, options).map(res => res.json()).subscribe(data => {
                    this.submitDisabled['homeSubmit'] = false;
                    this.comService.closeModel('closeHome1');
                    if(data.code){
                        this.toastr.error('Some technical issues is occurred', 'Oops!');
                        return; 
                    }
                    this.toastr.success('Home updated successfully.', 'Success!');
                    this.homes = [];
                    this.home_loaded = [];
                    this.superUsers = [];
                    this.getHome(this.home.AgencyID);
                    this.getsuperUser(this.home.AgencyID);
                }, err => {
                    this.submitDisabled['homeSubmit'] = false;
                });
            } else {
                this.submitDisabled['homeSubmit'] = true;
                const header = new Headers();
                header.append("Content-Type", "application/json");
                const options = new RequestOptions({headers: header});
                this.http.post(Constant.API_URL + 'home/', this.homeForm.value, options).map(res => res.json()).subscribe(data => {
                    this.submitDisabled['homeSubmit'] = false;
                    this.comService.closeModel('closeHome1');
                    if(data.code){
                        this.toastr.error('Some technical issues is occurred', 'Oops!');
                        return; 
                    }
                    this.toastr.success('Home created successfully.', 'Success!');
                    this.homes.unshift({
                        HomeID: data.HomeID,
                        AgencyID: this.home.AgencyID,
                        HomeName: this.home.HomeName,
                        HomePhone: this.home.HomePhone,
                        BinName: this.home.BinName,
                        BinBarcodeImage: data.BinBarcodeImage,
                        DefaultDeliveryDay: this.home.DefaultDeliveryDay,
                        DefaultDeliveryTime: this.home.DefaultDeliveryTime
                    });
                }, err => {
                    this.submitDisabled['homeSubmit'] = false;
                });
            }
        }  else 
        this.toastr.error('Please file all mandatory fields.', 'Oops!');
    }
    
/*==================================================================
  DECRIPTION : GET HOME
  ARGUMENT : AgencyID
  ===================================================================*/
    homes = [];
    homeLoad_more;
    homes_start = 0;
    home_loaded = [];
    superUsers = [];
    accordion_loader;
    getHome(AgencyID, get_superUsers = false) {
        if (this.home_loaded.indexOf(AgencyID) != -1) {
            return;
        } else {
            this.home_loaded.push(AgencyID);
        }
        this.loader['getHome'][AgencyID] = true;
        const header = new Headers();
        header.append("Content-Type", "application/json");
        const options = new RequestOptions({headers: header});
        this.http.get(Constant.API_URL + 'home/' + AgencyID + '/' + this.homes_start, options).map(res => res.json()).subscribe(data => {
            this.loader['getHome'][AgencyID] = false;
            var homes_data = data.homes;
            for (var x = 0; x < homes_data.length; x++) {
                this.homes.push(homes_data[x]);
            }
            if (get_superUsers) {
                this.getsuperUser(AgencyID);
            }
        }, err => {
            this.loader['getHome'][AgencyID] = false;
        });

    }

/*==================================================================
  DECRIPTION : SEARCH PATIENT USING SEARCH KEYWORD
  AGGUMENT : CHECKBOX EVENT
  ===================================================================*/
    keyword;
    patients = [];
    patients_AgencyID;
    showSearchLoading;
    searchPatients(e) {
        if (e.target.value == false) {
            this.patients = [];
            return;
        }
        this.showSearchLoading = true;
        const header = new Headers();
        header.append("Content-Type", "application/json");
        const options = new RequestOptions({headers: header});
        this.http.get(Constant.API_URL + 'notHomePatients/filter/' + e.target.value + '/', options).map(res => res.json()).subscribe(data => {
            this.showSearchLoading = false;
            if (data) {
                this.patients = data;
            } else {
                this.patients = [];
            }
        }, err => {
            this.showSearchLoading = false;


        });
    }


/*==================================================================
  DECRIPTION : ACTION PERFORM WHEN CLICK ON ADD USER
  ===================================================================*/
    edit_user = {
        user_id: '',
        AgencyID: '',
        userName: '',
        email: '',
        password: '',
        phone: '',
        is_super : ''
    };
    user_click(agency) {
        this.modal_title['user'] = 'Add User';

        this.edit_user = {
            user_id: '',
            AgencyID: '',
            userName: '',
            email: '',
            password: '',
            phone: '',
            is_super : ''
        };
        this.edit_user.AgencyID = agency.AgencyID;

    }

    /*==================================================================
  DECRIPTION : ACTION PERFORM WHEN CLICK ON EDIT USER
  ===================================================================*/
    editUser(user) {
        this.modal_title['user'] = 'Edit User';

        this.edit_user.user_id = user.user_id;
        this.edit_user.AgencyID = user.AgencyID;
        this.edit_user.userName = user.userName;
        this.edit_user.email = user.email;
        this.edit_user.password = user.password;
        this.edit_user.phone = user.phone;
        this.edit_user.is_super = user.is_super;
    }

    /*==================================================================
  DECRIPTION: SAVE USER / UPDATE USER
  ===================================================================*/
    userSubmit() {
        if (this.userForm.valid) {
            if (!this.edit_user.user_id) {
                this.userForm.value.AgencyID = this.edit_user.AgencyID;
                this.submitDisabled['userSubmit'] = true;
                const header = new Headers();
                header.append("Content-Type", "application/json");
                const options = new RequestOptions({
                    headers: header
                });
                this.http.post(Constant.API_URL + 'superUsers/', this.userForm.value, options).map(res => res.json()).subscribe(data => {

                    this.submitDisabled['userSubmit'] = false;
                    if (data == null) {
                        this.toastr.error('Email already exist.', 'Oops!');
                        return;
                    }
                    // this.getRegularCaregiver(this.userForm.value.AgencyID);
                    this.toastr.success('User created successfully.', 'Success!');
                    this.comService.closeModel('closeUser1');
                    this.superUsers.unshift({
                        user_id: data.user_id,
                        userName: this.edit_user.userName,
                        AgencyID: this.edit_user.AgencyID,
                        email: this.edit_user.email,
                        password: this.edit_user.password,
                        phone: this.edit_user.phone,
                        is_super: this.userForm.value.is_super
                    });
                }, err => {
                    this.submitDisabled['userSubmit'] = false;
                });
            } else {
                this.submitDisabled['userSubmit'] = true;
                const header = new Headers();
                header.append("Content-Type", "application/json");
                const options = new RequestOptions({headers: header});
                this.http.put(Constant.API_URL + 'superUsers/' + this.edit_user.user_id, this.userForm.value, options).map(res => res.json()).subscribe(data => {
                    this.submitDisabled['userSubmit'] = false;
                    this.toastr.success('User updated successfully.', 'Success!');
                    if (data == null) {
                        this.toastr.error('Email already exist.', 'Oops!');
                        return;
                    }
                    this.comService.closeModel('closeUser1');
                    this.homes = [];
                    this.home_loaded = [];
                    this.superUsers = [];
                    this.start = 0;
                    this.getHome(this.edit_user.AgencyID);
                    this.getsuperUser(this.edit_user.AgencyID);
                }, err => {
                    this.submitDisabled['userSubmit'] = false;
                });
            }

        }   else 
            this.toastr.error('Please fill all mandatory fields', 'Oops!');

    }

/*==================================================================
  DECRIPTION : GET AGENCY USERS
  ARGUMENT : AgencyID
  ===================================================================*/
    superUser_start = 0;
    userLoad_more = false;

    getsuperUser(AgencyID) {
        this.loader['getSuperUsers'][AgencyID] = true;
        const header = new Headers();
        header.append("Content-Type", "application/json");
        const options = new RequestOptions({headers: header});
        this.http.get(Constant.API_URL + 'superUsersNotAddedInHome/' + AgencyID + '/' + this.superUser_start, options).map(res => res.json()).subscribe(data => {
            this.loader['getSuperUsers'][AgencyID] = false;
            var superUser_data = data.superUsers;
            for (var x = 0; x < superUser_data.length; x++) {
                this.superUsers.push(superUser_data[x]);
            }
        }, err => {
            this.loader['getSuperUsers'][AgencyID] = false;

        });

    }

    /*==================================================================
  DECRIPTION : GET HOME PATIENTS
  ARGUMENT : HomeID
  ===================================================================*/

    homePat_start = 0;
    HomePats = [];
    HomePatsLoad_more = true;
    viewHomePatients(HomeID) {
        this.loader['getHomePatients'][HomeID] = true;
        const header = new Headers();
        header.append("Content-Type", "application/json");
        const options = new RequestOptions({headers: header});
        this.http.get(Constant.API_URL + 'homePat/' + HomeID + '/' + this.homePat_start, options).map(res => res.json()).subscribe(data => {
            this.homePatsUsersFetched['homePatsFetched'][HomeID] = true;
            this.loader['getHomePatients'][HomeID] = false;
            var homePat_data = data.HomePats;
            for (var x = 0; x < homePat_data.length; x++) {
                this.HomePats.push(homePat_data[x]);
            }
        }, err => {
            this.loader['getHomePatients'][HomeID] = false;

        });
    }
    //-----

    /*==================================================================
  DECRIPTION : DELETE PATIENTS
  AGRUMENT : HomePat TABLE DETAIL
  ===================================================================*/

    delete_patient(HomePat) {

        Swal({
            title: 'Are you sure?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes',
            cancelButtonText: 'Cancel'
        }).then((result) => {
            if (result.value) {
                this.loader['deleteHomePatients'][HomePat.id] = true;
                const header = new Headers();
                header.append("Content-Type", "application/json");
                const options = new RequestOptions({headers: header});
                this.http.delete(Constant.API_URL + 'HomePat/' + HomePat.id, options).map(res => res.json()).subscribe(data => {
                    this.loader['deleteHomePatients'][HomePat.id] = false;
                    // Removing Patient form Home Patients List
                    for (var i = 0; i < this.HomePats.length; i++) {
                        if (this.HomePats[i].id == HomePat.id) {
                            this.HomePats.splice(i, 1);
                            break;
                        }
                    }
                }, err => {
                    this.loader['deleteHomePatients'][HomePat.id] = false;
                });
            } else if (result.dismiss === Swal.DismissReason.cancel) {

            }
        })


    }


    /*==================================================================
      DECRIPTION : DELETE AGENCY USER
      ARGUMENT : user_id, AgencyID
      ===================================================================*/

    deleteUser(user_id, AgencyID) {

        Swal({
            title: 'Are you sure?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes',
            cancelButtonText: 'Cancel'
        }).then((result) => {
            if (result.value) {
                this.searchLoading['deleteUser'] = true;
                const header = new Headers();
                header.append("Content-Type", "application/json");
                const options = new RequestOptions({headers: header});
                this.http.delete(Constant.API_URL + 'user/' + user_id, options).map(res => res.json()).subscribe(data => {
                    this.searchLoading['deleteUser'] = false;
                    this.comService.closeModel('closeUser1');
                    this.superUsers = [];
                    this.getsuperUser(AgencyID);
                }, err => {
                    this.searchLoading['deleteUser'] = false;
                });
            } else if (result.dismiss === Swal.DismissReason.cancel) {

            }
        })

    }

 /*==================================================================
      DECRIPTION : HOME ACCORDIANCE
      ARGUMENT : UNIQUE KEY
      ===================================================================*/
    accordianceResponse = {
        '': false
    };
    Accordiance(index) {
        if (this.accordianceResponse[index]) {
            this.accordianceResponse[index] = false;
        } else {
            this.accordianceResponse[index] = true;
        }
    }
    public isPrimary:Boolean;
    addressBoxChange(e) {
        this.UpdatingAddressID = parseInt(e.target.value);
        var indx =  this.comService.indexWhere(this.home_addresses[this.CurrentHomeID], item=>item.HomeAddressesID === parseInt(e.target.value));
        var addressObj =  this.home_addresses[this.CurrentHomeID][indx];
        this.homeAddressForm.controls['Title'].setValue(addressObj.Title);
        this.homeAddressForm.controls['HomeAddress1'].setValue(addressObj.Address1);
        this.homeAddressForm.controls['HomeAddress2'].setValue(addressObj.Address2);
        this.homeAddressForm.controls['DeliveryInstructions'].setValue(addressObj.DeliveryInstructions);
        this.homeAddressForm.controls['HomeCity'].setValue(addressObj.City);
        this.homeAddressForm.controls['HomeProv'].setValue(addressObj.Prov);
        this.homeAddressForm.controls['HomePostal'].setValue(addressObj.Postal);
        this.homeAddressForm.controls['Active'].setValue(addressObj.Active == 0?false:true);
        this.isPrimary = addressObj.Active == 0 ? false:true;
     
    }

    /*==================================================================
    DECRIPTION: GET HOME ADDRESS
    ARGUMETNS : HomeID
      ===================================================================*/
    public home_addresses =[];
    public  CurrentHomeID = 0;
    private UpdatingAddressID = 0;
    public AddressLoading = false;
    getAddress(HomeID) {
        this.CurrentHomeID = HomeID;
        const header = new Headers();
        this.AddressLoading = true;
        header.append("Content-Type", "application/json");
        const options = new RequestOptions({headers: header});
        this.http.get(Constant.API_URL + 'HomeAddresses/'+HomeID, options).map(res => res.json()).subscribe(data => {
            this.home_addresses[HomeID] = data;
            this.AddressLoading = false;
        }, err => {
            this.AddressLoading = false;
            console.log(err);
        });
    }
        /*==================================================================
      DECRIPTION: ACTION PERFORM WHEN CLICK ON ADD HOME ADDRESS BUTTON
      ===================================================================*/   
      homeAddress_click(home) {
        this.homeAddressForm.reset();
        this.getAddress(home.HomeID);
        this.modal_title['home'] = 'Add Home Address';
    }

    public fullAddress = [];
        /*==================================================================
    DECRIPTION: SAVE / UPDATE HOME ADDRESS
    ===================================================================*/
    homeAddressSubmit() {
        if (this.homeAddressForm.valid) {
            this.homeAddressForm.value.HomeID = this.CurrentHomeID;
            this.homeAddressForm.value.Active = this.homeAddressForm.value.Active?1:0;
            if(this.homeAddressForm.value.Active ==1){
                this.fullAddress[this.CurrentHomeID] = this.homeAddressForm.value.HomeAddress1+', '+this.homeAddressForm.value.HomeAddress2+', '+this.homeAddressForm.value.HomeCity+','+this.homeAddressForm.value.HomeProv+' ('+this.homeAddressForm.value.HomePostal+')';
               }
            if (this.UpdatingAddressID != 0) {
                this.submitDisabled['homeSubmit'] = true;
                const header = new Headers();
                header.append("Content-Type", "application/json");
                const options = new RequestOptions({headers: header});
                this.http.put(Constant.API_URL + 'HomeAddresses/' + this.UpdatingAddressID, this.homeAddressForm.value, options).map(res => res.json()).subscribe(data => {
                    this.submitDisabled['homeSubmit'] = false;
                    this.comService.closeModel('closeHomeAddress');
                    this.toastr.success('Home address save successfully', 'Success!');
                }, err => {
                    this.submitDisabled['homeSubmit'] = false;
                });
            } else {
                this.submitDisabled['homeSubmit'] = true;
                const header = new Headers();
                header.append("Content-Type", "application/json");
                const options = new RequestOptions({headers: header});
                this.http.post(Constant.API_URL + 'HomeAddresses/', this.homeAddressForm.value, options).map(res => res.json()).subscribe(data => {
                    this.submitDisabled['homeSubmit'] = false;
                    this.comService.closeModel('closeHomeAddress');
                    this.toastr.success('Home address update successfully', 'Success!');
                }, err => {
                    this.submitDisabled['homeSubmit'] = false;
                });
            }
        }  else 
        this.toastr.error('Please fill all mandatory fields', 'Oops!');
        

    }
}