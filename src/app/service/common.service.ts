import { Injectable } from '@angular/core';
import {Constant} from './../constant';
import {Http, Response, Headers, RequestOptions} from '@angular/http';

@Injectable()
export class CommonService {
  public baseUrl = 'http://localhost:8282/'
  public general= [];
  public reorder= [];
  public batch= [];
  public medChangeTicket= [];
  public returnClass = [];
  public newCards = []; 
  public reminders = {batches:[],reorders:[]};
  public awaitApprovalTicket = [];
  public awaitingBin = [];
  public medChanges = [];
  public inBinOrderCard =[];
  public dueTodayBin =[];
  public dueTodayOrderCard =[];
  public area = [];
  public inBin = [];
  public newRuns=[];
  public batches = [];
  public startDate = '';
  public endDate = '';
  public isReturn = [];
  public homeBinLoader:Boolean;
  public deliveryInTransit =[];
  public deliveryFinlize = [];
  public ischecked = "checked";
  public searchBarCoed = null;
  public scantext= null;
  public nonAutomatedPatCards = [];
  public awaitingAppPatCards = [];
  public deliveryPackages =[];
  public runList =[];
  public orderBarcode;
  public patientList = [];
  public navCounter={"Tickets":0,"reminder":0,"homeBin":0,"delivery":0,"production":0};
  public  ticketStatusColor = ['t-sunday','t-monday', 't-tuesday', 't-wednesday', 't-thursday', 't-friday'];

  public filter = {
  "new":Boolean,
  "production":Boolean,
  "reminder":Boolean,
  "ready":Boolean,
  "deliveryRun":Boolean,
}
shownGroup = null;
  constructor(private http: Http,) { }

  /*==================================================================
  WRITTEN BY:SEAMLESS CARE
  DECRIPTION: LOADER
  ===================================================================*/

  showLoading(showLoading = true) {
    if(showLoading) document.body.classList.add('loader');
    else  document.body.classList.remove('loader');
  }  


  /*==================================================================
  WRITTEN BY:SEAMLESS CARE
  DECRIPTION: CLOSE MODAL
  ===================================================================*/ 
  closeModel(idName = 'CloseButton') {
    var element = document.getElementById(idName) as any;
    element.click();
  }

  /*==================================================================
  WRITTEN BY:SEAMLESS CARE
  DECRIPTION: CLOSE MODAL
  ===================================================================*/ 

    indexWhere(object, condition) {
      const item = object.find(condition)
      return object.indexOf(item)
  }


  createDefaultImage(fullName) {
    if(fullName) {
      var firstName = fullName.split(' ').slice(0, -1).join(' ');
      var lastName = fullName.split(' ').slice(-1).join(' ');
      return firstName.charAt(0) + ' '+lastName.charAt(0);
    }
  }


  // timeSince(date) {
  //   var seconds = Math.floor((new Date() - date) / 1000);
  //   var interval = Math.floor(seconds / 31536000);
  //   if (interval > 1) {
  //     return interval + " years";
  //   }
  //   interval = Math.floor(seconds / 2592000);
  //   if (interval > 1) {
  //     return interval + " months";
  //   }
  //   interval = Math.floor(seconds / 86400);
  //   if (interval > 1) {
  //     return interval + " days";
  //   }
  //   interval = Math.floor(seconds / 3600);
  //   if (interval > 1) {
  //     return interval + " hours";
  //   }
  //   interval = Math.floor(seconds / 60);
  //   if (interval > 1) {
  //     return interval + " minutes";
  //   }
  //   return Math.floor(seconds) + " seconds";
  // }

  /*==================================================================
  WRITTEN BY:SEAMLESS CARE
  DECRIPTION: ADD DAY IN EXISTING DATE
  ===================================================================*/ 
    addDay(datePass,day){
    var existingDate = new Date(datePass);
    return existingDate.setDate( existingDate.getDate() + parseInt(day) );
    }

    activeNav(e){
      for(var i in this.filter){
        if(i == e) this.filter[i] = true
        else this.filter[i] = false
      }
    }
 
  /*==================================================================
  WRITTEN BY:SEAMLESS CARE
  DECRIPTION: CHNAGE DATE FROM IN (yyyy-mm-dd)
  ===================================================================*/

  dateFormat(dt){
    var dateObj = new Date(dt);
    var mobtn = dateObj.getMonth() + parseInt('1');
      return dateObj.getFullYear()+'-'+mobtn+'-'+dateObj.getDate();
  }

  /*==================================================================
  WRITTEN BY:SEAMLESS CARE
  DECRIPTION: CHNAGE DATE FROM IN (yyyy-mm-dd H:m:s )
  ===================================================================*/

  dateFormatWithTime(dt){
    var dateObj = new Date(dt);
    var mobtn = dateObj.getMonth() + parseInt('1');
      return dateObj.getFullYear()+'-'+mobtn+'-'+dateObj.getDate()+' '+dateObj.getHours()+':'+dateObj.getMinutes()+':'+dateObj.getSeconds();
  }

  /*==================================================================
  WRITTEN BY:SEAMLESS CARE
  DECRIPTION: CHNAGE DATE FROM IN ( H:m:s )
  ===================================================================*/

  dateFormatTime(dt){
    var dateObj = new Date(dt);
      return dateObj.getHours()+':'+dateObj.getMinutes()+':'+dateObj.getSeconds();
  }

  /*==================================================================
  WRITTEN BY:SEAMLESS CARE
  DECRIPTION: ENCODE AND DECODE HTMLENTITES
  ===================================================================*/
    htmlentities = {
      /**
       * Converts a string to its html characters completely.
       *
       * @param {String} str String with unescaped HTML characters
       **/
      encode : function(str) {
        var buf = [];
        for (var i=str.length-1;i>=0;i--) {
          buf.unshift(['&#', str[i].charCodeAt(), ';'].join(''));
        }
        return buf.join('');
      },
      /**
       * Converts an html characterSet into its original character.
       *
       * @param {String} str htmlSet entities
       **/
      decode : function(str) {
        return str.replace(/&#(\d+);/g, function(match, dec) {
          return String.fromCharCode(dec);
        });
      }
    };


    managePatCardCollapse(e,key){
          if(e.target.classList.contains('atth-tkt-inner-icon-plus')) 
            this.returnClass[key] = true; 
          else 
            this.returnClass[key] = false; 
          
    }


    
    /*==================================================================
WRITTEN BY:SEAMLESS CARE
DECRIPTION: CONVERT DATE IN HUMAN READABLE FORM
===================================================================*/
dateConverter(date) {
  var dateObj = new Date(date);
  const monthNames = ["January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"
  ];
  var month = dateObj.getMonth() + 1; //months from 1-12
  var monthName = dateObj.getMonth();
  var day = dateObj.getDate();
  var year = dateObj.getFullYear();
  var newdate = day + ' '+ monthNames[monthName] + ' '+ year;
  return newdate;
  }

  toggleGroup(group) {
    if (this.isGroupShown(group))  this.shownGroup = null;
    else  this.shownGroup = group;
  }

isGroupShown(group) {
    return this.shownGroup === group;
}; 

/*==================================================================
WRITTEN BY:SEAMLESS CARE
DECRIPTION: GET DATE FROM DAY
===================================================================*/
getDateFromDay(day,hours,mints){
  var d = new Date();
  var n = d.getDay();
  d.setHours(hours);
  d.setMinutes(mints);  
  if(day == n)  return  d;
  else{
    d.setDate(d.getDate() +(day+(7-d.getDay())) % 7);
    return d
  }  
}


/*==================================================================
WRITTEN BY:SEAMLESS CARE
DECRIPTION: GET Which Day 
INPUT : date
===================================================================*/
getWhichDay(day){
  return  new Date(day).getDay();
}
isDate(s){
  return new Date(s);
}

// getDateFromDay(day,time){
//   var d = new Date();
//   var n = d.getDay();
//   if(day == n)  return  this.dateFormat(d);
//   else return this.dateFormat(d.setDate(d.getDate() + day));
// }

/*==================================================================
WRITTEN BY:SEAMLESS CARE
DECRIPTION: DELETE ORDERS(SOFT DELETE)
===================================================================*/

  deleteOrder(orderID){
    this.http.put(Constant.API_URL+'order/delete/',{"OrderId":orderID}).map(res => res.json()).subscribe(data => {
      console.log(data);
      }, err => {
        console.log(err);
    });
  }

  dateFormater(javascript_date) {
    if(javascript_date) {
    var date = new Date(javascript_date);
    return date.toISOString().slice(0, 19).replace('T', ' ');
    }
  }

  time_replase(time) {
    
    if(!time) {
      return;
    }
    time = new Date(time);
    switch (typeof time) {
      case 'number':
        break;
      case 'string':
        time = +new Date(time);
        break;
      case 'object':
        if (time.constructor === Date) time = time.getTime();
        break;
      default:
        time = +new Date();
    }
    var time_formats = [
      [60, 'seconds', 1], // 60
      [120, '1 minute ago', '1 minute from now'], // 60*2
      [3600, 'minutes', 60], // 60*60, 60
      [7200, '1 hour ago', '1 hour from now'], // 60*60*2
      [86400, 'hours', 3600], // 60*60*24, 60*60
      [172800, 'Yesterday', 'Tomorrow'], // 60*60*24*2
      [604800, 'days', 86400], // 60*60*24*7, 60*60*24
      [1209600, 'Last week', 'Next week'], // 60*60*24*7*4*2
      [2419200, 'weeks', 604800], // 60*60*24*7*4, 60*60*24*7
      [4838400, 'Last month', 'Next month'], // 60*60*24*7*4*2
      [29030400, 'months', 2419200], // 60*60*24*7*4*12, 60*60*24*7*4
      [58060800, 'Last year', 'Next year'], // 60*60*24*7*4*12*2
      [2903040000, 'years', 29030400], // 60*60*24*7*4*12*100, 60*60*24*7*4*12
      [5806080000, 'Last century', 'Next century'], // 60*60*24*7*4*12*100*2
      [58060800000, 'centuries', 2903040000] // 60*60*24*7*4*12*100*20, 60*60*24*7*4*12*100
    ];
    var seconds = (+new Date() - time) / 1000,
      token = 'ago',
      list_choice = 1;
  
    if (seconds == 0) {
      return 'now'
    }
    if (seconds < 0) {
      seconds = Math.abs(seconds);
      token = 'ago';
      list_choice = 2;
    }
    var i = 0,
      format;
    while (format = time_formats[i++])
      if (seconds < format[0]) {
        if (typeof format[2] == 'string')
          return format[list_choice];
        else
          return Math.floor(seconds / format[2]) + ' ' + format[1] + ' ' + token;
      }
    return time;
  }
}
