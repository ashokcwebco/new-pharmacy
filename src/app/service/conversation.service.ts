import { Injectable } from '@angular/core';
import {Constant} from './../constant';
import {Http, Response, Headers, RequestOptions} from '@angular/http';
import { AuthService } from '../service/auth.service';


@Injectable()
export class ConversationService {

    constructor(
        private http: Http,
        private auth: AuthService,

    ) {}

    /*==================================================================
      DECRIPTION : SAVE CONVERSATION
      ARGUMENT : PharmacyTicketID
      ===================================================================*/
    data = {
        Msg: []
    };
    saveConversation(PharmacyTicketID) {
        var Msg = this.data.Msg[PharmacyTicketID];
        if (Msg) {
            this.loadingTicketConversations[PharmacyTicketID] = true;
            const header = new Headers();
            header.append("Content-Type", "application/json");
            const options = new RequestOptions({headers: header});
            var body = {
                PharmacyTicketID: PharmacyTicketID,
                Msg: Msg,
                UserID: this.auth.authCheck()['user_id'],
                name: this.auth.authCheck()['userName']
            };
            this.http.post(Constant.API_URL + 'conversation/', body, options).map(res => res.json()).subscribe(data => {
                this.loadingTicketConversations[PharmacyTicketID] = false;
                this.data.Msg[PharmacyTicketID] = '';
                this.ticketConversations[PharmacyTicketID].unshift(data);
            this.scrollToBottom(PharmacyTicketID);
                
            }, err => {

            });
        }
    }

    /*==================================================================
    DECRIPTION : GET CONVERSATION
    ARGUMENT : PharmacyTicketID
    ===================================================================*/
    ticketConversations = {
        '': []
    };
    loadingTicketConversations = {
        '': []
    };
    iHaveTicketConversations = {
        '': []
    };;
    getConversation(PharmacyTicketID) {
        this.loadingTicketConversations[PharmacyTicketID] = true;
        const header = new Headers();
        header.append("Content-Type", "application/json");
        const options = new RequestOptions({
            headers: header
        });
        this.http.get(Constant.API_URL + 'conversation/' + PharmacyTicketID, options).map(res => res.json()).subscribe(data => {
            this.loadingTicketConversations[PharmacyTicketID] = false;
            this.iHaveTicketConversations[PharmacyTicketID] = true;
            this.ticketConversations[PharmacyTicketID] = data;
            this.scrollToBottom(PharmacyTicketID);
        }, err => {
            this.loadingTicketConversations[PharmacyTicketID] = false;

        });
    }

    scrollToBottom(PharmacyTicketID) {
        return;
        /*var element = document.getElementById("content_bottom_"+PharmacyTicketID);
       if(element) {
        setTimeout(()=>{element.scrollIntoView(true)},200); 
       }*/
      }

}