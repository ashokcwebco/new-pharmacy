import { Injectable } from '@angular/core';
import { Constant } from '../../app/constant';

@Injectable()
export class AuthService {
  showHeader;
  constructor() { }

  /*==================================================================
  WRITTEN BY:
  DECRIPTION: Authentication
  ===================================================================*/
  authCheck() {
    if(localStorage.getItem(Constant.LOGIN_NAME)) {
      
        var data = {
          user_id : localStorage.getItem(Constant.LOGIN_STORAGE),
          userName : localStorage.getItem(Constant.LOGIN_NAME)
        };
        return data;
      } else {
        return false;
      }
  }

}
