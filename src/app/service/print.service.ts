import { Injectable } from '@angular/core';

@Injectable()
export class PrintService {
  constructor() { }

  print(id) {
    let printContents, popupWin;
    printContents = document.getElementById('print-section-'+id).innerHTML;
        popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
        popupWin.document.open();
        popupWin.document.write(`
        <html>
            <head>
            <style>
            //........Customized style.......
            </style>
            </head>
        <body onload="window.print();window.close()">${printContents}</body>
        </html>`
        );
        popupWin.document.close();
    }

    printDeliverySheet(pkgID){
        let printContents, popupWin;
        printContents = document.getElementById('delivery-section-'+pkgID).innerHTML;
            popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
            popupWin.document.open();
            popupWin.document.write(`
            <html>
                <head>
                <style>
                //........Customized style.......
                </style>
                </head>
            <body onload="window.print();window.close()">${printContents}</body>
            </html>`
            );
            popupWin.document.close();
        }
    
}
