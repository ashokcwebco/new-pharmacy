import { Injectable, ViewContainerRef } from '@angular/core';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
@Injectable()
export class CustomOptionService {

constructor(public toastr: ToastsManager, vcr: ViewContainerRef){
  this.toastr.setRootViewContainerRef(vcr);
}



showSuccess(msg) {
  this.toastr.success(msg, 'Success!')
  
}

showError(msg) {
  this.toastr.error(msg, 'Oops!');
}

showWarning(msg) {
  this.toastr.warning(msg, 'Alert!');
}

showInfo(msg) {
  this.toastr.info(msg);
}

showCustom() {
  this.toastr.custom('<span style="color: red">Message in red.</span>', null, {enableHTML: true});
}
}
