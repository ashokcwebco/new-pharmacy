  htmlentities = {
      /**
       * Converts a string to its html characters completely.
       *
       * @param {String} str String with unescaped HTML characters
       **/
      encode : function(str) {
        var buf = [];
        
        for (var i=str.length-1;i>=0;i--) {
          buf.unshift(['&#', str[i].charCodeAt(), ';'].join(''));
        }
        
        return buf.join('');
      },
      /**
       * Converts an html characterSet into its original character.
       *
       * @param {String} str htmlSet entities
       **/
      decode : function(str) {
        return str.replace(/&#(\d+);/g, function(match, dec) {
          return String.fromCharCode(dec);
        });
      },

      GO_LIVE:'2018-01-01',
      barCodeOption:{
          type: "Code 128",
          x: 120,
          y: 40,
          maxWidth: 120,
          horizontalAlign: "center",
          verticalAlign: "top",
          height: 40 - 5
      },
      reduceHomePat : function(data){
        var dataObj = [];
        for(var i in data){
            dataObj.push({"Home":data[i][0],"PatCard":data[i]});
        }
        return dataObj;
      },

      reduceOrderByPat:function(obj){
        var dataObj = [];
    
        for(var i in obj){
          var  patObj = {"PatId":i,"HomeName": obj[i][0].HomeName, "OrderID":obj[i][0].OrderID,"FirstName":obj[i][0].FirstName,"LastName":obj[i][0].LastName,"BarCode":obj[i][0].BarCode,"BarCodeImage":obj[i][0].BarCodeImage,"DateCreated":obj[i][0].DateCreated};
          for(var j=0; j<obj[i].length; j++){
            delete obj[i][j].BarCodeImage;
            delete obj[i][j].BarCode;
            delete obj[i][j].HomeName;
            delete obj[i][j].FirstName;
            delete obj[i][j].LastName;
            delete obj[i][j].OrderID;
            delete obj[i][j].ID;
          }
        dataObj.push({"Patients":patObj,"RXDetails": obj[i]});
        }
      return dataObj;
      }
    };
    module.exports = htmlentities;