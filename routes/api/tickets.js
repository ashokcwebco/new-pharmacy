var mssql = require("mssql");
var dateTime = require('node-datetime');
var groupArray = require('group-array');
var bardcode = require("bardcode");
const commonFn = require('./commonFn');
module.exports = function(app, db) {
    var dt = dateTime.create();
    var created = dt.format('Y-m-d H:M:S');
    dbrequest = new mssql.Request(db);
    app.get('/ticket/type', (req, res) => {
        var dt = dateTime.create();
        var created = dt.format('Y-m-d H:M:S');
        dbrequest.query(`SELECT * FROM LocalPortal.dbo.TicketType WHERE status = 1`, function(error, results) {
            if (error) res.json(error);
            else res.json(results.recordset);
        })
    });
    /*==================================================================
    WRITTEN BY: SEAMLESS CARE
    DECRIPTION:CREATE NEW TICKET  AND ORDERS WITH PRNS
    ===================================================================*/

    app.post('/ticket/new-ticket', (req, res) => {
        var dt = dateTime.create();
        var created = dt.format('Y-m-d H:M:S');
        var orderRxQuery = '';
        if (req.body.prns.length > 0) {
            var prns = req.body.prns;
            for (var x = 0; x < prns.length; x++) {
                orderRxQuery += `INSERT INTO LocalPortal.dbo.OrderRx(OrderID,RxID) VALUES((SELECT MAX(OrderID) FROM LocalPortal.dbo.Orders),'${prns[x]}');`;
            }
        }
        var query = `INSERT INTO LocalPortal.dbo.PharmacyTickets(Title,Details,DeliveryDate,PatID,TicketType, DateCreated,UserIDCreated)
        VALUES('${req.body.title}','${req.body.Details}','${req.body.DeliveryDate}',${req.body.PatID}, ${req.body.TicketType}, '${created}',${req.body.UserIDCreated});
        INSERT INTO LocalPortal.dbo.Orders (TicketID,TicketType,Title,DateCreated,UserIDCreated,OrderStatus,PatID)
        VALUES((SELECT MAX(PharmacyTicketID) FROM  LocalPortal.dbo.PharmacyTickets),${req.body.TicketType},'${req.body.title}','${created}',${req.body.UserIDCreated},1,${req.body.PatID});
        ${orderRxQuery} 
        SELECT MAX(OrderID) as OrderID FROM LocalPortal.dbo.Orders`;
        dbrequest.query(query, function(error, results) {
            if (error) res.json(error);
            else {
                var order = results.recordset[0].OrderID;
                var barCd = "OD00000" + results.recordset[0].OrderID;
                var Image = bardcode.drawBarcode("svg", barCd, commonFn.barCodeOption);
                imageEncode = commonFn.encode(Image);
                var query6 = `UPDATE LocalPortal.dbo.Orders SET BarCode='${barCd}', BarCodeImage ='${imageEncode}'  WHERE OrderID= ${order};
                           
                            SELECT TOP 1 tt.TicketName, pt.Details,pt.TicketType, pt.DeliveryDate, pt.Title, pt.PharmacyTicketID,O.OrderID,H.HomeName,H.BinName,H.DefaultDeliveryTime,O.UserIDPackaged, pt.UserIDCreated, pt.UserIDPreApproval, pt.DateCreated, users.userName as createdBy, pt.PatID,PAT.FirstName, PAT.LastName,O.BarCode, O.BarCodeImage 
                            FROM LocalPortal.dbo.PharmacyTickets pt 
                            LEFT JOIN LocalPortal.dbo.Orders O on O.TicketID = pt.PharmacyTicketID
                            LEFT JOIN LocalPortal.dbo.TicketType tt ON tt.TicketTypeID = pt.TicketType
                            LEFT JOIN LocalPortal.dbo.Users users on users.user_id = pt.UserIDCreated
                            LEFT JOIN LocalPortal.dbo.HomePatRel HPR on HPR.PatID = pt.PatID
                            LEFT JOIN LocalPortal.dbo.Home H on H.HomeID = HPR.HomeID
                            LEFT JOIN Pharmacy.dbo.Pat PAT on PAT.ID = pt.PatID
                            WHERE  pt.DateComplete is null AND O.UserIDApproved is null ORDER BY PharmacyTicketID DESC  `;
                request.query(query6, function(error, result) {
                    if (error) res.json(error);
                    else res.json(result.recordsets[0]);
                });
            }
        })
    });

    /*==================================================================
    WRITTEN BY: SEAMLESS CARE
    DECRIPTION:UPDATE TICKET DELIVERY DATE
    ===================================================================*/
    app.put('/updateTicketDeliveryDate', (req, res) => {
        var query = `UPDATE LocalPortal.dbo.PharmacyTickets SET DeliveryDate = '${req.body.deliveryDate}' WHERE PharmacyTicketID= ${req.body.PharmacyTicketID}`;
        dbrequest.query(query, function(error, results) {
            if (error) res.json(error);
            else res.json(results);
        })
    });


     /*==================================================================
    WRITTEN BY: SEAMLESS CARE
    DECRIPTION:GET ALL NEW TICKET CREATED 
    ===================================================================*/
    app.get('/ticket/ticket-card', (req, res) => {
        var query = `SELECT tt.TicketName, pt.Details, pt.Title, pt.PharmacyTicketID, pt.DateCreated, pt.UserIDCreated, pt.PatID, pt.TicketType, users.userName as createdBy
        FROM LocalPortal.dbo.PharmacyTickets pt 
        LEFT JOIN LocalPortal.dbo.TicketType tt ON tt.TicketTypeID = pt.TicketType
        LEFT JOIN LocalPortal.dbo.Users users on users.user_id = pt.UserIDCreated
        WHERE pt.PharmacyTicketID not in (Select orders.TicketID FROM LocalPortal.dbo.Orders)`;
        dbrequest.query(query, function(error, results) {
            if (error) res.json(error);
            else res.json(results.recordset);
        })
    });



    /*==================================================================
    WRITTEN BY: SEAMLESS CARE
    DECRIPTION:GET ALL ATTACHED ORDER WITH TICKETS 
    ===================================================================*/
    app.get('/ticket/orders/:id', (req, res) => {
        var query = `SELECT  PAT.ID,Rx.ID as RxID,(SELECT TOP 1 OrderID FROM LocalPortal.dbo.Orders WHERE PatID= PAT.ID) as OrderID, PAT.FirstName,H.HomeName, PAT.LastName, DRG.BrandName, DRG.GenericName, DRG.Strength,RX.RxNum, DRG.DIN, DOC.LastName + ', ' + DOC.FirstName as 'doctor', RX.SIG
        FROM Pharmacy.dbo.Rx RX 
        LEFT JOIN Pharmacy.dbo.Drg DRG on DRG.ID = RX.DrgID
        LEFT JOIN Pharmacy.dbo.Pat PAT on PAT.ID = RX.PatID
        LEFT JOIN Pharmacy.dbo.Doc DOC on DOC.ID = RX.DocID 
        LEFT JOIN LocalPortal.dbo.HomePatRel MPR on MPR.PatID = PAT.ID
        LEFT JOIN LocalPortal.dbo.Home H on H.HomeID = MPR.HomeID
        WHERE Rx.ID IN (SELECT ORX.RxID FROM LocalPortal.dbo.Orders O INNER JOIN LocalPortal.dbo.OrderRx ORX on ORX.OrderID = O.OrderID WHERE O.TicketID = ${req.params.id} )`;
        dbrequest.query(query, function(error, results) {
            if (error) res.json(error);
            else {
                var attachedOrderData = groupArray(results.recordsets[0], "ID");
                var dataObj = [];
                for (var i in attachedOrderData)
                    dataObj.push({
                        "Patients": {
                            "PatId": i,
                            "HomeName": attachedOrderData[i][0].HomeName,
                            "OrderID": attachedOrderData[i][0].OrderID,
                            "FirstName": attachedOrderData[i][0].FirstName,
                            "LastName": attachedOrderData[i][0].LastName,
                            "DateCreated": attachedOrderData[i][0].DateCreated
                        },
                        "RXDetails": attachedOrderData[i]
                    });
                res.json(dataObj);
            }
        })
    });


    /*==================================================================
    WRITTEN BY: SEAMLESS CARE
    DECRIPTION:PACKAGE ATTACHED ORDER 
    ===================================================================*/
    app.put('/ticket/orderPackage', (req, res) => {
        var dt = dateTime.create();
        var created = dt.format('Y-m-d H:M:S');
        var query = `UPDATE LocalPortal.dbo.Orders SET UserIDPackaged=${req.body.UserIDPackaged}, DatePackaged= '${created}' WHERE OrderID = ${req.body.OrderID}`;
        dbrequest.query(query, function(error, results) {
            if (error) res.json(error);
            else res.json(results);
        });
    });

    /*==================================================================
    WRITTEN BY: SEAMLESS CARE
    DECRIPTION:APPROVE ORDER 
    ===================================================================*/
    app.put('/ticket/orderApprove', (req, res) => {
        var dt = dateTime.create();
        var created = dt.format('Y-m-d H:M:S');
        var query = `UPDATE LocalPortal.dbo.Orders SET UserIDApproved=${req.body.UserIDPackaged}, DateApproved= '${created}' WHERE OrderID = ${req.body.OrderID}`;
        dbrequest.query(query, function(error, results) {
            if (error) res.json(error);
            else res.json(results);
        });
    });

    /*==================================================================
    WRITTEN BY: SEAMLESS CARE
    DECRIPTION:PRE-APPROVAL FOR MED CHANGES 
    ===================================================================*/
    app.put('/ticket/preApproval', (req, res) => {
        var dt = dateTime.create();
        var created = dt.format('Y-m-d H:M:S');
        var query = `UPDATE LocalPortal.dbo.PharmacyTickets SET UserIDPreApproval=${req.body.UserID}, DatePreApproval= '${created}' WHERE PharmacyTicketID = ${req.body.ticketId}`;
        dbrequest.query(query, function(error, results) {
            if (error) res.json(error);
            else res.json(results);
        });
    });


    /*
    Get all tickets In Production - Pre Approval 
    NOTE: THIS Will only be for ticketType = 3 "Med Changes"
    */
    app.get('/ticket/getMedChangesTicket', (req, res) => {
        var query = `SELECT tt.TicketName, pt.Details,pt.TicketType, pt.Title, pt.PharmacyTicketID, pt.UserIDCreated, pt.DateCreated, users.userName, pt.PatID 
        FROM LocalPortal.dbo.PharmacyTickets pt 
        LEFT JOIN LocalPortal.dbo.TicketType tt ON tt.TicketTypeID = pt.TicketType
        LEFT JOIN LocalPortal.dbo.Users users on users.user_id = pt.UserIDCreated
        WHERE pt.TicketType = 3 AND pt.UserIDPreApproval is null  AND pt.PharmacyTicketID IN (Select TicketID FROM LocalPortal.dbo.Orders)`;
        dbrequest.query(query, function(error, results) {
            if (error) res.json(error);
            else res.json(results.recordset);
        })
    });

    /*
    Get all tickets In Production - Automated 
    NOTE: orders.PackagerID > 0 means that the order is sent to an automated packager. 0 = non automated
    */
    app.get('/ticket/automated', (req, res) => {
        var query = `SELECT tt.TicketName, pt.Details,pt.TicketType, pt.Title, pt.PharmacyTicketID, pt.UserIDCreated, pt.DateCreated, users.userName, pt.PatID 
        FROM LocalPortal.dbo.PharmacyTickets pt 
        LEFT JOIN LocalPortal.dbo.TicketType tt ON tt.TicketTypeID = pt.TicketType
        LEFT JOIN LocalPortal.dbo.Users users on users.user_id = pt.UserIDCreated
        WHERE pt.PharmacyTicketID in 
        (Select orders.TicketID FROM LocalPortal.dbo.Orders orders WHERE orders.DatePackaged is null and orders.PackagerID > 0)`;
        dbrequest.query(query, function(error, results) {
            if (error) res.json(error);
            else res.json(results.recordset);
        })
    });
    // (SELECT OrderID FROM LocalPortal.dbo.OrderRx WHERE RxID= Rx.ID) as OrderID,
    /*==================================================================
    WRITTEN BY: SEAMLESS CARE
    DECRIPTION:GET PATIENT CARD DETAILS FOR  AUTOMATED TICKETS 
    INPUTS: TICKET ID
    ===================================================================*/
    app.get('/ticket/getAutoOrderPatCard/:id', (req, res) => {
        var query = `SELECT PAT.ID,Rx.ID as RxID, PAT.FirstName,H.HomeName,O.OrderID,O.BarCode,O.BarCodeImage, PAT.LastName, DRG.BrandName, DRG.GenericName, DRG.Strength,RX.RxNum, DRG.DIN, DOC.LastName + ', ' + DOC.FirstName as 'doctor', RX.SIG
        FROM LocalPortal.dbo.Orders O
        INNER JOIN LocalPortal.dbo.OrderRx ORX on ORX.OrderID = O.OrderID
        INNER JOIN Pharmacy.dbo.Rx RX on RX.ID = ORX.RxID
        LEFT JOIN Pharmacy.dbo.Drg DRG on DRG.ID = RX.DrgID
        LEFT JOIN Pharmacy.dbo.Pat PAT on PAT.ID = RX.PatID
        LEFT JOIN Pharmacy.dbo.Doc DOC on DOC.ID = RX.DocID 
        LEFT JOIN LocalPortal.dbo.HomePatRel MPR on MPR.PatID = PAT.ID
        LEFT JOIN LocalPortal.dbo.Home H on H.HomeID = MPR.HomeID
        WHERE O.TicketID = ${req.params.id} AND O.DatePackaged is null and O.PackagerID > 0`;
        dbrequest.query(query, function(error, results) {
            if (error) res.json(error);
            else res.json(commonFn.reduceOrderByPat(groupArray(results.recordsets[0], "ID")));
        })
    });

    /*
    Get all tickets In Production - Non-Automated 
    NOTE: orders.PackagerID = 0 means  non automated orders
    */
    app.get('/ticket/nonAutomated', (req, res) => {
        var query = `SELECT tt.TicketName, pt.Details, pt.Title,pt.TicketType, pt.PharmacyTicketID, pt.UserIDCreated,pt.DateCreated, users.userName, pt.PatID 
        FROM LocalPortal.dbo.PharmacyTickets pt 
        LEFT JOIN LocalPortal.dbo.TicketType tt ON tt.TicketTypeID = pt.TicketType
        LEFT JOIN LocalPortal.dbo.Users users on users.user_id = pt.UserIDCreated
        WHERE pt.PharmacyTicketID in 
        (Select orders.TicketID FROM LocalPortal.dbo.Orders orders WHERE orders.DatePackaged is null and orders.PackagerID = 0 )`;
        dbrequest.query(query, function(error, results) {
            if (error) res.json(error);
            else res.json(results.recordset);
        })
    });


    /*==================================================================
    WRITTEN BY: SEAMLESS CARE
    DECRIPTION:GET PATIENT CARD DETAILS FOR NON AUTOMATED TICKETS 
    ===================================================================*/
    app.get('/ticket/getNonAutoOrderPatCard/:id', (req, res) => {
        var query = `SELECT  PAT.ID,(SELECT TOP 1 OrderID FROM LocalPortal.dbo.Orders WHERE PatID= PAT.ID) as OrderID, PAT.FirstName,H.HomeName,Rx.ID as RxID, PAT.LastName, DRG.BrandName, DRG.GenericName, DRG.Strength,RX.RxNum, DRG.DIN, DOC.LastName + ', ' + DOC.FirstName as 'doctor', RX.SIG
        FROM Pharmacy.dbo.Rx RX 
        LEFT JOIN Pharmacy.dbo.Drg DRG on DRG.ID = RX.DrgID
        LEFT JOIN Pharmacy.dbo.Pat PAT on PAT.ID = RX.PatID
        LEFT JOIN Pharmacy.dbo.Doc DOC on DOC.ID = RX.DocID 
        LEFT JOIN LocalPortal.dbo.HomePatRel MPR on MPR.PatID = PAT.ID
        LEFT JOIN LocalPortal.dbo.Home H on H.HomeID = MPR.HomeID
        WHERE Rx.ID IN (SELECT ORX.RxID FROM LocalPortal.dbo.Orders O INNER JOIN LocalPortal.dbo.OrderRx ORX on ORX.OrderID = O.OrderID WHERE O.TicketID = ${req.params.id} AND O.DatePackaged is null and O.PackagerID = 0 )`;
        dbrequest.query(query, function(error, results) {
            if (error) res.json(error);
            else {
                var attachedOrderData = groupArray(results.recordsets[0], "ID");
                var dataObj = [];
                for (var i in attachedOrderData)
                    dataObj.push({
                        "Patients": {
                            "PatId": i,
                            "HomeName": attachedOrderData[i][0].HomeName,
                            "OrderID": attachedOrderData[i][0].OrderID,
                            "FirstName": attachedOrderData[i][0].FirstName,
                            "LastName": attachedOrderData[i][0].LastName,
                            "DateCreated": attachedOrderData[i][0].DateCreated
                        },
                        "RXDetails": attachedOrderData[i]
                    });
                res.json(dataObj);
            }
        })
    });

    /*
    Get all tickets In Production - Awaiting-Approval 
    order date approved is null
    */
    app.get('/ticket/awaitingApproveal', (req, res) => {
        var query = `SELECT tt.TicketName, pt.Details,TicketType, pt.Title, pt.PharmacyTicketID, pt.UserIDCreated, pt.DateCreated, users.userName, pt.PatID 
        FROM LocalPortal.dbo.PharmacyTickets pt 
        LEFT JOIN LocalPortal.dbo.TicketType tt ON tt.TicketTypeID = pt.TicketType
        LEFT JOIN LocalPortal.dbo.Users users on users.user_id = pt.UserIDCreated
        WHERE pt.PharmacyTicketID in 
        (Select orders.TicketID FROM LocalPortal.dbo.Orders orders WHERE orders.DatePackaged is not null AND orders.DateApproved is null)`;
        dbrequest.query(query, function(error, results) {
            if (error) res.json(error);
            else res.json(results.recordset);
        })
    });


    /*==================================================================
    WRITTEN BY: SEAMLESS CARE
    DECRIPTION:GET PATIENT CARD DETAILS FOR NON AUTOMATED TICKETS 
    ===================================================================*/
    app.get('/ticket/getAwaitingOrderPatCard/:id', (req, res) => {
        var query = `SELECT PAT.ID,Rx.ID as RxID, PAT.FirstName,H.HomeName,O.OrderID,O.BarCode,O.BarCodeImage, PAT.LastName, DRG.BrandName, DRG.GenericName, DRG.Strength,RX.RxNum, DRG.DIN, DOC.LastName + ', ' + DOC.FirstName as 'doctor', RX.SIG
        FROM LocalPortal.dbo.Orders O
        INNER JOIN LocalPortal.dbo.OrderRx ORX on ORX.OrderID = O.OrderID
        INNER JOIN Pharmacy.dbo.Rx RX on RX.ID = ORX.RxID
        LEFT JOIN Pharmacy.dbo.Drg DRG on DRG.ID = RX.DrgID
        LEFT JOIN Pharmacy.dbo.Pat PAT on PAT.ID = RX.PatID
        LEFT JOIN Pharmacy.dbo.Doc DOC on DOC.ID = RX.DocID 
        LEFT JOIN LocalPortal.dbo.HomePatRel MPR on MPR.PatID = PAT.ID
        LEFT JOIN LocalPortal.dbo.Home H on H.HomeID = MPR.HomeID
        WHERE O.TicketID = ${req.params.id} AND O.DatePackaged is not null and O.DateApproved is null`;
        dbrequest.query(query, function(error, results) {
            if (error) res.json(error);
            else res.json(commonFn.reduceOrderByPat(groupArray(results.recordsets[0], "ID")));
        })
    });
    /***** CREATED BY AHEMAD *** */
    // SELECT tt.TicketName, pt.Details, pt.Title, pt.PharmacyTicketID, pt.UserIDCreated, pt.DateCreated, users.userName, pt.PatID 
    // FROM LocalPortal.dbo.PharmacyTickets pt 
    // LEFT JOIN LocalPortal.dbo.TicketType tt ON tt.TicketTypeID = pt.TicketType
    // LEFT JOIN LocalPortal.dbo.Users users on users.user_id = pt.UserIDCreated
    // WHERE pt.PharmacyTicketID in 
    // (Select orders.TicketID FROM LocalPortal.dbo.Orders orders WHERE orders.DatePackaged is not null 
    // AND orders.DateApproved is not null AND orders.DateBinned is null)

    /*
    Get all tickets Ready - In-Bin 
    Bin 
    */
    app.get('/ticket/ticket-card1', (req, res) => {
        var query = `SELECT tt.TicketName, pt.Details, pt.Title, pt.PharmacyTicketID, pt.DateCreated, users.userName, pt.PatID 
        FROM LocalPortal.dbo.PharmacyTickets pt 
        LEFT JOIN LocalPortal.dbo.TicketType tt ON tt.TicketTypeID = pt.TicketType
        LEFT JOIN LocalPortal.dbo.Users users on users.user_id = pt.UserIDCreated
        WHERE pt.PharmacyTicketID in 
        (Select orders.TicketID FROM LocalPortal.dbo.Orders orders WHERE orders.DatePackaged is not null 
        AND orders.DateApproved is not null AND orders.DateBagged is null AND orders.BinID > 0 )
        ORDER BY pt.PharmacyTicketID DESC`;
        dbrequest.query(query, function(error, results) {
            if (error) res.json(error);
            else res.json(results.recordset);
        })
    });
    /*
    Get ReOrder Tickets 
    NOTE: 
    */
    app.get('/ticket/reorders', (req,res) => {
        var query = `SELECT tt.TicketName, pt.Details,pt.TicketType,pt.DeliveryDate, pt.Title, pt.PharmacyTicketID,O.OrderID,H.HomeName,H.BinName,H.DefaultDeliveryTime,O.UserIDPackaged, pt.UserIDCreated, pt.DateCreated, users.userName as createdBy, pt.PatID,PAT.FirstName, PAT.LastName,O.BarCode, O.BarCodeImage 
        FROM LocalPortal.dbo.PharmacyTickets pt 
        LEFT JOIN LocalPortal.dbo.Orders O on O.TicketID = pt.PharmacyTicketID
        LEFT JOIN LocalPortal.dbo.TicketType tt ON tt.TicketTypeID = pt.TicketType
        LEFT JOIN LocalPortal.dbo.Users users on users.user_id = pt.UserIDCreated
        LEFT JOIN LocalPortal.dbo.HomePatRel HPR on HPR.PatID = pt.PatID
        LEFT JOIN LocalPortal.dbo.Home H on H.HomeID = HPR.HomeID
        LEFT JOIN Pharmacy.dbo.Pat PAT on PAT.ID = pt.PatID
        WHERE pt.TicketType = 2 AND pt.DateComplete is null AND O.UserIDApproved is null`;
        dbrequest.query(query, function(error, results) {
            if (error) res.json(error);
            else res.json(results.recordsets[0]);
        })
    });


    app.get('/reorder/RxCards/:id', (req, res) => {
        var query = `SELECT Rx.ID as RxID, DRG.BrandName, DRG.GenericName, DRG.Strength,RX.RxNum, DRG.DIN, DOC.LastName + ', ' + DOC.FirstName as 'doctor', RX.SIG
        FROM Pharmacy.dbo.Rx RX 
        LEFT JOIN Pharmacy.dbo.Drg DRG on DRG.ID = RX.DrgID
        LEFT JOIN Pharmacy.dbo.Doc DOC on DOC.ID = RX.DocID 
        WHERE Rx.ID IN (SELECT ORX.RxID FROM LocalPortal.dbo.Orders O INNER JOIN LocalPortal.dbo.OrderRx ORX on ORX.OrderID = O.OrderID WHERE O.TicketID = ${req.params.id} )`;
        dbrequest.query(query, function(error, results) {
            if (error) res.json(error);
            else  res.json(results.recordsets[0]);
        })
    });

    /*
    Get General Tickets 
    NOTE: 
    */
    app.get('/ticket/general', (req,res) => {
        var query = `SELECT tt.TicketName, pt.Details,pt.TicketType,pt.DeliveryDate, pt.Title, pt.PharmacyTicketID,O.OrderID,H.HomeName,H.BinName,H.DefaultDeliveryTime,O.UserIDPackaged, pt.UserIDCreated, pt.DateCreated, users.userName as createdBy, pt.PatID,PAT.FirstName, PAT.LastName,O.BarCode, O.BarCodeImage 
        FROM LocalPortal.dbo.PharmacyTickets pt 
        LEFT JOIN LocalPortal.dbo.Orders O on O.TicketID = pt.PharmacyTicketID
        LEFT JOIN LocalPortal.dbo.TicketType tt ON tt.TicketTypeID = pt.TicketType
        LEFT JOIN LocalPortal.dbo.Users users on users.user_id = pt.UserIDCreated
        LEFT JOIN LocalPortal.dbo.HomePatRel HPR on HPR.PatID = pt.PatID
        LEFT JOIN LocalPortal.dbo.Home H on H.HomeID = HPR.HomeID
        LEFT JOIN Pharmacy.dbo.Pat PAT on PAT.ID = pt.PatID
        WHERE pt.TicketType = 1 AND pt.DateComplete is null AND O.UserIDApproved is null`;
        dbrequest.query(query, function(error, results) {
            if (error) res.json(error);
            else res.json(results.recordsets[0]);
        })
    });

    /*
    Get medchange
    NOTE: 
    */
    app.get('/ticket/medchanges', (req,res) => {
        var query = `SELECT tt.TicketName, pt.Details,pt.TicketType,pt.DeliveryDate, u.userName as Pharmacist, pt.Title, pt.PharmacyTicketID,O.OrderID,H.HomeName,H.BinName,H.DefaultDeliveryTime,O.UserIDPackaged, pt.UserIDCreated, pt.UserIDPreApproval, pt.DateCreated, users.userName as createdBy, pt.PatID,PAT.FirstName, PAT.LastName,O.BarCode, O.BarCodeImage 
        FROM LocalPortal.dbo.PharmacyTickets pt 
        LEFT JOIN LocalPortal.dbo.Orders O on O.TicketID = pt.PharmacyTicketID
        LEFT JOIN LocalPortal.dbo.TicketType tt ON tt.TicketTypeID = pt.TicketType
        LEFT JOIN LocalPortal.dbo.Users users on users.user_id = pt.UserIDCreated
        LEFT JOIN LocalPortal.dbo.Users u on u.user_id = pt.UserIDPreApproval
        LEFT JOIN LocalPortal.dbo.HomePatRel HPR on HPR.PatID = pt.PatID
        LEFT JOIN LocalPortal.dbo.Home H on H.HomeID = HPR.HomeID
        LEFT JOIN Pharmacy.dbo.Pat PAT on PAT.ID = pt.PatID
        WHERE pt.TicketType = 3 AND pt.DateComplete is null AND O.UserIDApproved is null`;
        dbrequest.query(query, function(error, results) {
            if (error) res.json(error);
            else res.json(results.recordsets[0]);
        })
    });

       /*
    Get medchange
    NOTE: 
    */
   app.get('/ticket/batch', (req,res) => {
    var query = `SELECT O.orderID, tt.TicketName, pt.Details,pt.TicketType,pt.DeliveryDate, u.userName as Pharmacist, pt.Title, pt.PharmacyTicketID,O.OrderID,H.HomeName,H.BinName,H.DefaultDeliveryTime,O.UserIDPackaged, pt.UserIDCreated, pt.UserIDPreApproval, pt.DateCreated, users.userName as createdBy, pt.PatID,PAT.FirstName, PAT.LastName,O.BarCode, O.BarCodeImage 
    FROM LocalPortal.dbo.PharmacyTickets pt 
    LEFT JOIN LocalPortal.dbo.Orders O on O.TicketID = pt.PharmacyTicketID
    LEFT JOIN LocalPortal.dbo.TicketType tt ON tt.TicketTypeID = pt.TicketType
    LEFT JOIN LocalPortal.dbo.Users users on users.user_id = pt.UserIDCreated
    LEFT JOIN LocalPortal.dbo.Users u on u.user_id = pt.UserIDPreApproval
    LEFT JOIN LocalPortal.dbo.HomePatRel HPR on HPR.PatID = pt.PatID
    LEFT JOIN LocalPortal.dbo.Home H on H.HomeID = HPR.HomeID
    LEFT JOIN Pharmacy.dbo.Pat PAT on PAT.ID = pt.PatID
    WHERE pt.TicketType = 4 AND pt.DateComplete is null AND O.UserIDApproved is null`;
    dbrequest.query(query, function(error, results) {
        if (error) res.json(error);
        else res.json(results.recordsets[0]);
    })
});

 

    /*==================================================================
    DECRIPTION:OPERATION PAGE LEFT PANEL COUNTERS 
    ===================================================================*/
    app.get('/counter', (req, res) => {
        var query = `SELECT COUNT(ReminderID) as ReminderCount,
        (SELECT COUNT(PharmacyTicketID) FROM LocalPortal.dbo.PharmacyTickets WHERE DateComplete is null AND TicketType != 4) as TicketCount,
        (SELECT COUNT(HomeID) FROM LocalPortal.dbo.Home ) as BinsCount,
        (SELECT COUNT(DeliveryRunID) FROM LocalPortal.dbo.DeliveryRuns) as DeliveryCount,
        (SELECT COUNT(PharmacyTicketID) FROM LocalPortal.dbo.PharmacyTickets WHERE DateComplete is null AND TicketType = 4) as batchTicketCount,
        (SELECT COUNT(BatchNum) FROM Pharmacy.dbo.PackagerBatch WHERE DateCreated > '${commonFn.GO_LIVE}' AND BatchNum NOT IN (SELECT BatchNum FROM LocalPortal.dbo.Orders)) as batchPkgCount
        FROM LocalPortal.dbo.Reminder`
        request.query(query, function(error, results) {
            if (error) res.json(error);
            else {
                res.json(results.recordset[0]);
            }
        })
    });

    /*==================================================================
    DECRIPTION:COMPLETE TICKET  
    ===================================================================*/

    app.put('/ticket/complete', (req, res) => {
        var dt = dateTime.create();
        var created = dt.format('Y-m-d H:M:S');
        var query = `UPDATE LocalPortal.dbo.PharmacyTickets  SET DateComplete = '${created}', UserIDCompleted = ${req.body.UserID} WHERE PharmacyTicketID = ${req.body.TicketID};
        UPDATE LocalPortal.dbo.Orders SET UserIDPackaged =${req.body.UserID}, DatePackaged ='${created}', DateBinned='${created}' ,UserIDBinned= ${req.body.UserID}, UserIDApproved=${req.body.UserID}, DateApproved= '${created}' WHERE TicketID = ${req.body.TicketID}`;
        dbrequest.query(query, function(error, results) {
            if (error) res.json(error);
            else res.json(results);
        });
    });

    /*==================================================================
    DECRIPTION:SAVING CONVERSAION  
    ===================================================================*/

    app.post('/conversation', (req, res) => {
        var dt = dateTime.create();
        var DateCreated = dt.format('Y-m-d H:M:S');
        var query = `INSERT INTO LocalPortal.dbo.PharmacyTicketChat (PharmacyTicketID, Msg, UserID, DateCreated)
         VALUES (${req.body.PharmacyTicketID}, '${req.body.Msg}', '${req.body.UserID}', '${DateCreated}')`;
        request.query(query, function(error, results) {
            if (error) res.json(error);
            else res.json({
                PharmacyTicketID: req.body.PharmacyTicketID,
                Msg: req.body.Msg,
                UserID: req.body.UserID,
                DateCreated: DateCreated,
                name: req.body.name
            });
        });
    });

    /*==================================================================
    DECRIPTION :GET CONVERSAIONS
    INPUT : PharmacyTicketID  
    ===================================================================*/

    app.get('/conversation/:PharmacyTicketID', (req, res) => {
        var query = `SELECT 
        tc.*,
        (SELECT userName from LocalPortal.dbo.Users as U WHERE U.user_id = tc.UserID) as name
        FROM LocalPortal.dbo.PharmacyTicketChat as tc WHERE tc.PharmacyTicketID = ${req.params.PharmacyTicketID} ORDER BY tc.PharmacyTicketChatID DESC`;
        request.query(query, function(error, results) {
            if (error) res.json(error);
            else res.json(results.recordset);
        });
    });



}