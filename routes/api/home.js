var mssql = require("mssql");
var bardcode = require("bardcode");
const commonFn = require('./commonFn');


module.exports = function(app, caregiver, loaclPortal) {

    /*==================================================================
    DECRIPTION: SAVING HOME
    ===================================================================*/
    app.post('/home', (req, res) => {
        query = `INSERT INTO LocalPortal.dbo.home (AgencyID, HomeName, HomePhone,BinName, DefaultDeliveryDay, DefaultDeliveryTime,SubAreaID,AreaID) VALUES ('${req.body.AgencyID}', '${req.body.HomeName}', '${req.body.HomePhone}', '${req.body.BinName}', '${req.body.DefaultDeliveryDay}', '${req.body.DefaultDeliveryTime}','${req.body.SubArea}','${req.body.Area}');SELECT SCOPE_IDENTITY() as HomeID`;
        request.query(query, function(error, results) {
            if (error) {
                res.json(error);
            } else {
                var HomeID_detail = results.recordset[0];
                var HomeID = HomeID_detail.HomeID;
                // Creating Barcode
                var barCd = "BIN000000" + HomeID;
                var Image = bardcode.drawBarcode("svg", barCd, commonFn.barCodeOption);
                Image = commonFn.encode(Image);
                //--------------------
                query_bin = `UPDATE LocalPortal.dbo.home SET BinBarcode = '${barCd}', BinBarcodeImage = '${Image}' WHERE HomeID = ${HomeID}`;
                request.query(query_bin, function(error_update, results_update) {
                    if (error_update) {
                        res.json(error_update);
                    } else {
                        var result = {
                            BinBarcode: barCd,
                            BinBarcodeImage: Image,
                            HomeID: HomeID
                        }
                        res.json(result);
                    }
                })

            }
        })
    });

    /*==================================================================
    DECRIPTION: UPDATING HOME
    ARGUMENT : HomeID
    ===================================================================*/
    app.put('/home/:HomeID', (req, res) => {
        query = `UPDATE LocalPortal.dbo.home 
                SET  AgencyID = ${req.body.AgencyID},
                HomeName = '${req.body.HomeName}',
                HomePhone = '${req.body.HomePhone}',
                BinName = '${req.body.BinName}',
                AreaID = '${req.body.Area}',
                SubAreaID = '${req.body.SubArea}',
                DefaultDeliveryDay = '${req.body.DefaultDeliveryDay}',
                DefaultDeliveryTime = '${req.body.DefaultDeliveryTime}'
                WHERE HomeID = ${req.params.HomeID}`;
        request.query(query, function(error, results) {
            if (error) {
                res.json(error);
            } else {
                res.json(results);
            }
        })
    });


    app.get('/home/:AgencyID/:start', (req, res) => {
        var limit = 20;
        var query = `
        SELECT H.HomeID, H.AgencyID,H.SubAreaID,H.AreaID, H.HomeName, H.HomePhone, H.DefaultDeliveryDay, H.BinName, HA.Address1,HA.Address2,HA.Title, HA.City, HA.Prov, HA.Postal,HA.HomeAddressesID,H.DefaultDeliveryTime, H.BinBarcode, H.BinBarcodeImage
        FROM LocalPortal.dbo.home as H
        LEFT JOIN LocalPortal.dbo.HomeAddresses HA on (HA.HomeID = H.HomeID AND HA.Active = 1)
        WHERE H.AgencyID = ${req.params.AgencyID}
        ORDER BY HomeID DESC OFFSET ${req.params.start} ROWS FETCH NEXT ${limit} ROWS ONLY;
        SELECT COUNT(HomeID) as total FROM LocalPortal.dbo.home WHERE AgencyID = ${req.params.AgencyID}`;
        request.query(query, function(error, results) {
            if (error) {
                res.json(error);
            } else {
                var data = {
                    "homes": "",
                    "total_homes": ""
                };
                data['homes'] = results.recordsets[0];
                data['total_homes'] = results.recordsets[1][0].total;
                res.json(data);

            }
        })
    });

     /*==================================================================
    DECRIPTION: GET AGENCY HOME
    ARGUMENT : AgencyID, START VALUE 
    ===================================================================*/

    app.get('/isPrimary/:HomeID', (req, res) => {
        var query = `SELECT HomeID FROM LocalPortal.dbo.HomeAddresses  WHERE Active=1 AND HomeID = ${req.params.HomeID}`;
        request.query(query, function(error, results) {
            if (error) res.json(error);
            else  res.json(results.recordsets[0]);
        })
    });
     /*==================================================================
    DECRIPTION: GET HOME PATIENT
    ARGUMENT : HomeID, START VALUE
    ===================================================================*/
    app.get('/homePat/:HomeID/:start', (req, res) => {
        var limit = 20;
        var query = `SELECT LocalPortal.dbo.HomePatRel.id, LocalPortal.dbo.HomePatRel.HomeID, LocalPortal.dbo.HomePatRel.PatID,Pharmacy.dbo.Pat.FirstName, Pharmacy.dbo.Pat.LastName  
                    FROM LocalPortal.dbo.HomePatRel
                    JOIN Pharmacy.dbo.Pat ON Pharmacy.dbo.Pat.ID = LocalPortal.dbo.HomePatRel.PatID
                    WHERE LocalPortal.dbo.HomePatRel.HomeID = ${req.params.HomeID} 
                    ORDER BY LocalPortal.dbo.HomePatRel.id DESC OFFSET ${req.params.start} ROWS FETCH NEXT ${limit} ROWS ONLY;
                    SELECT COUNT(LocalPortal.dbo.HomePatRel.HomeID) as total FROM LocalPortal.dbo.HomePatRel WHERE LocalPortal.dbo.HomePatRel.HomeID = ${req.params.HomeID}
                    `;
        request.query(query, function(error, results) {
            if (error) {
                res.json(error);
            } else {

                var data = {
                    "HomePats": "",
                    "total_HomePats": ""
                };
                data['HomePats'] = results.recordsets[0];
                data['total_HomePats'] = results.recordsets[1][0].total;
                res.json(data);

            }
        })
    });

     /*==================================================================
    DECRIPTION: ADD PATIENT IN HOME
    ===================================================================*/
    app.post('/HomePat', (req, res) => {
        query = `INSERT INTO LocalPortal.dbo.HomePatRel (HomeID, PatID) VALUES ('${req.body.HomeID}', '${req.body.PatID}');SELECT SCOPE_IDENTITY() as HomePatID`;
        request.query(query, function(error, results) {
            if (error) {
                res.json(error);
            } else {
                res.json(results.recordset[0].HomePatID);
            }
        })
    });

     /*==================================================================
    DECRIPTION: DELETE PATIENT FROM HOME
    ARGUMENT : PatID
    ===================================================================*/
    app.delete('/HomePat/:id', (req, res) => {
        request.query(`DELETE FROM LocalPortal.dbo.HomePatRel WHERE id =${req.params.id}`, function(error, results) {
            if (error) {
                res.json(error);
            } else {
                res.json(results);
            }
        })
    });


        /*==================================================================
    DECRIPTION: SAVING HOME ADDRESS
    ===================================================================*/
    app.post('/HomeAddresses', (req, res) => {
        var query = '';
        if(req.body.Active== 1){
            query +=  `UPDATE LocalPortal.dbo.HomeAddresses SET Active = 0 WHERE HomeID = ${req.body.HomeID};`;
        }
        query += `INSERT INTO LocalPortal.dbo.HomeAddresses (HomeID,Title, Address1,Address2, City, Prov, Postal,Active,DeliveryInstructions) VALUES (${req.body.HomeID},'${req.body.Title}','${req.body.HomeAddress1}','${req.body.HomeAddress2}', '${req.body.HomeCity}', '${req.body.HomeProv}', '${req.body.HomePostal}',${req.body.Active},'${req.body.DeliveryInstructions}');SELECT SCOPE_IDENTITY() as HomeAddressesID`;
        request.query(query, function(error, results) {
            if (error) res.json(error);
            else res.json(results);
            
        })
    });


    /*==================================================================
    DECRIPTION: GET HOME ADDRESS
    ARGUMETNS : HomeID
    ===================================================================*/
    app.get('/HomeAddresses/:HomeID', (req, res) => {

        query = `SELECT * FROM LocalPortal.dbo.HomeAddresses WHERE HomeID = ${req.params.HomeID} ORDER BY HomeAddressesID DESC`;
        request.query(query, function(error, results) {
            if (error) res.json(error);
            else res.json(results.recordsets[0]);
        })
    });

        /*==================================================================
    DECRIPTION: UPDATING HOME ADDRESS
    ARGUMENT : HomeAddressesID
    ===================================================================*/
    app.put('/HomeAddresses/:HomeAddressesID', (req, res) => {
        var query = '';
        if(req.body.Active== 1){
            query +=  `UPDATE LocalPortal.dbo.HomeAddresses SET Active = 0 WHERE HomeID = ${req.body.HomeID};`;
        }
        query += `UPDATE LocalPortal.dbo.HomeAddresses 
                SET 
                Title = '${req.body.Title}',
                Address1 = '${req.body.HomeAddress1}',
                Address2 = '${req.body.HomeAddress2}',
                City = '${req.body.HomeCity}',
                Prov = '${req.body.HomeProv}',
                Postal = '${req.body.HomePostal}',
                Active = ${req.body.Active},
                DeliveryInstructions = '${req.body.DeliveryInstructions}'
                WHERE HomeAddressesID = ${req.params.HomeAddressesID};`;
        request.query(query, function(error, results) {
            if (error) res.json(error);
            else res.json(results);
        })
    });


    /*==================================================================
    DECRIPTION: GET AREA
    ARGUMETNS : 
    ===================================================================*/
    app.get('/home/area', (req, res) => {
        request.query('SELECT * FROM LocalPortal.dbo.Area', function(error, results) {
            if (error)res.json(error);
            else res.json(results.recordsets[0]);
        })
    });

    /*==================================================================
    DECRIPTION: GET AREA
    ARGUMETNS : 
    ===================================================================*/
    app.get('/home/area/subArea/:AreaID', (req, res) => {
        var query = `SELECT SA.SubAreaID, SA.SubAreaName 
        FROM LocalPortal.dbo.Area  A 
        LEFT JOIN LocalPortal.dbo.SubArea SA on SA.AreaID = A.AreaID 
        WHERE SA.AreaID = ${req.params.AreaID}`;
        request.query(query, function(error, results) {
            if (error)res.json(error);
            else res.json(results.recordsets[0]);
        })
    });

}