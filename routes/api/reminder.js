var mssql = require("mssql");
var dateTime = require('node-datetime');
var groupArray = require('group-array');
var bardcode = require("bardcode");
const commonFn = require('./commonFn');
module.exports = function(app, db) {
    request = new mssql.Request(db);
    var dt = dateTime.create();
    var created = dt.format('Y-m-d H:M:S');

    /*=============================================================================
     WRITTEN BY: SEAMLESS CARE
     DECRIPTION:FILTER TODAY PACKAGE ORDER OF EACH PATIENT OR BY DRUG GENERIC NAME  
     ==============================================================================*/
    app.get('/reminders/filterPatientOrStrip/:keyword', (req, res) => {
        var query = `SELECT PAT.FirstName, PAT.LastName,RX.RxNum,RX.SIG,DRG.GenericName FROM Pharmacy.dbo.Rx RX
        LEFT JOIN Pharmacy.dbo.Drg DRG on DRG.ID = RX.DrgID
        LEFT JOIN Pharmacy.dbo.Pat PAT on PAT.ID = RX.PatID
        WHERE RX.PatID IN (SELECT ID FROM Pharmacy.dbo.Pat WHERE FirstName  LIKE TRIM('%${req.params.keyword}') OR LastName LIKE TRIM('%${req.params.keyword}'))
        ORDER BY RX.ID DESC OFFSET 1 ROWS FETCH NEXT 10 ROWS ONLY`;
        request.query(query, function(error, results) {
            if (error) res.json(error);
            else res.json(results.recordsets[0]);
        })
    });
    // AND (DATEDIFF(Day, PB.DateCreated, getdate() ) <= 0)


    /*=============================================================================
    WRITTEN BY: SEAMLESS CARE
    DECRIPTION:SAVE REMINDER  
    ==============================================================================*/
    app.post('/reminders/save', (req, res) => {
        var query = `INSERT INTO LocalPortal.dbo.Reminder 
        (Title,TicketType,Details, Active,RepeatOn,DaysInterval,DateLastCreated,UserIDLastCreated, DueDate, PatID)
        VALUES('${req.body.Title}', '${req.body.TicketType}', '${req.body.Details}', '${req.body.status}', '${req.body.repeat}', '${req.body.dayIntervel}','${created}', '${req.body.UserIDLastCreated}', '${req.body.DueDate}', '${req.body.PatID}');SELECT SCOPE_IDENTITY() AS ReminderID`;
        request.query(query, function(error, results) {
            if (error) res.json(error);
            else {
                var ReminderID = results.recordset[0].ReminderID;
                if (req.body.TicketType == 2) {
                    // Saving ReminderRx
                    var prns = req.body.prns;
                    var ReminderRxQuery = '';
                    for (var x = 0; x < prns.length; x++) {
                        ReminderRxQuery += `INSERT INTO LocalPortal.dbo.ReminderRx 
             (ReminderID, RxID, PatID)
             VALUES('${ReminderID}','${prns[x].ID}', '${req.body.PatID}');`;
                    }
                    request.query(ReminderRxQuery, function(error, results) {
                        if (error) res.json(error);
                        else res.json('success');
                    });
                    //------------------
                } else {
                    res.json('success');
                }
            }
        })
    });

    /*=============================================================================
    WRITTEN BY: SEAMLESS CARE
    DECRIPTION:Update REMINDER  
    ==============================================================================*/
    app.put('/reminders/update', (req, res) => {
        var query = `UPDATE LocalPortal.dbo.Reminder SET Title = '${req.body.Title}', DueDate = '${req.body.DueDate}',  Details = '${req.body.Details}', Active = ${req.body.Active},
        RepeatOn = ${req.body.RepeatOn}, DaysInterval=${req.body.DaysInterval} WHERE ReminderID = ${req.body.ReminderID}`;
        request.query(query, function(error, results) {
            if (error) res.json(error);
            else res.json(results);
        })
    });

    //   /*=============================================================================
    // WRITTEN BY: SEAMLESS CARE
    // DECRIPTION:DELETE REMINDER  
    // ==============================================================================*/
    // app.delete('/reminders/:id', (req,res) => { 
    //     var query = `DELETE FROM LocalPortal.dbo.Reminder  WHERE ReminderID = ${req.params.RxID}`;
    //     request.query(query,function (error, results) {
    //        if(error) res.json(error);
    //        else res.json(results); 
    //     })
    // });

    /*=============================================================================
    WRITTEN BY: SEAMLESS CARE
    DECRIPTION:SAVE REMINDER  
    ==============================================================================*/
    app.get('/reminders', (req, res) => {
        var query = `
        SELECT R.*,TT.TicketName, Pat.FirstName + ' '+Pat.LastName as PatName,
        STUFF((SELECT ','+CAST(RRX.RxID AS VARCHAR) FROM LocalPortal.dbo.ReminderRx as RRX WHERE RRX.ReminderID = R.ReminderID FOR XML PATH('')), 1, 1, '') as Prns
        FROM LocalPortal.dbo.Reminder R
        LEFT JOIN LocalPortal.dbo.TicketType TT on TT.TicketTypeID = R.TicketType
        LEFT JOIN Pharmacy.dbo.Pat as Pat ON Pat.ID  = R.PatID
        ORDER BY R.DueDate ASC`;
        request.query(query, function(error, results) {
            if (error) res.json(error);
            else res.json(results.recordsets[0]);
        })
    });

    app.get('/remindersFilter/:keyword', (req, res) => {
        var query = `SELECT R.*,TT.TicketName, Pat.FirstName + ' '+Pat.LastName as PatName FROM LocalPortal.dbo.Reminder R
        LEFT JOIN LocalPortal.dbo.TicketType TT on TT.TicketTypeID = R.TicketType
        LEFT JOIN Pharmacy.dbo.Pat as Pat ON Pat.ID  = R.PatID
        WHERE (R.Details LIKE '%${req.params.keyword}%' OR R.Title LIKE '%${req.params.keyword}%')
        ORDER BY R.DueDate ASC
        `;
        request.query(query, function(error, results) {
            if (error) res.json(error);
            else res.json(results.recordsets[0]);
        })
    });

    app.get('/reminders/barcode', (req, res) => {

        var barCd = "OD000002011";
        var Image = bardcode.drawBarcode("svg", barCd, commonFn.barCodeOption);
        Image = commonFn.encode(Image);
        var query6 = `INSERT INTO LocalPortal.dbo.OrderBarCode (BarCode,OrderID,BarCodeImage)
          VALUES('${barCd}',2525,'${Image}')`;
        request.query(query6, function(error, results) {
            if (error) res.json(error);
            else res.json(results);
        });

    });

    /*==================================================================
    DECRIPTION: Get ReminderRx  
    ===================================================================*/
    app.get('/ReminderRx/:ReminderID', (req, res) => {
        query = `SELECT drg.GenericName + ' ' + drg.Strength +  ' Doc: ' + doc.LastName + ', ' + doc.FirstName + ' Rem: ' + CAST(rx.RemQty AS VARCHAR(16)) + ' Rx:' + CAST(rx.RxNum AS VARCHAR(16)) as DrgInfo, RRX.RxID, rx.RxNum FROM LocalPortal.dbo.ReminderRx as RRX
                 LEFT JOIN Pharmacy.dbo.Rx as RX ON RX.ID = RRX.RxID 
                 LEFT JOIN Pharmacy.dbo.Drg drg on DrgID = drg.ID
                 LEFT JOIN Pharmacy.dbo.Doc doc on DocID = doc.ID
                 WHERE RRX.ReminderID = ${req.params.ReminderID}`
        request.query(query, function(error, results) {
            if (error) res.json(error);
            else res.json(results.recordset);
        });
    });

        /*==================================================================
    DECRIPTION:FROM REMINDER CREATE NEW TICKET AND ORDERS WITH PRNS
    ===================================================================*/

    app.post('/reminders/ticket/new-ticket', (req, res) => {
        var dt = dateTime.create();
        var created = dt.format('Y-m-d H:M:S');
        var Prns = req.body.Prns.split(",");
        var orderRxQuery = '';
        if (Prns.length > 0) {
            for (var x = 0; x < Prns.length; x++) {
                orderRxQuery += `INSERT INTO LocalPortal.dbo.OrderRx(OrderID,RxID) VALUES((SELECT MAX(OrderID) FROM LocalPortal.dbo.Orders),'${Prns[x]}');`;
            }
            var query = `INSERT INTO LocalPortal.dbo.PharmacyTickets(Title,Details,DeliveryDate,PatID,TicketType, DateCreated,UserIDCreated)
            VALUES('${req.body.title}','${req.body.Details}','${req.body.DeliveryDate}',${req.body.PatID}, ${req.body.TicketType}, '${created}',${req.body.UserIDCreated});
            INSERT INTO LocalPortal.dbo.Orders (TicketID,TicketType,Title,DateCreated,UserIDCreated,OrderStatus,PatID)
            VALUES((SELECT MAX(PharmacyTicketID) FROM  LocalPortal.dbo.PharmacyTickets),${req.body.TicketType},'${req.body.title}','${created}',${req.body.UserIDCreated},1,${req.body.PatID});
            ${orderRxQuery} 
            SELECT MAX(OrderID) as OrderID FROM LocalPortal.dbo.Orders`;
            dbrequest.query(query, function(error, results) {
                if (error) res.json(error);
                else {
                    var order = results.recordset[0].OrderID;
                    var barCd = "OD00000" + results.recordset[0].OrderID;
                    var Image = bardcode.drawBarcode("svg", barCd, commonFn.barCodeOption);
                    imageEncode = commonFn.encode(Image);
                    var query6 = `UPDATE LocalPortal.dbo.Orders SET BarCode='${barCd}', BarCodeImage ='${imageEncode}'  WHERE OrderID= ${order};
                               
                                SELECT TOP 1 tt.TicketName, pt.Details,pt.TicketType, pt.Title, pt.PharmacyTicketID,O.OrderID,H.HomeName,H.BinName,H.DefaultDeliveryTime,O.UserIDPackaged, pt.UserIDCreated, pt.UserIDPreApproval, pt.DateCreated, users.userName as createdBy, pt.PatID,PAT.FirstName, PAT.LastName,O.BarCode, O.BarCodeImage 
                                FROM LocalPortal.dbo.PharmacyTickets pt 
                                LEFT JOIN LocalPortal.dbo.Orders O on O.TicketID = pt.PharmacyTicketID
                                LEFT JOIN LocalPortal.dbo.TicketType tt ON tt.TicketTypeID = pt.TicketType
                                LEFT JOIN LocalPortal.dbo.Users users on users.user_id = pt.UserIDCreated
                                LEFT JOIN LocalPortal.dbo.HomePatRel HPR on HPR.PatID = pt.PatID
                                LEFT JOIN LocalPortal.dbo.Home H on H.HomeID = HPR.HomeID
                                LEFT JOIN Pharmacy.dbo.Pat PAT on PAT.ID = pt.PatID
                                WHERE  pt.DateComplete is null AND O.UserIDApproved is null ORDER BY PharmacyTicketID DESC  `;
                    request.query(query6, function(error, result) {
                        if (error) res.json(error);
                        else res.json(result.recordsets[0]);
                    });
                }
            })
        }
    });
}