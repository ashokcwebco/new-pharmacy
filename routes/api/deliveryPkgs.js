var mssql = require("mssql");
var dateTime = require('node-datetime');
var groupArray = require('group-array');
var bardcode = require("bardcode");
const commonFn = require('./commonFn');
module.exports = function(app,db){
    request = new mssql.Request(db);
    var dt = dateTime.create();
    var currentDate = dt.format('Y-m-d H:M:S');
/*==================================================================
WRITTEN BY: SEAMLESS CARE
DECRIPTION:CREATE NEW RUN  
===================================================================*/
// SELECT * FROM LocalPortal.dbo.DeliveryRuns DR WHERE DeliveryRunID = (SELECT SCOPE_IDENTITY() AS id)
    app.post('/createNewRun', (req,res) => {
        var query = `INSERT INTO LocalPortal.dbo.DeliveryRuns (Title,DateScheduled,DriverUserID,UserScheduled,DateCreated) 
            VALUES('${req.body.Title}','${req.body.DateScheduled}','${req.body.DriverUserID}',${req.body.UserScheduled},'${currentDate}');
            SELECT DR.DeliveryRunID,DR.DateScheduled,DR.DriverUserID,DR.Title,U.userName, UB.userName as ScheduledBy, DR.DateCreated
            FROM LocalPortal.dbo.DeliveryRuns DR
            LEFT JOIN LocalPortal.dbo.DeliveryPkgs DP on DP.DeliveryRunId = DR.DeliveryRunID
            LEFT JOIN LocalPortal.dbo.Users U on U.user_id = DR.DriverUserID
            LEFT JOIN LocalPortal.dbo.Users UB on UB.user_id = DR.UserScheduled WHERE DR.DeliveryRunID = (SELECT SCOPE_IDENTITY() AS id)`;
        dbrequest.query(query,function (error, results) {
            if(error) res.json(error);
            else  res.json(results.recordsets[0]);
        }) 
    });

/*==================================================================
WRITTEN BY: SEAMLESS CARE
DECRIPTION:GET ALL RUN 
===================================================================*/
    app.get('/getNewRun', (req,res) => {
        var query = `SELECT * FROM LocalPortal.dbo.DeliveryRuns WHERE DateStarted is null`;
        dbrequest.query(query,function (error, results) {
        if(error) res.json(error);
        else  res.json(results.recordsets[0]);
        })
    });

/*==================================================================
WRITTEN BY: SEAMLESS CARE
DECRIPTION:CREATE NEW DELIVERY PACKAGES WITH BAR CODE 
===================================================================*/
    app.post('/newDeliveryCard', (req,res) => {
        var query = `INSERT INTO LocalPortal.dbo.DeliveryPkgs (DeliveryRunId,Title,DeliveryInstructions,Address1, Address2, City,Prov,Postal,HomeID) 
        VALUES(${req.body.DeliveryRunId},'${req.body.Title}','${req.body.DeliveryInstructions}','${req.body.Address1}','${req.body.Address2}','${req.body.City}','${req.body.Prov}','${req.body.Postal}',${req.body.HomeID}); SELECT SCOPE_IDENTITY() AS id`;
        dbrequest.query(query,function (error, results) {
            if(error) res.json(error);
            else{
                var  pkgId =  results.recordset[0].id;
                var  barCd = "DP00000000"+pkgId;
                var Image =  bardcode.drawBarcode("svg",barCd, commonFn.barCodeOption);
                imageEncode = commonFn.encode(Image);
                var query = `UPDATE LocalPortal.dbo.DeliveryPkgs SET deliveryPkgCode='${barCd}', deliveryPkgCodeImg ='${imageEncode}'  WHERE DeliveryPkgId= ${pkgId};
                            SELECT DP.* FROM LocalPortal.dbo.DeliveryPkgs DP 
                            WHERE DeliveryPkgId= ${pkgId} AND  DP.DateDelivered is null `;
                request.query(query,function (error, results) {  
                        if(error) res.json(error);
                        else res.json(results.recordsets[0]);
                });
            }  
        })
    });

/*==================================================================
WRITTEN BY: SEAMLESS CARE
DECRIPTION:GET ALL PACKAGES 
INPUT: HOMEID
===================================================================*/
    app.get('/getPackages/:HomeID', (req,res) => {
        var query = `SELECT DP.* FROM LocalPortal.dbo.DeliveryPkgs DP 
            WHERE DP.HomeID = ${req.params.HomeID} AND  DP.DateDelivered is null`;
        dbrequest.query(query,function (error, results) {
        if(error) res.json(error);
        else  res.json(results.recordsets[0]);
        })
    });
/*==================================================================
WRITTEN BY: SEAMLESS CARE
DECRIPTION:GET ALL PACKAGES 
===================================================================*/
    app.get('/getPackages', (req,res) => {
        var query = `SELECT DP.* FROM LocalPortal.dbo.DeliveryPkgs DP 
            LEFT JOIN  LocalPortal.dbo.DeliveryRuns DR on DR.DeliveryRunID = DP.DeliveryRunId
            WHERE  DP.UserDelivered is null OR  DP.ReturnStatus = 1 ORDER BY DeliveryPkgId DESC`;
        dbrequest.query(query,function (error, results) {
        if(error) res.json(error);
        else  res.json(results.recordsets[0]);
        })
    });

/*==================================================================
WRITTEN BY: SEAMLESS CARE
DECRIPTION:GET HOEM ADDRESS FOR SHIPPING ADDRESS 
INPUT: HOMEID
===================================================================*/
    app.get('/getHome/:HomeID', (req,res) => {
        var query = `SELECT Address1,Address2,City,Prov,Postal FROM LocalPortal.dbo.Home WHERE HomeID = ${req.params.HomeID}`;
        dbrequest.query(query,function (error, results) {
        if(error) res.json(error);
        else  res.json(results.recordsets[0][0]);
        })
    });

/*==================================================================
WRITTEN BY: SEAMLESS CARE
DECRIPTION:GET PACKAGE ORDERS  
INPUT: PACKAGE ID
===================================================================*/
    app.get('/deliveryPkgsOrders/:PkgID', (req,res) => {
        var query = `SELECT PT.PharmacyTicketID, PT.DeliveryDate, PT.TicketType, PT.Details, O.OrderID, O.BarCode, O.DeliveryPkgID, PAT.LastName, PAT.FirstName, O.PatID, O.BarCodeImage 
        FROM LocalPortal.dbo.Orders O 
        INNER JOIN  LocalPortal.dbo.PharmacyTickets PT on PT.PharmacyTicketID = O.TicketID
        LEFT JOIN Pharmacy.dbo.Pat PAT on PAT.ID = O.PatID
        WHERE O.DeliveryPkgID = ${req.params.PkgID}`;
        dbrequest.query(query,function (error, results) {
        if(error) res.json(error);
        else  res.json(results.recordsets[0]);
        })
    });



    /*==================================================================
WRITTEN BY: SEAMLESS CARE
DECRIPTION:GET PACKAGE ORDERS  
INPUT: PACKAGE ID
===================================================================*/
app.get('/deliveryPkgsOrder/:PkgID', (req,res) => {
    var query = `SELECT PT.PharmacyTicketID, PT.DeliveryDate, PT.TicketType, PT.Details, O.OrderID, O.BarCode, O.DeliveryPkgID, PAT.LastName, PAT.FirstName, O.PatID, O.BarCodeImage 
    FROM LocalPortal.dbo.Orders O 
    INNER JOIN  LocalPortal.dbo.PharmacyTickets PT on PT.PharmacyTicketID = O.TicketID
    LEFT JOIN Pharmacy.dbo.Pat PAT on PAT.ID = O.PatID
    WHERE O.DeliveryPkgID = ${req.params.PkgID}`;
    dbrequest.query(query,function (error, results) {
    if(error) res.json(error);
    else  res.json(results.recordset[0]);
    })
});

/*==================================================================
WRITTEN BY: SEAMLESS CARE
DECRIPTION:MOVE DELIVERY PACKAGE TO IN RIN  
INPUT: DATEDELIVERED, USERDELIVERED, DELIVERYID
===================================================================*/

    app.put('/sendToDeliver', (req,res) => {
        var query = `UPDATE LocalPortal.dbo.DeliveryPkgs SET DateDelivered='${currentDate}',ReturnStatus = 0, UserDelivered =${req.body.UserDelivered}  WHERE DeliveryPkgId= ${req.body.DeliveryPkgId};
            
            SELECT DR.*,H.HomeName, H.DefaultDeliveryDay, H.DefaultDeliveryTime,U.userName
            FROM LocalPortal.dbo.DeliveryRuns DR
            LEFT JOIN LocalPortal.dbo.DeliveryPkgs DP on DP.DeliveryRunId = DR.DeliveryRunID
            LEFT JOIN LocalPortal.dbo.Users U on U.user_id = DR.DriverUserID
            LEFT JOIN LocalPortal.dbo.Home  H on H.HomeID = DP.HomeID
            WHERE DP.DateDelivered is not null AND (DR.DateStarted is null OR ReturnStatus = 0) AND DP.DeliveryPkgId = ${req.body.DeliveryPkgId}`;
        dbrequest.query(query,function (error, results) {
        if(error) res.json(error);
        else  res.json(results.recordsets[0]);
        })
    });

/*==================================================================
WRITTEN BY: SEAMLESS CARE
DECRIPTION:UPDATE DELIVERY INSTRUCTIONS 
INPUT: DeliveryInstructions, DELIVERYID
===================================================================*/

app.put('/updateDeliveryPkgs', (req,res) => {
    var query = `UPDATE LocalPortal.dbo.DeliveryPkgs SET DeliveryInstructions='${req.body.DeliveryInstructions}' WHERE DeliveryPkgId= ${req.body.DeliveryPkgId}`;
    dbrequest.query(query,function (error, results) {
    if(error) res.json(error);
    else  res.json(results);
    })
});

/*==================================================================
WRITTEN BY: SEAMLESS CARE
DECRIPTION:GET ALL RUNs
===================================================================*/
//  WHERE (DR.DateStarted is null AND DP.DateDelivered is not null ) OR (DP.ReturnStatus = 0 AND DR.DateStarted is not null) AND RunStatus is null
    app.get('/getDeliveryRun', (req,res) => {
        var query = `SELECT DR.DeliveryRunID,DR.DateScheduled,DR.DriverUserID,DR.Title,U.userName, UB.userName as ScheduledBy, DR.DateCreated
        FROM LocalPortal.dbo.DeliveryRuns DR
        LEFT JOIN LocalPortal.dbo.DeliveryPkgs DP on DP.DeliveryRunId = DR.DeliveryRunID
        LEFT JOIN LocalPortal.dbo.Users U on U.user_id = DR.DriverUserID
        LEFT JOIN LocalPortal.dbo.Users UB on UB.user_id = DR.UserScheduled
        WHERE DR.DateStarted is null OR (DP.ReturnStatus = 0 AND DR.DateStarted is not null) AND RunStatus is null
        GROUP BY DR.DeliveryRunID,DR.DateScheduled,DR.DriverUserID,DR.Title,U.userName, UB.userName,DR.DateCreated`;
        dbrequest.query(query,function (error, results) {
        if(error) res.json(error);
        else  res.json(results.recordsets[0]);
        })
    });

/*==================================================================
WRITTEN BY: SEAMLESS CARE
DECRIPTION:GET PACKAGE IN RUN
INPUT RUN ID  
===================================================================*/
app.get('/getPackageInRun/:DeliveryRunID', (req,res) => {
    var query = `SELECT DP.*,H.HomeName,H.DefaultDeliveryDay, H.DefaultDeliveryTime
    FROM LocalPortal.dbo.DeliveryPkgs DP
    LEFT JOIN LocalPortal.dbo.Home  H on H.HomeID = DP.HomeID
    LEFT JOIN LocalPortal.dbo.Orders O on O.DeliveryPkgID = DP.DeliveryPkgId
    WHERE DP.DateDelivered is not null AND DP.ReturnStatus=0 AND  DP.DeliveryRunID = ${req.params.DeliveryRunID} AND O.DateComplete is null `;
    dbrequest.query(query,function (error, results) {
    if(error) res.json(error);
    else  res.json(results.recordsets[0]);
    })
});

/*==================================================================
WRITTEN BY: SEAMLESS CARE
DECRIPTION:START RUN
INPUT: DeliveryInstructions, DELIVERYID
===================================================================*/

app.put('/updateRun', (req,res) => {
    var query = `UPDATE LocalPortal.dbo.DeliveryRuns 
    SET DateScheduled = '${req.body.DateScheduled}', 
    UserScheduled = ${req.body.UserScheduled},
    DriverUserID = ${req.body.DriverUserID},
    Title= '${req.body.Title}'
    WHERE DeliveryRunID= ${req.body.DeliveryPkgId}`;
    dbrequest.query(query,function (error, results) {
    if(error) res.json(error);
    else  res.json(results);
    })
});


/*==================================================================
WRITTEN BY: SEAMLESS CARE
DECRIPTION:UPDETE RUN DRIVER
INPUT: DRIVERID AND RUNID
===================================================================*/

app.put('/updateRunDriver', (req,res) => {
    var query = `UPDATE LocalPortal.dbo.DeliveryRuns 
    SET DriverUserID = ${req.body.DriverUserID}
    WHERE DeliveryRunID= ${req.body.RunID}`;
    dbrequest.query(query,function (error, results) {
    if(error) res.json(error);
    else  res.json(results);
    })
});

/*==================================================================
WRITTEN BY: SEAMLESS CARE
DECRIPTION:START RUN
INPUT: DeliveryInstructions, DELIVERYID
===================================================================*/

app.get('/getStartRun/:RunID/:userId', (req,res) => {
    var query = `UPDATE LocalPortal.dbo.DeliveryRuns SET DateStarted='${currentDate}', UserStarted= ${req.params.userId},RunStatus=1 WHERE DeliveryRunID= ${req.params.RunID};
    
    SELECT DR.*,H.HomeName, H.DefaultDeliveryDay, H.DefaultDeliveryTime,U.userName,US.userName as startedBy
    FROM LocalPortal.dbo.DeliveryRuns DR
    LEFT JOIN LocalPortal.dbo.DeliveryPkgs DP on DP.DeliveryRunId = DR.DeliveryRunID
    LEFT JOIN LocalPortal.dbo.Users U on U.user_id = DR.DriverUserID
    LEFT JOIN LocalPortal.dbo.Users US on US.user_id = DR.UserStarted
    LEFT JOIN LocalPortal.dbo.Home  H on H.HomeID = DP.HomeID
    WHERE  DR.DateStarted is not null AND DR.DateFinalized is null AND DP.ReturnStatus=0 AND DR.DeliveryRunId = ${req.params.RunID}`;
    dbrequest.query(query,function (error, results) {
    if(error) res.json(error);
    else  res.json(results.recordsets[0]);
    })
});

/*==================================================================
WRITTEN BY: SEAMLESS CARE
DECRIPTION:GET SPACIFIC RUN
INPUT: RUNID
===================================================================*/

app.get('/getRun/:RunID', (req,res) => {
    var query = `SELECT DR.DeliveryRunID, DR.DateScheduled, DR.DriverUserID, DR.Title, U.userName
    FROM LocalPortal.dbo.DeliveryRuns DR
    LEFT JOIN LocalPortal.dbo.DeliveryPkgs DP on DP.DeliveryRunId = DR.DeliveryRunID
    LEFT JOIN LocalPortal.dbo.Users U on U.user_id = DR.DriverUserID
    WHERE DR.DeliveryRunId = ${req.params.RunID}`;
    dbrequest.query(query,function (error, results) {
    if(error) res.json(error);
    else  res.json(results.recordsets[0][0]);
    })
});

/*==================================================================
WRITTEN BY: SEAMLESS CARE
DECRIPTION:GET ALL TRANSIT PACKAGES
===================================================================*/
app.get('/getDeliveryInTransit', (req,res) => {
    var query = `SELECT DR.*,U.userName, US.userName as startedBy
    FROM LocalPortal.dbo.DeliveryRuns DR
    LEFT JOIN LocalPortal.dbo.Users U on U.user_id = DR.DriverUserID
    LEFT JOIN LocalPortal.dbo.Users US on US.user_id = DR.UserStarted
    WHERE  DR.DateStarted is not null AND DR.DateFinalized is null AND RunStatus=1`;
    dbrequest.query(query,function (error, results) {
    if(error) res.json(error);
    else  res.json(results.recordsets[0]);
    })
});

/*==================================================================
WRITTEN BY: SEAMLESS CARE
DECRIPTION:GET FINALIZE DELIVERY PACKAGES
===================================================================*/
app.get('/getDeliveryFinalize', (req,res) => {
    var query = `SELECT DR.*,U.userName,UF.userName as FinalizedBy,US.userName as startedBy
    FROM LocalPortal.dbo.DeliveryRuns DR
    LEFT JOIN LocalPortal.dbo.Users U on U.user_id = DR.DriverUserID
    LEFT JOIN LocalPortal.dbo.Users UF on UF.user_id = DR.UserFinalized
    LEFT JOIN LocalPortal.dbo.Users US on US.user_id = DR.UserStarted
    WHERE DR.DateStarted is not null AND DR.DateFinalized is not null`;
    dbrequest.query(query,function (error, results) {
    if(error) res.json(error);
    else  res.json(results.recordsets[0]);
    })
});

/*==================================================================
WRITTEN BY: SEAMLESS CARE
DECRIPTION:DELIVERY RETURENED
INPUT: DateReturned, DELIVERYRUNID
===================================================================*/

app.get('/deliveryReturn/:RunID', (req,res) => {
    var query = `UPDATE LocalPortal.dbo.DeliveryRuns SET DateReturned='${currentDate}' WHERE DeliveryRunID= ${req.params.RunID}`;
    dbrequest.query(query,function (error, results) {
    if(error) res.json(error);
    else  res.json(results.recordsets[0]);
    })
});

/*==================================================================
WRITTEN BY: SEAMLESS CARE
DECRIPTION:RETURENED
INPUT: PACKAGE RETURN
===================================================================*/
app.get('/packageReturn/:pkgId/:status', (req,res) => {
    var query = `UPDATE LocalPortal.dbo.DeliveryPkgs SET ReturnStatus=${req.params.status},DateReturned='${currentDate}' WHERE deliveryPkgCode= '${req.params.pkgId}'`;
    dbrequest.query(query,function (error, results) {
    if(error) res.json(error);
    else  res.json(results);
    })
});

/*==================================================================
WRITTEN BY: SEAMLESS CARE
DECRIPTION:DELIVERY FINALIZED DLIVERY
INPUT: DateReturned, DELIVERYRUNID
===================================================================*/

app.get('/deliveryFinalize/:RunID/:userID', (req,res) => {
    var query = `UPDATE LocalPortal.dbo.DeliveryRuns SET DateFinalized='${currentDate}', UserFinalized=${req.params.userID} WHERE DeliveryRunID= ${req.params.RunID};
    SELECT DR.*,U.userName,UF.userName as FinalizedBy,US.userName as startedBy
    FROM LocalPortal.dbo.DeliveryRuns DR
    LEFT JOIN LocalPortal.dbo.Users U on U.user_id = DR.DriverUserID
    LEFT JOIN LocalPortal.dbo.Users UF on UF.user_id = DR.UserFinalized
    LEFT JOIN LocalPortal.dbo.Users US on US.user_id = DR.UserStarted
    WHERE DR.DateStarted is not null AND DR.DateFinalized is not null AND DR.DeliveryRunId = ${req.params.RunID}`;
    dbrequest.query(query,function (error, results) {
    if(error) res.json(error);
    else  res.json(results.recordsets[0]);
    })
});
// UPDATE LocalPortal.dbo.DeliveryRuns SET DateFinalized='${currentDate}', UserFinalized=${req.body.userID} WHERE DeliveryRunID= ${req.body.RunID}
app.put('/deliveryFinalize', (req,res) => {
    var query = `UPDATE LocalPortal.dbo.Orders SET DateComplete='${currentDate}', UserIDCompleted=${req.body.userID} WHERE DeliveryPkgID IN(${req.body.pkgIDs})`;
    dbrequest.query(query,function (error, results) {
    if(error) res.json(error);
    else  res.json(results);
    })
});
}