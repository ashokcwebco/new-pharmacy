var mssql = require("mssql");
var dateTime = require('node-datetime');
var groupArray = require('group-array');
var bardcode = require("bardcode");
const commonFn = require('./commonFn');
module.exports = function(app,db){
    request = new mssql.Request(db);
    var dt = dateTime.create();
    var created = dt.format('Y-m-d H:M:S');

    /*
    1A) Get All Automated Batches created between two dates
    INPUTS StartDate,  EndDate. (user can select the start and end date)
    */
    app.get('/order/getBatches', (req,res) => {
        var query = `SELECT PB.BatchNum, PB.PackagerId, PB.DateCreated, PB.Description, PB.StartDate, PB.EndDate, P.Description
        FROM Pharmacy.dbo.PackagerBatch PB 
        LEFT JOIN Pharmacy.dbo.Packager P on P.id = PB.PackagerId
        LEFT JOIN LocalPortal.dbo.Orders O on O.BatchNum != PB.BatchNum
        WHERE PB.DateCreated > '${commonFn.GO_LIVE}'`;  
        request.query(query,function (error, results) {
           if(error) res.json(error);
           else  res.json(results.recordsets);
        })
    });

    /*
    1B) Get Patients and Rxs for a Batch.
    INPUT: batchNum 
    */
    app.get('/order/getBatchDetails/:batchNum', (req,res) => {
        var query = `SELECT  PAT.ID, PAT.FirstName, PAT.LastName, PBR.RxNum, DRG.BrandName, DRG.GenericName, DRG.Strength, DRG.DIN, DOC.LastName + ', ' + DOC.FirstName as 'doctor', RX.SIG
        FROM Pharmacy.dbo.PackagerBatchRx PBR 
        LEFT JOIN Pharmacy.dbo.PackagerBatch PB on PBR.PackagerBatchId = PB.Id
        LEFT JOIN Pharmacy.dbo.Rx RX on Rx.RxNum = PBR.RxNum
        LEFT JOIN Pharmacy.dbo.Drg DRG on DRG.ID = RX.DrgID
        LEFT JOIN Pharmacy.dbo.Pat PAT on PAT.ID = RX.PatID
        LEFT JOIN Pharmacy.dbo.Doc DOC on DOC.ID = RX.DocID
        WHERE PB.BatchNum = ${req.params.batchNum}`;
        request.query(query,function (error, results) {
           if(error) res.json(error);
           else{var array =  groupArray(results.recordsets[0], 'ID');
                var dataObj = [];
                for(var i in array){
                    dataObj.push({"Patients":{"PatId":i,"FirstName":array[i][0].FirstName,"LastName":array[i][0].LastName},"RXDetails":array[i]});
                }
                res.json(dataObj);
             }
        })
    });
    /*
    1C) Create Automated Orders from a Batch
    TODO: 1) For each Patient from the list in 1B, create an Order recrod 
    TODO: 2) FOR each Patient Rx from the list in 1B create an OrderRx record
    */
    app.post('/order/createOrdersFromBatch', (req,res) => {
        var dt = dateTime.create();
        var created = dt.format('Y-m-d H:M:S');
        // DECLARE  @DeliveryDate datetime
        // IF((SELECT DATEPART(weekday,CURRENT_TIMESTAMP)) > (SELECT H.DefaultDeliveryDay FROM LocalPortal.dbo.Home H INNER JOIN LocalPortal.dbo.HomePatRel HPR on HPR.HomeID = H.HomeID WHERE HPR.PatID = 51266))
        //    @DeliveryDate datetime = (SELECT DATEADD(DAY, (DATEDIFF(DAY, (SELECT H.DefaultDeliveryDay -1 FROM LocalPortal.dbo.Home H INNER JOIN LocalPortal.dbo.HomePatRel HPR on HPR.HomeID = H.HomeID WHERE HPR.PatID = 51266) , GETDATE()) / 7) * 7 + 7, (SELECT H.DefaultDeliveryDay -1 FROM LocalPortal.dbo.Home H INNER JOIN LocalPortal.dbo.HomePatRel HPR on HPR.HomeID = H.HomeID WHERE HPR.PatID = 51266))) 
        //   ELSE
        //    @DeliveryDate datetime = (SELECT DATEADD(day, @NextDayID,CURRENT_TIMESTAMP))
        // Step 1 get batch patients and rxs. 
        // INCOMEPLETE Ashok to complete
        var query = `SELECT PB.PackagerId,PB.BatchNum,Rx.RxNum,RX.ID, RX.PatID
        FROM Pharmacy.dbo.PackagerBatchRx PBR 
        LEFT JOIN Pharmacy.dbo.PackagerBatch PB on PBR.PackagerBatchId = PB.Id
        LEFT JOIN Pharmacy.dbo.Rx RX on Rx.RxNum = PBR.RxNum
        WHERE PB.BatchNum = ${req.body.batchNum}`;
        request.query(query,function (error, results) {
           if(error) res.json(error);   
           else
           {   var rxNumByPackId =  groupArray(results.recordsets[0], 'PackagerId','PatID');
                var counterLoop = 0;
                var insertRx = 0;
                var TicketIds = [];
                for(var i in rxNumByPackId){
                    for(var k in rxNumByPackId[i]){ 
                         counterLoop = parseInt(rxNumByPackId[i][k].length) + parseInt(counterLoop);
                        var query2 = `INSERT INTO LocalPortal.dbo.PharmacyTickets(Title,Details,DeliveryDate,PatID,TicketType, DateCreated,UserIDCreated)
                        VALUES((SELECT H.HomeName FROM LocalPortal.dbo.Home H INNER JOIN LocalPortal.dbo.HomePatRel HPR on HPR.HomeID = H.HomeID WHERE HPR.PatID = ${k}),'',CURRENT_TIMESTAMP,${k}, 4, CURRENT_TIMESTAMP,${req.body.UserIDCreated}); SELECT PharmacyTicketID,PatID FROM LocalPortal.dbo.PharmacyTickets WHERE PharmacyTicketID = (SELECT SCOPE_IDENTITY() AS id)`;
                        request.query(query2,function (error, results) {
                            if(error) res.json(error);
                            else{ 
                                TicketIds.push(results.recordsets[0][0].PharmacyTicketID);
                                var query1 = `INSERT INTO LocalPortal.dbo.Orders (TicketID,TicketType,Title,PackagerID,DateCreated,UserIDCreated,OrderStatus,PatID,BatchNum)
                                VALUES(${results.recordsets[0][0].PharmacyTicketID},4,(SELECT H.HomeName FROM LocalPortal.dbo.Home H INNER JOIN LocalPortal.dbo.HomePatRel HPR on HPR.HomeID = H.HomeID WHERE HPR.PatID = ${results.recordsets[0][0].PatID} ),${i},CURRENT_TIMESTAMP,${req.body.UserIDCreated},4,${results.recordsets[0][0].PatID},${req.body.batchNum}); SELECT OrderID,PatID FROM LocalPortal.dbo.Orders WHERE OrderID = (SELECT SCOPE_IDENTITY() AS id)`;
                                request.query(query1,function (error, results) {
                                    if(error) res.json(error);
                                    else{ 
                                        var  barCd = "OD00000"+results.recordsets[0][0].OrderID;
                                        var Image =  bardcode.drawBarcode("svg",barCd, commonFn.barCodeOption);
                                        imageEncode = commonFn.encode(Image);
                                        var query6 = `UPDATE LocalPortal.dbo.Orders SET BarCode='${barCd}', BarCodeImage ='${imageEncode}'  WHERE OrderID= ${results.recordsets[0][0].OrderID}`;
                                        request.query(query6,function (error, results) {  
                                                if(error) res.json(error);
                                        });
                                        var  query3 = '';
                                        for(var j in rxNumByPackId[i][results.recordsets[0][0].PatID]){
                                                query3 += `INSERT INTO LocalPortal.dbo.OrderRx (OrderID,RxID) VALUES(${results.recordsets[0][0].OrderID},${rxNumByPackId[i][results.recordsets[0][0].PatID][j].ID});`;
                                            }
                                        request.query(query3,function (error, results) {  
                                            if(error) res.json(error);
                                            else {insertRx = parseInt(insertRx) + parseInt(results.rowsAffected.length); 
                                                if(insertRx == counterLoop){
                                                    // res.json(TicketIds.toString());
                                                    var query5 = `SELECT O.orderID, tt.TicketName, pt.Details,pt.TicketType,pt.DeliveryDate, u.userName as Pharmacist, pt.Title, pt.PharmacyTicketID,O.OrderID,H.HomeName,H.BinName,H.DefaultDeliveryTime,O.UserIDPackaged, pt.UserIDCreated, pt.UserIDPreApproval, pt.DateCreated, users.userName as createdBy, pt.PatID,PAT.FirstName, PAT.LastName,O.BarCode, O.BarCodeImage 
                                                    FROM LocalPortal.dbo.PharmacyTickets pt 
                                                    LEFT JOIN LocalPortal.dbo.Orders O on O.TicketID = pt.PharmacyTicketID
                                                    LEFT JOIN LocalPortal.dbo.TicketType tt ON tt.TicketTypeID = pt.TicketType
                                                    LEFT JOIN LocalPortal.dbo.Users users on users.user_id = pt.UserIDCreated
                                                    LEFT JOIN LocalPortal.dbo.Users u on u.user_id = pt.UserIDPreApproval
                                                    LEFT JOIN LocalPortal.dbo.HomePatRel HPR on HPR.PatID = pt.PatID
                                                    LEFT JOIN LocalPortal.dbo.Home H on H.HomeID = HPR.HomeID
                                                    LEFT JOIN Pharmacy.dbo.Pat PAT on PAT.ID = pt.PatID
                                                    WHERE pt.TicketType = 4 AND pt.DateComplete is null AND O.UserIDApproved is null AND PharmacyTicketID IN (${TicketIds.toString()})`;
                                                    request.query(query5,function (error, results) { 
                                                        res.json(results.recordsets[0]);
                                                    });
                                                }  
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }
                }
            }
        }); 
    });

    /*
    2A) Get Non Automated Rx for a specific home filled on 
        METHOD: GET
        INPUTS ticketId, HomeID.
    */
    app.get('/order/getNonAutomatedRx', (req,res) => {
        var query = `SELECT  Rx.RxNum, RX.ID as RxID, DRG.BrandName, DRG.GenericName, DRG.Strength, DRG.DIN, DOC.LastName + ', ' + DOC.FirstName as 'doctor', RX.SIG,PT.PatID
        FROM LocalPortal.dbo.PharmacyTickets PT
        INNER JOIN Pharmacy.dbo.Rx RX on RX.PatID = PT.PatID
        LEFT  JOIN Pharmacy.dbo.Drg DRG on DRG.ID = RX.DrgID
        LEFT JOIN Pharmacy.dbo.Doc DOC on DOC.ID = RX.DocID
        LEFT JOIN LocalPortal.dbo.PortalPatient PP on PP.PatID = PT.PatID
        WHERE PT.PharmacyTicketID = ${req.query.ticketId} AND RX.FillDate <= '${req.query.startDate}' AND RX.FillDate > '${req.query.endDate}'
        AND RX.ID NOT IN (SELECT ORX.RxID FROM LocalPortal.dbo.OrderRx ORX INNER JOIN LocalPortal.dbo.Orders O on O.OrderID = ORX.OrderID WHERE O.TicketID = ${req.query.ticketId})
        GROUP BY RX.ID, RX.RxNum, DRG.BrandName,DRG.GenericName,DRG.Strength,DRG.DIN,DOC.LastName,DOC.FirstName,RX.SIG,PT.PatID`;
        request.query(query,function (error, results) {
           if(error) res.json(error);
           else  res.json(results.recordset);
        })
    });
    /*
    2C) Get Automated patient Rx 
        METHOD: GET
        INPUTS ticketId, HomeID
    */
    app.get('/order/getAutomatedRx', (req,res) => {
        var query = `SELECT  RX.ID as RxID, RX.RxNum, DRG.BrandName, DRG.GenericName, DRG.Strength, DRG.DIN, DOC.LastName + ', ' + DOC.FirstName as 'doctor', RX.SIG,PT.PatID,PB.PackagerId
        FROM LocalPortal.dbo.PharmacyTickets PT
        INNER JOIN Pharmacy.dbo.Rx RX on RX.PatID = PT.PatID
        INNER JOIN Pharmacy.dbo.PackagerBatchRx PBR on PBR.RxNum = RX.RxNum
        INNER JOIN Pharmacy.dbo.PackagerBatch PB on PB.Id = PBR.PackagerBatchId
        LEFT JOIN Pharmacy.dbo.Drg DRG on DRG.ID = RX.DrgID
        LEFT JOIN Pharmacy.dbo.Doc DOC on DOC.ID = RX.DocID
        LEFT JOIN LocalPortal.dbo.PortalPatient PP on PP.PatID = PT.PatID
        WHERE PT.PharmacyTicketID = ${req.query.ticketId} AND PB.DateCreated <= '${req.query.startDate}' AND PB.DateCreated > '${req.query.endDate}'
        AND RX.ID NOT IN (SELECT ORX.RxID FROM LocalPortal.dbo.OrderRx ORX INNER JOIN LocalPortal.dbo.Orders O on O.OrderID = ORX.OrderID WHERE O.TicketID = ${req.query.ticketId}) 
        GROUP BY RX.ID, RX.RxNum, DRG.BrandName,DRG.GenericName,DRG.Strength,DRG.DIN,DOC.LastName,DOC.FirstName,RX.SIG,PT.PatID,PB.PackagerId`;
        request.query(query,function (error, results) {
           if(error) res.json(error);
           else  res.json(results.recordsets[0]);
        })
    });

    /*
    2C) Create order for automated and not automated Rxcards 
        METHOD:POST
    */

    app.post('/order/orderPatRxCards', (req,res) => {
        var dt = dateTime.create();
        var created = dt.format('Y-m-d H:M:S');
        var query = `INSERT INTO LocalPortal.dbo.Orders (TicketID,TicketType,Title,DateCreated,PackagerID,UserIDCreated,OrderStatus,PatID)
        VALUES(${req.body.TicketID},${req.body.TicketType},'${req.body.Title}','${created}',${req.body.PackagerId}, ${req.body.UserIDCreated},1,${req.body.PatID}); SELECT SCOPE_IDENTITY() AS id  `;
        request.query(query,function (error, results) {
        if(error) res.json(error);
        else{
            var query = '';
            var Id = results.recordsets[0][0].id;
            for(var i=0; i< req.body.RxIDs.length; i++ ){
                query +=`INSERT INTO LocalPortal.dbo.OrderRx (OrderID,RxID) VALUES(${Id}, ${req.body.RxIDs[i]});`;
            } 
            request.query(query,function (error, results) {
                if(error) res.json(error);
                else res.json(results.recordsets[0]);
            });
        }  
        })
    });


 /*
    Get all tickets Ready - Awaiting-Bin 
    Bin date is null
    */
//    WHERE O.DatePackaged is not null AND O.DateApproved is not null AND O.DateBinned is null;
   app.get('/order/getAwaitingBin', (req,res) => {
        var query = `SELECT PAT.ID as PatID ,PAT.FirstName,PAT.LastName,O.OrderID,O.BarCodeImage,O.BarCode, O.DateCreated,O.DatePackaged,U.userName as packedBy,UAP.userName as approvedBy,O.DateApproved,H.HomeName,BinName, H.HomeID
        FROM LocalPortal.dbo.Orders O 
        LEFT JOIN Pharmacy.dbo.Pat PAT on PAT.ID = O.PatID
        LEFT JOIN LocalPortal.dbo.Users U on U.user_id = O.UserIDPackaged
        LEFT JOIN LocalPortal.dbo.Users UAP on UAP.user_id = O.UserIDApproved
        LEFT JOIN LocalPortal.dbo.HomePatRel MPR on MPR.PatID = PAT.ID
        LEFT JOIN LocalPortal.dbo.Home H on H.HomeID = MPR.HomeID
        WHERE O.DatePackaged is not null AND O.DateApproved is not null AND O.DateBinned is null;`;
        dbrequest.query(query,function (error, results) {
            if(error) res.json(error);
            else  res.json(results.recordsets);
        })
    });


 /* 
    Get tickets Ready - Awaiting-Bin BY Order ID
    Bin date is null 
    */
   app.get('/order/getAwaitingBin/:orderId', (req,res) => {
        var query = `SELECT PAT.ID,PAT.FirstName,PAT.LastName,O.OrderID,O.BarCodeImage,O.BarCode, O.DateCreated,O.DatePackaged,U.userName as packedBy,UAP.userName as approvedBy,O.DateApproved,H.HomeName,BinName, H.HomeID
        FROM LocalPortal.dbo.Orders O 
        LEFT JOIN Pharmacy.dbo.Pat PAT on PAT.ID = O.PatID
        LEFT JOIN LocalPortal.dbo.Users U on U.user_id = O.UserIDPackaged
        LEFT JOIN LocalPortal.dbo.Users UAP on UAP.user_id = O.UserIDApproved
        LEFT JOIN LocalPortal.dbo.HomePatRel MPR on MPR.PatID = PAT.ID
        LEFT JOIN LocalPortal.dbo.Home H on H.HomeID = MPR.HomeID
        WHERE O.DatePackaged is not null AND O.DateApproved is not null AND O.DateBinned is null AND O.OrderID = ${req.params.orderId}`;
        dbrequest.query(query,function (error, results) {
        if(error) res.json(error);
        else  res.json(results.recordsets[0]);
        })
});


app.get('/order/getAwaitingBinMedDetails', (req,res) => {
    var query = `SELECT RX.ID,RX.PatID,DRG.BrandName, DRG.GenericName, DRG.Strength,RX.RxNum, DRG.DIN, DOC.LastName + ', ' + DOC.FirstName as 'doctor', RX.SIG 
    FROM Pharmacy.dbo.Rx RX 
    LEFT JOIN Pharmacy.dbo.Drg DRG on DRG.ID = RX.DrgID
    LEFT JOIN Pharmacy.dbo.Pat PAT on PAT.ID = RX.PatID
    LEFT JOIN Pharmacy.dbo.Doc DOC on DOC.ID = RX.DocID
    WHERE Rx.ID IN (SELECT ORX.RxID FROM LocalPortal.dbo.Orders O INNER JOIN LocalPortal.dbo.OrderRx ORX on ORX.OrderID = O.OrderID WHERE O.DatePackaged is not null AND O.DateApproved is not null AND O.DateBinned is null)  `;
    dbrequest.query(query,function (error, results) {
       if(error) res.json(error);
       else  res.json(results.recordsets);
    })
});

    /*
     Get tickets Ready - Awaiting-Bin cards details BY Order ID
    Bin date is null,  Order ID
    */
app.get('/order/getAwaitingBinMedDetails/:orderId', (req,res) => {
    var query = `SELECT RX.ID,RX.PatID,DRG.BrandName, DRG.GenericName, DRG.Strength,RX.RxNum, DRG.DIN, DOC.LastName + ', ' + DOC.FirstName as 'doctor', RX.SIG FROM Pharmacy.dbo.Rx RX 
    LEFT JOIN Pharmacy.dbo.Drg DRG on DRG.ID = RX.DrgID
    LEFT JOIN Pharmacy.dbo.Pat PAT on PAT.ID = RX.PatID
    LEFT JOIN Pharmacy.dbo.Doc DOC on DOC.ID = RX.DocID
    WHERE Rx.ID IN (SELECT ORX.RxID FROM LocalPortal.dbo.Orders O INNER JOIN LocalPortal.dbo.OrderRx ORX on ORX.OrderID = O.OrderID WHERE O.DatePackaged is not null AND O.DateApproved is not null AND O.DateBinned is null AND O.OrderID = ${req.params.orderId})  `;
    dbrequest.query(query,function (error, results) {
       if(error) res.json(error);
       else  res.json(results.recordsets[0]);
    })
});


app.get('/order/getOrderByBarCode/:barCode', (req,res) => { 
    var query = `SELECT PAT.ID,PAT.FirstName,PAT.LastName,O.OrderID,O.BarCodeImage,O.BarCode, O.DateCreated,O.DatePackaged,U.userName as packedBy,UAP.userName as approvedBy,O.DateApproved,H.HomeName 
    FROM LocalPortal.dbo.Orders O 
    LEFT JOIN Pharmacy.dbo.Pat PAT on PAT.ID = O.PatID
    LEFT JOIN LocalPortal.dbo.Users U on U.user_id = O.UserIDPackaged
    LEFT JOIN LocalPortal.dbo.Users UAP on UAP.user_id = O.UserIDApproved
    LEFT JOIN LocalPortal.dbo.HomePatRel MPR on MPR.PatID = PAT.ID
    LEFT JOIN LocalPortal.dbo.Home H on H.HomeID = MPR.HomeID
    WHERE O.DatePackaged is not null AND O.DateApproved is not null AND O.DateBinned is null AND O.BarCode = '${req.params.barCode}'`;
    dbrequest.query(query,function (error, results) {
       if(error) res.json(error);
       else  res.json(results.recordsets);
    })
});

  /*==================================================================
    WRITTEN BY: SEAMLESS CARE
    DECRIPTION:ORDER MOVE IN BIN  
    ===================================================================*/
    app.post('/order/moveInBin', (req,res) => {
        var dt = dateTime.create();
        var created = dt.format('Y-m-d H:M:S');
        var query = `UPDATE LocalPortal.dbo.Orders SET UserIDBinned=${req.body.UserIDBinned}, DateBinned= '${created}' WHERE OrderID = ${req.body.OrderID}`;
        dbrequest.query(query,function (error, results) {
            if(error) res.json(error);
            else  res.json(results);
        });
    });

  /*==================================================================
    WRITTEN BY: SEAMLESS CARE
    DECRIPTION: GET MEDICATION   
    ===================================================================*/
    app.get('/getMedication/:OrderID', (req,res) => {
       
        var query = `SELECT O.OrderID, ORX.RxID, DRG.BrandName, DRG.GenericName, DRG.Strength,RX.RxNum, DRG.DIN, DOC.LastName + ', ' + DOC.FirstName as 'doctor', RX.SIG
        FROM LocalPortal.dbo.Orders O
        LEFT JOIN LocalPortal.dbo.OrderRx ORX on ORX.OrderID = O.OrderID 
        LEFT JOIN Pharmacy.dbo.Rx RX on RX.ID = ORX.RxID
        LEFT JOIN Pharmacy.dbo.Drg DRG on DRG.ID = RX.DrgID
        LEFT JOIN Pharmacy.dbo.Doc DOC on DOC.ID = RX.DocID
        WHERE  O.OrderID=${req.params.OrderID}`;
        dbrequest.query(query,function (error, results) {
           if(error) res.json(error);
           else  res.json(results.recordsets[0]);
        })
    });


    app.put('/orderMoveInPackage', (req,res) => {
        var query = `UPDATE LocalPortal.dbo.Orders SET DeliveryPkgID=${req.body.DeliveryPkgID}, OrderStatus= 3 WHERE OrderID = ${req.body.OrderID}`;
        dbrequest.query(query,function (error, results) {
            if(error) res.json(error);
            else  res.json(results.rowsAffected[0]);
        })
    });


    app.post('/ReOrder/', (req,res) => {
        var dt = dateTime.create();
        var created = dt.format('Y-m-d H:M:S');
        var prns = req.body.prns;
        var query = `INSERT INTO LocalPortal.dbo.Orders (TicketID,TicketType,Title,DateCreated,UserIDCreated,OrderStatus,PatID)
        VALUES(${req.body.PharmacyTicketID},${req.body.TicketType},'${req.body.Title}','${created}',${req.body.UserIDCreated},1,${req.body.PatID});SELECT SCOPE_IDENTITY() as OrderID`;                  
        request.query(query,function (error, results) {
            if(error) res.json(error);
            else{ 
                var  order =  results.recordset[0].OrderID;
                var  barCd = "OD00000"+results.recordset[0].OrderID;
                var Image =  bardcode.drawBarcode("svg",barCd, commonFn.barCodeOption);
                imageEncode = commonFn.encode(Image);
                var queryUpdate = `UPDATE LocalPortal.dbo.Orders SET BarCode='${barCd}', BarCodeImage ='${imageEncode}'  WHERE OrderID= ${order}`;
                request.query(queryUpdate,function (error, results) {  
                    if(error) res.json(error);
                    else  {
                        var  queryRX = '';
                        for(var x = 0; x < prns.length; x++){
                                queryRX += `INSERT INTO LocalPortal.dbo.OrderRx (OrderID,RxID) VALUES(${order},${prns[x]});`;
                            }
                        request.query(queryRX,function (error, results) {  
                            if(error) res.json(error);
                            else  res.json('success');
                        });
                    }
                });
            }
        });
    });

    /* GET ORDER DETAIL
    INPUT: OrderIDs exp. 253,256
    */
    app.get('/order/detail/:orderIds', (req,res) => {
        var query = `SELECT O.OrderID,O.BarCode, O.BarCodeImage, O.DateCreated,PAT.ID,PAT.FirstName,PAT.LastName, H.BinName,H.HomeName, H.DefaultDeliveryDay, H.DefaultDeliveryTime
        FROM LocalPortal.dbo.Orders O
        LEFT JOIN Pharmacy.dbo.Pat PAT on PAT.ID = O.PatID
        LEFT JOIN LocalPortal.dbo.HomePatRel HPR on HPR.PatID = PAT.ID
        LEFT JOIN LocalPortal.dbo.Home H on H.HomeID = HPR.HomeID
        WHERE O.OrderID IN (${req.params.orderIds})`;
        dbrequest.query(query,function (error, results) {
           if(error) res.json(error);
           else  res.json(results.recordsets[0]);
        })
    });

    /* GET DELETE OREDR
        INPUT: OrderID 
        FornKey  is pending 
    */


    app.put('/order/delete/deleteOrder', (req,res) => {
        var query = `UPDATE LocalPortal.dbo.Orders SET OrderStatus=2 WHERE OrderID= ${req.body.OrderId}`;
        dbrequest.query(query,function (error, results) {
           if(error) res.json(error);
           else  res.json(results);
        })
    });


}