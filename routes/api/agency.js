var mssql = require("mssql");
module.exports = function(app,caregiver,loaclPortal){
    
    //     Inserting Agency
    app.post('/agency', (req,res) => {
        query = `INSERT INTO LocalPortal.dbo.agency (AgencyName, AgencyPhone, Address1, Address2, City, Prov, Postal) VALUES ('${req.body.AgencyName}', '${req.body.AgencyPhone}', '${req.body.Address1}', '${req.body.Address2}', '${req.body.City}', '${req.body.Prov}', '${req.body.Postal}');SELECT SCOPE_IDENTITY() as AgencyID`;
        request.query(query,function (error, results) {
            if(error) {
                res.json(error);
            } else {
                    res.json(results);
            }
        })
    });

        //     Update Agency
        app.put('/agency/:AgencyID', (req,res) => {
            var query = `UPDATE LocalPortal.dbo.agency SET AgencyName = '${req.body.AgencyName}', AgencyPhone = '${req.body.AgencyPhone}', Address1 = '${req.body.Address1}', Address2 = '${req.body.Address2}', City = '${req.body.City}', Prov='${req.body.Prov}', Postal='${req.body.Postal}' 
                            WHERE AgencyID = ${req.params.AgencyID}`;
            request.query(query,function (error, results) {
                if(error) {
                    res.json(error);
                } else {
                        res.json(results);
                }
            })
        });


        // get Agencies
        app.get('/agencies', (req,res) => {
            var query = `SELECT * FROM LocalPortal.dbo.agency ORDER BY AgencyID DESC;
            SELECT COUNT(AgencyID) as total FROM LocalPortal.dbo.agency`;
            request.query(query,function (error, results) {
                if(error) {
                    res.json(error);
                } else {                      
                    var data = { "agencies":"" ,"total_agencies": ""};
                    data['agencies'] = results.recordsets[0];
                    data['total_agencies'] = results.recordsets[1][0].total;
                    res.json(data);
                  
                }
            })
        });

                // get Agencies using filter
                app.get('/agencies/filter/:keyword', (req,res) => {
                    var query = `SELECT DISTINCT A.*
                    FROM LocalPortal.dbo.agency as A
                    LEFT JOIN LocalPortal.dbo.Home as H ON A.AgencyID = H.AgencyID
                    LEFT JOIN LocalPortal.dbo.HomePatRel as HPR ON HPR.HomeID = H.HomeID
                    LEFT JOIN Pharmacy.dbo.Pat P on  P.ID = HPR.PatID
                    LEFT JOIN LocalPortal.dbo.Users as U ON U.AgencyID = A.AgencyID
                    WHERE (A.AgencyName LIKE '%${req.params.keyword}%' OR H.HomeName LIKE '%${req.params.keyword}%' OR H.BinName LIKE '%${req.params.keyword}%' OR H.BinBarcode LIKE '%${req.params.keyword}%' OR U.userName LIKE '%${req.params.keyword}%' OR P.FirstName LIKE '%${req.params.keyword}%')
                    ORDER BY AgencyID DESC;
                    SELECT COUNT(AgencyID) as total FROM LocalPortal.dbo.agency`;
                    request.query(query,function (error, results) {
                        if(error) {
                            res.json(error);
                        } else {
                            var data = { "agencies":"" ,"total_agencies": ""};
                            data['agencies'] = results.recordsets[0];
                            data['total_agencies'] = results.recordsets[1][0].total;
                            res.json(data);
                          
                        }
                    })
                });


                app.get('/getHomeOrders/:HomeID', (req,res) => {
                    var query = `SELECT DISTINCT O.PatID, H.HomeID,O.OrderID, H.HomeName, H.BinName,H.BinBarcode,P.LastName, P.FirstName
                    FROM LocalPortal.dbo.Home H
                    LEFT JOIN LocalPortal.dbo.HomePatRel HPR on HPR.HomeID = H.HomeID
                    LEFT JOIN Pharmacy.dbo.Pat P on P.ID = HPR.PatID
                    LEFT JOIN LocalPortal.dbo.Orders O on O.PatID = P.ID
                    WHERE H.HomeID = ${req.params.HomeID} AND O.DatePackaged is not null  AND O.DateApproved is not null AND O.DateBagged is null AND O.DateBinned is not null`;
                    dbrequest.query(query,function (error, results) {
                        if(error) res.json(error);
                        else{
                            res.json(results.recordset);
                        }  
                    }) 
                });

                
}