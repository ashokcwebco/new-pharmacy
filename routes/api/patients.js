var mssql = require("mssql");
var dateTime = require('node-datetime');
module.exports = function(app,db){
    request1 = new mssql.Request(db);
    app.get('/patients', (req,res) => {
        var query = `SELECT TOP 10 ID, LastName, FirstName,  FirstName+' '+LastName as PatName FROM Pharmacy.dbo.Pat`;
        request1.query(query,function (error, results) {
           if(error) res.json(error);
           else  res.json(results.recordset);  
        })
    });

    app.get('/patients/filter/:keyword', (req,res) => {
        var query = `SELECT TOP 10 PAT.ID, PAT.FirstName,  PAT.LastName, HOME.HomeName, AGENCY.AgencyName, HOME.DefaultDeliveryDay, DATEPART(hh,HOME.DefaultDeliveryTime) as hour,  DATEPART(n,HOME.DefaultDeliveryTime) as minute
        FROM Pharmacy.dbo.Pat PAT
        LEFT JOIN LocalPortal.dbo.HomePatRel HOMEPAT ON HOMEPAT.PatID = Pat.ID
        LEFT JOIN LocalPortal.dbo.Home HOME ON HOME.HomeID = HOMEPAT.HomeID
        LEFT JOIN LocalPortal.dbo.Agency AGENCY ON AGENCY.AgencyID = HOME.AgencyID
        WHERE  (PAT.FirstName LIKE '%${req.params.keyword}' OR PAT.LastName LIKE '%${req.params.keyword}' OR PAT.FirstName + ' ' + LastName LIKE '${req.params.keyword}%')`;
        request1.query(query,function (error, results) {
           if(error) res.json(error);
           else  res.json(results.recordset);  
        })
    });

    // Search patients only that are not added in home
    app.get('/notHomePatients/filter/:keyword', (req,res) => {
        var query = `SELECT Pharmacy.dbo.Pat.ID, Pharmacy.dbo.Pat.LastName, Pharmacy.dbo.Pat.FirstName
         FROM Pharmacy.dbo.Pat

         WHERE 
         (FirstName LIKE '%${req.params.keyword}' OR LastName LIKE '%${req.params.keyword}' OR FirstName + ' ' + LastName LIKE '${req.params.keyword}%') 
         AND Pharmacy.dbo.Pat.ID NOT IN (SELECT LocalPortal.dbo.HomePatRel.PatID FROM LocalPortal.dbo.HomePatRel)`;

        request1.query(query,function (error, results) {
           if(error) res.json(error);
           else  res.json(results.recordset);  
        })
    });


    // Get Patients PRNS
    app.get('/getPatientsRx/:PatID', (req,res) => {
        var query = `SELECT  drg.GenericName + ' ' + drg.Strength +  ' Doc: ' + doc.LastName + ', ' + doc.FirstName + ' Rem: ' + CAST(rx.RemQty AS VARCHAR(16)) + ' Rx:' + CAST(rx.RxNum AS VARCHAR(16)) as DrgInfo , rx.ID, rx.OrigRxNum, rx.RxNum, rx.Init, rx.FillDate, rx.DispQty, rx.AuthQty, rx.DaysSupply, rx.SIG, rx.DIN, rx.NHCycle, rx.NHBatchFill, rx.NHUnitDose
        FROM Pharmacy.dbo.Rx rx
        LEFT JOIN Pharmacy.dbo.Drg drg on DrgID = drg.ID
        LEFT JOIN Pharmacy.dbo.Doc doc on DocID = doc.ID
        WHERE rx.Status NOT IN (2,5,6,7,8,3)
        AND rx.PatID = ${req.params.PatID}
        AND rx.NHBatchFill = 0`;

        request1.query(query,function (error, results) {
            if(error) res.json(error);
            else  res.json(results.recordset);  
        })
    });

        
    // Get Patients PRNS NEW
    app.get('/getPatRx/:PatID', (req,res) => {
        var query = `SELECT  drg.GenericName, drg.Strength , doc.LastName + ', ' + doc.FirstName as 'Doctor', 
        rx.RemQty, rx.RxNum, rx.FillDate, rx.SIG, rx.ID, rx.DIN, rx.NHUnitDose, 
        doc.NoFaxRefills, doc.NoPhoneRefills
        FROM Pharmacy.dbo.Rx rx
        LEFT JOIN Pharmacy.dbo.Drg drg on DrgID = drg.ID
        LEFT JOIN Pharmacy.dbo.Doc doc on DocID = doc.ID
        WHERE rx.Status NOT IN (2,5,6,7,8,3)
        AND rx.PatID = ${req.params.PatID}
        AND rx.NHBatchFill = 0`;

        request1.query(query,function (error, results) {
            if(error) res.json(error);
            else  res.json(results.recordset);  
        })
    });

    app.get('/patients/filterTop/:keyword', (req,res) => {
        var query = `SELECT TOP 10 PAT.ID, PAT.FirstName + ' ' + PAT.LastName as PatientName, HOME.HomeName
        FROM Pharmacy.dbo.Pat PAT
        LEFT JOIN LocalPortal.dbo.HomePatRel HOMEPAT ON HOMEPAT.PatID = Pat.ID
        LEFT JOIN LocalPortal.dbo.Home HOME ON HOME.HomeID = HOMEPAT.HomeID
        WHERE  PAT.FirstName  LIKE '%${req.params.keyword}%' OR PAT.LastName LIKE '%${req.params.keyword}%'` ; 
        request1.query(query,function (error, results) {
           if(error) res.json(error);
           else  res.json(results.recordsets[0]);  
        })
    })

      // Get Patients PRNS
      app.get('/getPatPro/:PatID', (req,res) => {
        var query = `SELECT TOP 10 PAT.ID, CONCAT(PAT.FirstName,' ', PAT.LastName) as PatientName,H.BinName, H.HomeName,HA.Address1, HA.Address2,HA.City, HA.Prov, HA.Postal
        FROM Pharmacy.dbo.Pat PAT
        LEFT JOIN LocalPortal.dbo.HomePatRel HP ON HP.PatID = Pat.ID
        LEFT JOIN LocalPortal.dbo.Home H ON H.HomeID = HP.HomeID
        LEFT JOIN LocalPortal.dbo.HomeAddresses HA on (HA.HomeID = H.HomeID AND HA.Active = 1)
        WHERE  PAT.ID ='${req.params.PatID}'`;

        request1.query(query,function (error, results) {
            if(error) res.json(error);
            else  res.json(results.recordset[0]);  
        })
    });


    /*==================================================================
    WRITTEN BY: SEAMLESS CARE
    DECRIPTION: GET MEDICATION OF PATIENT
    ===================================================================*/
    app.get('/getPatMedication/:PatID', (req,res) => {
        var query = `SELECT DRG.BrandName, DRG.GenericName, DRG.Strength,RX.RxNum, DRG.DIN, DOC.LastName + ', ' + DOC.FirstName as 'doctor', RX.SIG
        FROM  Pharmacy.dbo.Rx RX 
        LEFT JOIN Pharmacy.dbo.Drg DRG on DRG.ID = RX.DrgID
        LEFT JOIN Pharmacy.dbo.Doc DOC on DOC.ID = RX.DocID
        WHERE  RX.PatID=${req.params.PatID}`;
        dbrequest.query(query,function (error, results) {
           if(error) res.json(error);
           else  res.json(results.recordsets[0]);
        })
    });
      /*==================================================================
    WRITTEN BY: SEAMLESS CARE
    DECRIPTION: SERVICE TICKET
    ===================================================================*/

    app.get('/getPatTicketService/:PatID', (req,res) => {
        var query = `SELECT T.PharmacyTicketID, T.DateCreated,T.DeliveryDate, T.TicketType, TT.TicketName,T.UserIDCompleted, T.DateComplete, U.userName as createdBy, T.Details
        FROM LocalPortal.dbo.PharmacyTickets T
        LEFT JOIN LocalPortal.dbo.Orders O on O.TicketID = T.PharmacyTicketID
        LEFT JOIN LocalPortal.dbo.TicketType TT on TT.TicketTypeID = T.TicketType
        LEFT JOIN LocalPortal.dbo.Users U on U.user_id = T.UserIDCreated
        WHERE  T.PatID=${req.params.PatID}`;
        dbrequest.query(query,function (error, results) {
           if(error) res.json(error);
           else  res.json(results.recordsets[0]);
        })
    });
    /*==================================================================
    WRITTEN BY: SEAMLESS CARE
    DECRIPTION: GET ALLERGIES AND CONDITIONS
    ===================================================================*/

    app.get('/getPatAlgCnd/:PatID', (req,res) => {
        var query = `SELECT Code, Comment FROM Pharmacy.dbo.PatCnd  WHERE PatID=${req.params.PatID};
                 SELECT Code,Comment FROM Pharmacy.dbo.PatAlg  WHERE PatID=${req.params.PatID};`;
        dbrequest.query(query,function (error, results) {
           if(error) res.json(error);
           else res.json({"Cnd":results.recordsets[0], "Alg":results.recordsets[1]});
        })
    });




  
}