var mssql = require("mssql");
var dateTime = require('node-datetime');
var groupArray = require('group-array');
const commonFn = require('./commonFn');
module.exports = function(app,db){
    request = new mssql.Request(db);
    var dt = dateTime.create();
    var created = dt.format('Y-m-d H:M:S');
    
/*==================================================================
WRITTEN BY:SEAMLESS CARE
DECRIPTION: GET ALL HOMES AND ITS APPROVED ORDERS
===================================================================*/
//  WHERE DATEPART (weekday,PT.DeliveryDate) != DATEPART (weekday,CURRENT_TIMESTAMP)
// WHERE HPR.PatID IN (SELECT O.PatID FROM LocalPortal.dbo.Orders O LEFT JOIN LocalPortal.dbo.PharmacyTickets PT on PT.PharmacyTicketID = O.TicketID);
    app.get('/getInBin', (req,res) => {
        var query = `SELECT (SELECT COUNT(O.OrderID) FROM LocalPortal.dbo.Orders O INNER JOIN LocalPortal.dbo.HomePatRel HPR on HPR.PatID = O.PatID WHERE HPR.HomeID = H.HomeID AND O.TicketType !=1 AND O.DatePackaged is not null  AND  O.UserIDCompleted is null AND  O.DateApproved is not null AND O.DateBinned is not null AND (O.OrderStatus =1 OR O.OrderStatus =3)) as TotalOrders,
        HPR.PatID, H.HomeID, H.HomeName, H.SubAreaID, H.AreaID, HA.DeliveryInstructions,H.DefaultDeliveryDay,HA.Title, H.HomePhone,HA.Address1, HA.Address2,HA.City, HA.Prov, HA.Postal, H.BinName,H.BinBarcode,P.LastName, P.FirstName
        FROM LocalPortal.dbo.Home H
        LEFT JOIN LocalPortal.dbo.HomeAddresses HA on (HA.HomeID = H.HomeID AND HA.Active = 1)
        LEFT JOIN LocalPortal.dbo.HomePatRel HPR on HPR.HomeID = H.HomeID
        LEFT JOIN Pharmacy.dbo.Pat P on P.ID = HPR.PatID

        SELECT O.PatID, H.HomeID,O.OrderID,O.BarCodeImage,U.userName as createdBy,PT.TicketType, US.userName as Pharmacist, O.BarCode, H.HomeName,PT.PharmacyTicketID,PT.Details,PT.DeliveryDate,PT.DateCreated, H.BinName,H.BinBarcode,P.LastName, P.FirstName
        FROM LocalPortal.dbo.Home H
        LEFT JOIN LocalPortal.dbo.HomePatRel HPR on HPR.HomeID = H.HomeID
        LEFT JOIN Pharmacy.dbo.Pat P on P.ID = HPR.PatID
        LEFT JOIN LocalPortal.dbo.Orders O on O.PatID = P.ID
        LEFT JOIN LocalPortal.dbo.PharmacyTickets PT on PT.PharmacyTicketID = O.TicketID
        LEFT JOIN LocalPortal.dbo.Users U ON U.user_id = PT.UserIDCreated
        LEFT JOIN LocalPortal.dbo.Users US ON US.user_id = PT.UserIDPreApproval
        WHERE O.DatePackaged is not null AND PT.TicketType != 1  AND O.DateApproved is not null AND O.DateBagged is null AND O.DateBinned is not null AND O.DeliveryPkgID is null AND DATEPART (weekday,PT.DeliveryDate) != DATEPART (weekday,CURRENT_TIMESTAMP)`;
        dbrequest.query(query,function (error, results) {
            if(error) res.json(error);
            else{
                dataObj = [{"Homes":commonFn.reduceHomePat(groupArray(results.recordsets[0], 'HomeID')),"OrderCards":results.recordsets[1]}];
                res.json(dataObj);
            }  
        }) 
    });


/*==================================================================
WRITTEN BY:SEAMLESS CARE
DECRIPTION: GET Due Today
===================================================================*/
// RIGHT JOIN LocalPortal.dbo.Orders O on O.PatID = P.ID
//     RIGHT JOIN LocalPortal.dbo.PharmacyTickets PT on PT.PharmacyTicketID = O.TicketID
app.get('/getDueToday', (req,res) => {
    var query = `SELECT (SELECT COUNT(O.OrderID) FROM LocalPortal.dbo.Orders O INNER JOIN LocalPortal.dbo.HomePatRel HPR on HPR.PatID = O.PatID WHERE HPR.HomeID = H.HomeID AND O.TicketType !=1 AND  O.DatePackaged is not null  AND O.UserIDCompleted is null AND O.DateApproved is not null AND O.DateBinned is not null AND O.DeliveryPkgID is null) as TotalOrders,
     HPR.PatID, H.HomeID, H.SubAreaID, H.AreaID, HA.DeliveryInstructions,H.DefaultDeliveryDay, H.HomeName,HA.Title, HA.Address1, HA.Address2, HA.City, HA.Prov, HA.Postal, H.HomePhone, H.BinName,H.BinBarcode,P.LastName, P.FirstName
    FROM LocalPortal.dbo.Home H
    LEFT JOIN LocalPortal.dbo.HomePatRel HPR on HPR.HomeID = H.HomeID
    LEFT JOIN LocalPortal.dbo.HomeAddresses HA on (HA.HomeID = H.HomeID AND HA.Active = 1)
    LEFT JOIN Pharmacy.dbo.Pat P on P.ID = HPR.PatID 
    WHERE P.ID IN (SELECT O.PatID FROM LocalPortal.dbo.Orders O LEFT JOIN LocalPortal.dbo.PharmacyTickets PT on PT.PharmacyTicketID = O.TicketID  WHERE DATEPART (weekday,PT.DeliveryDate) = DATEPART (weekday,CURRENT_TIMESTAMP));

    SELECT O.PatID, H.HomeID,O.OrderID,O.BarCodeImage,PT.TicketType,U.userName as createdBy, US.userName as Pharmacist, O.BarCode, H.HomeName,PT.PharmacyTicketID,PT.Details,PT.DeliveryDate,PT.DateCreated, H.BinName,H.BinBarcode,P.LastName, P.FirstName
    FROM LocalPortal.dbo.Home H
    LEFT JOIN LocalPortal.dbo.HomePatRel HPR on HPR.HomeID = H.HomeID
    LEFT JOIN Pharmacy.dbo.Pat P on P.ID = HPR.PatID
    LEFT JOIN LocalPortal.dbo.Orders O on O.PatID = P.ID
    LEFT JOIN LocalPortal.dbo.PharmacyTickets PT on PT.PharmacyTicketID = O.TicketID
    LEFT JOIN LocalPortal.dbo.Users U ON U.user_id = PT.UserIDCreated
    LEFT JOIN LocalPortal.dbo.Users US ON US.user_id = PT.UserIDPreApproval
    WHERE O.DatePackaged is not null AND PT.TicketType != 1 AND O.DateApproved is not null AND O.DateBagged is null AND O.DateBinned is not null AND O.DeliveryPkgID is null`;
    dbrequest.query(query,function (error, results) {
        if(error) res.json(error);
        else{
            dataObj = [{"Homes":commonFn.reduceHomePat(groupArray(results.recordsets[0], 'HomeID')),"OrderCards":results.recordsets[1]}];
            res.json(dataObj);
        }  
    }) 
});



/*==================================================================
WRITTEN BY:SEAMLESS CARE
DECRIPTION: GET FILTER HOME AND ITS APPROVED ORDERS
===================================================================*/
    app.get('/getFilterInBin/:keyword', (req,res) => {
        var query = `SELECT HPR.PatID, H.HomeID, H.HomeName, H.HomePhone, H.Address1, H.Address2, H.City, H.Prov, H.Postal, H.BinName,H.BinBarcode,P.LastName, P.FirstName
        FROM LocalPortal.dbo.Home H
        LEFT JOIN LocalPortal.dbo.HomePatRel HPR on HPR.HomeID = H.HomeID
        LEFT JOIN Pharmacy.dbo.Pat P on P.ID = HPR.PatID 
        WHERE H.HomeName LIKE '${req.params.keyword}%' OR BinName LIKE '${req.params.keyword}%';

        SELECT DISTINCT O.PatID, H.HomeID,O.OrderID, H.HomeName, H.BinName,H.BinBarcode,P.LastName, P.FirstName
        FROM LocalPortal.dbo.Home H
        LEFT JOIN LocalPortal.dbo.HomePatRel HPR on HPR.HomeID = H.HomeID
        LEFT JOIN Pharmacy.dbo.Pat P on P.ID = HPR.PatID
        LEFT JOIN LocalPortal.dbo.Orders O on O.PatID = P.ID
        WHERE O.DatePackaged is not null  AND O.DateApproved is not null AND O.DateBagged is null AND O.DateBinned is not null AND O.DeliveryPkgID is null`;
        dbrequest.query(query,function (error, results) {
            if(error) res.json(error);
            else{
                dataObj = [{"Homes":commonFn.reduceHomePat(groupArray(results.recordsets[0], 'HomeID')),"OrderCards":results.recordsets[1]}];
                res.json(dataObj);
            }  
        }) 
    });

/*==================================================================
WRITTEN BY:SEAMLESS CARE
DECRIPTION: GET FILTER HOME AND ITS APPROVED ORDERS
===================================================================*/
    app.get('/scanBinWithBorCode/:barcode', (req,res) => {
        var query = ` SELECT H.HomeID, H.HomeName, H.Postal, H.BinName,H.BinBarcodeImage,H.BinBarcode
        FROM LocalPortal.dbo.Home H WHERE H.BinBarcode ='${req.params.barcode}'`;
        dbrequest.query(query,function (error, results) {
        if(error) res.json(error);
        else  res.json(results.recordsets[0]);
        })
    });



/*==================================================================
WRITTEN BY:SEAMLESS CARE
DECRIPTION: GET AREA
===================================================================*/
app.get('/getArea', (req,res) => {
    var query = `SELECT SA.SubAreaName, SA.SubAreaID, A.AreaName, A.AreaID
    FROM LocalPortal.dbo.Area A
    LEFT JOIN LocalPortal.dbo.SubArea SA on SA.AreaID = A.AreaID
    WHERE SubAreaID IN (SELECT SubAreaID FROM LocalPortal.dbo.Home)`;
    dbrequest.query(query,function (error, results) {
    if(error) res.json(error);
    else{
       var data =  groupArray(results.recordsets[0], 'AreaID');
        var dataObj = [];
        for(var i in data){
            dataObj.push({"Area":data[i][0],"SubArea":data[i]});
        }
        res.json(dataObj);
    }
    })
});
    
    
}