var mssql = require("mssql");
var dateTime = require('node-datetime');
module.exports = function(app,db){
    request = new mssql.Request(db);

    // Just for testing it will remove later
    app.get('/test/:tablename', (req,res) => {
      //query = `DELETE FROM LocalPortal.dbo.${req.params.tablename}`;
        
        //query = `INSERT INTO LocalPortal.dbo.${req.params.tablename} (HomeID, PatID) VALUES ('73', '48593')`;

       // query = `UPDATE LocalPortal.dbo.${req.params.tablename} SET HomeID = '74', PatID = '48593'`;

        //query = `SELECT * FROM LocalPortal.dbo.UserRelationUserRole`;

     query = `SELECT * FROM LocalPortal.dbo.${req.params.tablename}`;
        
        
        request.query(query,function (error, results) {
            if(error) {
                res.json(error);
            } else {
                res.json(results.recordset);
            }
        })
    });

    // Get UserType
    app.get('/user-type', (req,res) => {
        request.query('SELECT * FROM LocalPortal.dbo.UserRole',function (error, results) {
            res.json(results.recordset);
        })
    });
    
    // Inserting User
    app.post('/user/add', (req,res) => {

        // Check is email already exist
        query = "SELECT email FROM LocalPortal.dbo.Users WHERE email = '"+req.body.email+"'";
        request.query(query,function (error, results) {
            if(error) {
                res.json(error);
            } else {
                if(!results.recordset[0]) {
                    // adding user
                    var dt = dateTime.create();
                    var actionDate = dt.format('Y-m-d H:M:S');
                    var query = `INSERT INTO LocalPortal.dbo.Users (userName, email, password, phone, dateCreated) VALUES ('${req.body.userName}','${req.body.email}','${req.body.password}','${req.body.phone}','${actionDate}');SELECT SCOPE_IDENTITY() as user_id`;
                    request.query(query ,function (error, results) {
                        if(error) {
                            res.json(error);
                        } else {
                            var user_id = results.recordsets[0][0].user_id;
                            var user_types = req.body.userType;
                            for(x = 0; x < user_types.length; x++) {
                                var userRole_query = `INSERT INTO LocalPortal.dbo.UserRelationUserRole (user_id, RoleId) VALUES ('${user_id}','${user_types[x]}');SELECT SCOPE_IDENTITY() as user_id`;
                                request.query(userRole_query ,function (error, results) {
                                    if(error) {
                                        res.json(error);
                                    } 
                                })
                            }
                            res.json('success');
                        }  
                    })
                    //-------------
                }   else {
                    // Email Exist
                    res.json(null);
                }
            }
        })
    });

    // Get Users with there roles using RoleID | Fetch only limit data
    app.get('/users/:id/:start', (req,res) => {
        var limit = 50;
        var query = `SELECT u.user_id, u.userName, u.email, u.password, u.phone, u.dateCreated,u.updatedAt, ut.UserRole, ut.RoleId 
        FROM LocalPortal.dbo.Users u 

        LEFT JOIN LocalPortal.dbo.UserRelationUserRole ur ON ur.user_id = u.user_id
        

        LEFT JOIN LocalPortal.dbo.userRole ut ON ut.RoleId = ur.RoleId 


        WHERE ur.RoleId = ${req.params.id} AND u.is_super IS NULL ORDER BY u.user_id DESC 
        OFFSET ${req.params.start} ROWS FETCH NEXT ${limit} ROWS ONLY;

        SELECT COUNT(u.user_id) as total FROM LocalPortal.dbo.Users u 

        LEFT JOIN LocalPortal.dbo.UserRelationUserRole ur ON ur.user_id = u.user_id

        LEFT JOIN LocalPortal.dbo.userRole ut ON ut.RoleId = ur.RoleId 
        
        WHERE ur.RoleId = ${req.params.id} AND u.is_super IS NULL
        `
        request.query(query,function (error, results) {
            if(error) {
                res.json(error);
            }   else {
                var data = { "users":"" ,"total_user": ""};
                data['users'] = results.recordsets[0];
                data['total_user'] = results.recordsets[1][0].total;
                res.json(data);
            }
        })
    });

        // Get all users with there all roles | Fetch only limit data
        app.get('/allUsers/:start', (req,res) => {
            var limit = 50;
            var query = `SELECT U.user_id, U.userName, U.email, U.password, U.phone, U.dateCreated, U.updatedAt,
            STUFF
            ((SELECT ', ' +UR.UserRole FROM LocalPortal.dbo.UserRelationUserRole as URUR
            LEFT JOIN LocalPortal.dbo.UserRole UR ON UR.RoleId = URUR.RoleId
            WHERE URUR.user_id = U.user_id FOR XML PATH('')), 1, 1, '') [user_roles]
            FROM LocalPortal.dbo.Users as U WHERE U.is_super IS NULL  ORDER BY U.user_id DESC
            OFFSET ${req.params.start} ROWS FETCH NEXT ${limit} ROWS ONLY;
            
    
            SELECT COUNT(u.user_id) as total FROM LocalPortal.dbo.Users u
            WHERE u.is_super IS NULL 
            `
            request.query(query,function (error, results) {
                if(error) {
                    res.json(error);
                }   else {
                    var data = { "users":"" ,"total_user": ""};
                    data['users'] = results.recordsets[0];
                    data['total_user'] = results.recordsets[1][0].total;
                    res.json(data);
                }
            })
        });

        app.get('/getRegularUser/:AgencyID', (req,res) => {
            var query = `SELECT U.userName,U.user_id  FROM LocalPortal.dbo.Users U WHERE U.is_super = 2 AND U.AgencyID =${req.params.AgencyID} `;
            request.query(query,function (error, results) {
                if(error) res.json(error);
                else res.json(results.recordsets[0]);
            })
        })


    /*app.get('/user/:id', (req,res) => {
        var query = `SELECT u.user_id, u.userName, u.email, u.phone, u.dateCreated, u.updatedAt, ut.UserRole 
            FROM LocalPortal.dbo.Users u 
            INNER JOIN  LocalPortal.dbo.userRole  ut ON ut.RoleId = u.userType
            WHERE u.user_id = ${req.params.id}`
        request.query(query,function (error, results) {
            res.json(results.recordset[0]);
        })
    });*/
    
    // Purge User
    app.delete('/user/:id', (req,res) => {
        request.query(`DELETE FROM LocalPortal.dbo.Users WHERE user_id =${req.params.id}`,function (error, results) {
            if(error) {
                res.json(error);
            } else {
                res.json(results);
            }
        })
    });

    // Updating user
    app.put('/user/:id', (req,res) => {

                // Check is email already exist
                query = "SELECT email FROM LocalPortal.dbo.Users WHERE (email = '"+req.body.email+"' AND user_id != "+req.params.id+")";
                request.query(query,function (error, results) {
                    if(error) {
                        res.json(error);
                    } else {
                        if(!results.recordset[0]) {
                            // Updating user
                            var dt = dateTime.create();
                            var actionDate = dt.format('Y-m-d H:M:S');
                            var query = `UPDATE LocalPortal.dbo.Users SET userName = '${req.body.userName}', email = '${req.body.email}', password = '${req.body.password}', phone = '${req.body.phone}', updatedAt='${actionDate}' 
                            WHERE user_id = ${req.params.id};
                            
                            DELETE FROM LocalPortal.dbo.UserRelationUserRole WHERE user_id = '${req.params.id}'
                            `;
                            request.query(query,function (error, results) {
                                if(error) {
                                    res.json(error);
                                } else {
                                    var user_id = req.params.id;
                                    var user_types = req.body.userType;
                                    for(x = 0; x < user_types.length; x++) {
                                        var userRole_query = `INSERT INTO LocalPortal.dbo.UserRelationUserRole (user_id, RoleId) VALUES ('${user_id}','${user_types[x]}');SELECT SCOPE_IDENTITY() as user_id`;
                                        
                                        request.query(userRole_query ,function (error, results) {
                                            if(error) {
                                                res.json(error);
                                            } 
                                        })
                                    }
                                    res.json('success');
                                }
                            })
                            //-------------
                        }   else {
                            // Email Exist
                            res.json(null);
                        }
                    }
                })
    });


        // Saving CareGiver and assign them agency
        app.post('/superUsers', (req,res) => {

            // Check is email already exist
            query = "SELECT email FROM LocalPortal.dbo.Users WHERE email = '"+req.body.email+"'";
            request.query(query,function (error, results) {
                if(error) {
                    res.json(error);
                } else {
                    if(!results.recordset[0]) {
                        // adding user
                        var dt = dateTime.create();
                        var actionDate = dt.format('Y-m-d H:M:S');
                        var query = `INSERT INTO LocalPortal.dbo.Users (userName, email, password, AgencyID, phone, dateCreated, is_super) VALUES ('${req.body.userName}','${req.body.email}','${req.body.password}','${req.body.AgencyID}','${req.body.phone}','${actionDate}', '${req.body.is_super}');SELECT SCOPE_IDENTITY() as user_id`;
                        request.query(query ,function (error, results) {
                            if(error) {
                                res.json(error);
                            } else {
                                res.json(results.recordset[0]);
                            }  
                        })
                        //-------------
                    }   else {
                        // Email Exist
                        res.json(null);
                    }
                }
            })
        });


            // Get Caregiver using AgencyID
    app.get('/superUsers/:AgencyID/:start', (req,res) => {
        var limit = 20;
        var query = `SELECT * FROM LocalPortal.dbo.Users WHERE AgencyID = '${req.params.AgencyID}' ORDER BY user_id DESC OFFSET ${req.params.start} ROWS FETCH NEXT ${limit} ROWS ONLY;
        SELECT COUNT(user_id) as total FROM LocalPortal.dbo.Users WHERE AgencyID = '${req.params.AgencyID}'
        `
        request.query(query,function (error, results) {
            if(error) {
                res.json(error);
            }   else {
                var data = { "superUsers":"" ,"total_superUsers": ""};
                data['superUsers'] = results.recordsets[0];
                data['total_superUsers'] = results.recordsets[1][0].total;
                res.json(data);
            }
        })
    });

                // Get Caregiver using AgencyID
                app.get('/superUsersNotAddedInHome/:AgencyID/:start', (req,res) => {
                    var limit = 20;
                    var query = `SELECT * FROM 
                    LocalPortal.dbo.Users 
                    WHERE AgencyID = '${req.params.AgencyID}'
                    ORDER BY user_id DESC OFFSET ${req.params.start} ROWS FETCH NEXT ${limit} ROWS ONLY;

                    SELECT COUNT(user_id) as total FROM LocalPortal.dbo.Users 
                    WHERE AgencyID = '${req.params.AgencyID}' 
                    `
                    request.query(query,function (error, results) {
                        if(error) {
                            res.json(error);
                        }   else {
                            var data = { "superUsers":"" ,"total_superUsers": ""};
                            data['superUsers'] = results.recordsets[0];
                            data['total_superUsers'] = results.recordsets[1][0].total;
                            res.json(data);
                        }
                    })
                });

        // Updating Caregiver
        app.put('/superUsers/:id', (req,res) => {

            // Check is email already exist
            query = "SELECT email FROM LocalPortal.dbo.Users WHERE (email = '"+req.body.email+"' AND user_id != "+req.params.id+")";
            request.query(query,function (error, results) {
                if(error) {
                    res.json(error);
                } else {
                    if(!results.recordset[0]) {
                        // Updating user
                        var dt = dateTime.create();
                        var actionDate = dt.format('Y-m-d H:M:S');
                        var query = `UPDATE LocalPortal.dbo.Users SET userName = '${req.body.userName}', email = '${req.body.email}', password = '${req.body.password}', phone = '${req.body.phone}', updatedAt='${actionDate}', is_super = '${req.body.is_super}' 
                        WHERE user_id = ${req.params.id}`;
                        request.query(query,function (error, results) {
                            if(error) {
                                res.json(error);
                            } else {
                                res.json(results);
                            }
                        })
                        //-------------
                    }   else {
                        // Email Exist
                        res.json(null);
                    }
                }
            })

});


            // Get User User Roles
            app.get('/UserRoles/:user_id', (req,res) => {
                var query = `SELECT 
                LocalPortal.dbo.UserRelationUserRole.RoleId, LocalPortal.dbo.UserRole.UserRole 
                FROM 
                LocalPortal.dbo.UserRelationUserRole 
                LEFT JOIN LocalPortal.dbo.UserRole ON LocalPortal.dbo.UserRole.RoleId = LocalPortal.dbo.UserRelationUserRole.RoleId
                WHERE LocalPortal.dbo.UserRelationUserRole.user_id = '${req.params.user_id}';
                `
                request.query(query,function (error, results) {
                    if(error) {
                        res.json(error);
                    }   else {
                        
                        res.json(results.recordset);
                    }
                })
            });

            
            app.get('/getUserFilter/:keyword', (req,res) => {
                var query = `SELECT u.user_id, u.userName 
                 FROM LocalPortal.dbo.Users u 
                 LEFT JOIN LocalPortal.dbo.UserRelationUserRole UR on UR.user_id = u.user_id
                 WHERE u.is_super IS NULL AND UR.RoleId = 4 AND u.userName LIKE '%${req.params.keyword}%' AND u.user_id NOT IN(SELECT DriverUserID FROM LocalPortal.dbo.DeliveryRuns)` 
                request.query(query,function (error, results) {
                    if(error)  res.json(error);
                    else res.json(results.recordsets[0]);
                })
            });
            
            app.post('/UserPatRel', (req,res) => {
                var dt = dateTime.create();
                var actionDate = dt.format('Y-m-d H:M:S');
                var query = `DELETE LocalPortal.dbo.UserPatRel WHERE PatID= ${req.body.PatID};`;
                for(var i=0; i< req.body.UserIDs.length; i++){
                    query += `INSERT INTO LocalPortal.dbo.UserPatRel (PatID,UserID,DateCreated) VALUES(${req.body.PatID},${req.body.UserIDs[i].user_id},'${actionDate}');`;
                }
                // res.json(query);
                request.query(query,function (error, results) {
                    if(error)  res.json(error);
                    else res.json(results);
                })
            });
            app.get('/UserPatRel/:PatID', (req,res) => {
                var query = `SELECT u.user_id, u.userName FROM LocalPortal.dbo.Users u LEFT JOIN LocalPortal.dbo.UserPatRel UPR on UPR.UserID = u.user_id WHERE UPR.PatID =${req.params.PatID}` 
                request.query(query,function (error, results) {
                    if(error)  res.json(error);
                    else res.json(results.recordsets[0]);
                })
            });

            app.get('/getDrivers', (req,res) => {
                var query = `SELECT u.user_id, u.userName FROM LocalPortal.dbo.Users u LEFT JOIN LocalPortal.dbo.UserRelationUserRole URUR on URUR.user_id = u.user_id WHERE URUR.RoleId =4` 
                request.query(query,function (error, results) {
                    if(error)  res.json(error);
                    else res.json(results.recordsets[0]);
                })
            });

}