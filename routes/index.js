const loginRoutes = require('./api/login');
const ticketsRoutes = require('./api/tickets');
const usersRoutes = require('./api/users');
const patientRoutes = require('./api/patients');
const orderRoutes = require('./api/orders');
const agencyRoutes = require('./api/agency');
const homeRoutes = require('./api/home');
const reminderRoutes = require('./api/reminder');
const inBinRoutes = require('./api/inBin');
const deliveryPkgsRoutes = require('./api/deliveryPkgs');


module.exports = function(app, db){
    loginRoutes(app, db);
    ticketsRoutes(app,db);
    usersRoutes(app,db);
    patientRoutes(app,db);
    orderRoutes(app,db);
    agencyRoutes(app,db);
    homeRoutes(app,db);
    reminderRoutes(app,db);
    inBinRoutes(app,db);
    deliveryPkgsRoutes(app,db);
    
    
}

// module.exports = function(app, db){
//     ticketsRoutes(app, db);
// }